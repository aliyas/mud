# Authenticating The Server

A file named `server.cer` is required at this location in order to authenticate
the server and establish a websocket over TLS. Please see stockade-server for
more details about creating a certificate here using `keytool`.