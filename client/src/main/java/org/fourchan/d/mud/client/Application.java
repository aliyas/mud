package org.fourchan.d.mud.client;

import java.io.IOException;
import java.security.Security;

import org.apache.commons.cli.ParseException;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.fourchan.d.mud.client.fxml.FXMLLoaders;

import com.google.inject.Guice;
import com.google.inject.Injector;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Application extends javafx.application.Application {
	private static String[] ARGS;
	public static void main(String[] args) {
		Security.addProvider(new BouncyCastleProvider());
		ARGS = args;
		launch(args);
	}
	
	private Injector injector;
	private ClientMessageHandler client;
	
	@Override
	public void init() throws IOException {
		StockadeCommandLine commandLine = null;
		try {
			commandLine = new StockadeCommandLine(ARGS);
		} catch (ParseException e) {
			System.out.print(e.getMessage());
			System.exit(1);
		}

		injector = Guice.createInjector(new ClientModule(commandLine));
	}

	@Override
	public void start(Stage stage) throws Exception {
		FXMLLoader loader = new FXMLLoader(getClass().getResource(FXMLLoaders.ROOT_FXML));
	    loader.setControllerFactory(injector::getInstance);
		Scene scene = new Scene(loader.load(), 800, 600);
		stage.setScene(scene);
		stage.show();
		client = injector.getInstance(ClientMessageHandler.class);
		client.connect();
	}

	@Override
	public void stop() throws Exception {
		client.disconnect();
	}
}
