package org.fourchan.d.mud.client.controller;

import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Inject;

import org.fourchan.d.mud.client.ui.GameController;
import org.fourchan.d.mud.common.Constants;
import org.fourchan.d.mud.common.messages.common.SpawnPoint;

import com.google.common.base.Strings;

import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;

public class FXMLCharacterCreationController {
	private final GameSceneChanger gameSceneChanger;
	private final GameController gameController;
	private List<SpawnPoint> preInitPoints;

	@Inject
	public FXMLCharacterCreationController(
			GameSceneChanger gameSceneChanger,
			GameController gameController) {
		this.gameSceneChanger = gameSceneChanger;
		this.gameController = gameController;
	}

	@FXML private TextField firstNameField;
	@FXML private TextField lastNameField;
	@FXML private ComboBox<String> sexChoice;
	@FXML private ComboBox<String> spawnChoice;

	public void initialize() {
		sexChoice.getItems().setAll(Constants.ALL_STARTING_SEXES);
		if (preInitPoints != null) {
			notifySpawnPoints(preInitPoints);
		}
	}

	@FXML
	public void handleBackButtonPressed() {
		gameSceneChanger.toCharacterSelection();
	}

	@FXML
	public void handleCreateCharacter() {
		gameController.sendCreateCharacterRequest(
				Strings.nullToEmpty(firstNameField.getText()).trim(),
				Strings.nullToEmpty(lastNameField.getText()).trim(),
				Strings.nullToEmpty(sexChoice.getValue()).trim(),
				Strings.nullToEmpty(spawnChoice.getValue()).trim());
	}

	public void notifySpawnPoints(List<SpawnPoint> points) {
		if (spawnChoice == null) {
			preInitPoints = points;
			return;
		}
		List<String> names = points.stream()
				.map(point -> point.name)
				.collect(Collectors.toList());
		spawnChoice.getItems().setAll(names);
	}

	public void clear() {
		firstNameField.clear();
		lastNameField.clear();
		sexChoice.getSelectionModel().clearSelection();
		spawnChoice.getSelectionModel().clearSelection();
	}
}
