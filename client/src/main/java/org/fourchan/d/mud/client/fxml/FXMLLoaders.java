package org.fourchan.d.mud.client.fxml;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Injector;

import javafx.fxml.FXMLLoader;
import javafx.scene.Node;

public final class FXMLLoaders {
	private FXMLLoaders() {}

	private static final Logger logger = LoggerFactory.getLogger(FXMLLoaders.class);
	private static final String PATH = "fxml/";
	public static final String ROOT_FXML = PATH + "root.fxml";
	public static final String LOGIN_FXML = "login.fxml";
	public static final String REGISTER_FXML = "register.fxml";
	public static final String MAIN_GAME_FXML = "mainGame.fxml";
	public static final String CHARACTER_SELECTION_FXML = "characterSelection.fxml";
	public static final String CHARACTER_CREATION_FXML = "characterCreation.fxml";
	public static final String MAIN_GAME_SCREEN_FXML = "mainGameScreen.fxml";
	public static final String INVENTORY_FXML = "inventory.fxml";
	public static final String GUEST_ACKNOWLEDGE_FXML = "guestAck.fxml";
	public static final String PROMOTE_GUEST_FXML = "promoteGuest.fxml";

	public static Node load(String name, Injector injector) {
		FXMLLoader loader = new FXMLLoader(FXMLLoaders.class.getResource(name));
	    loader.setControllerFactory(injector::getInstance);
	    Node node = null;
	    try {
			node = loader.load();
		} catch (IOException e) {
			logger.error("Could not load FXML file '{}': {}", name, e);
		}
	    return node;
	}
}
