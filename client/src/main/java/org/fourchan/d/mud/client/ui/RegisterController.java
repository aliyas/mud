package org.fourchan.d.mud.client.ui;

import org.fourchan.d.mud.common.messages.responses.RegisterResponse;

public interface RegisterController {
	void handleRegisterResponse(RegisterResponse response);
}
