package org.fourchan.d.mud.client.controller;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.fourchan.d.mud.client.ClientMessageSender;
import org.fourchan.d.mud.client.SceneChanger;
import org.fourchan.d.mud.client.controller.FXMLModule.AllMapPoints;
import org.fourchan.d.mud.client.controller.FXMLModule.EntitiesToText;
import org.fourchan.d.mud.client.controller.FXMLModule.MapPointIdsToText;
import org.fourchan.d.mud.client.controller.FXMLModule.UiScheduledThreadPool;
import org.fourchan.d.mud.client.fxml.FXMLLoaders;
import org.fourchan.d.mud.client.game.ClientGameState;
import org.fourchan.d.mud.client.ui.GameController;
import org.fourchan.d.mud.client.ui.LogoutController;
import org.fourchan.d.mud.client.ui.LogoutInitiator;
import org.fourchan.d.mud.client.ui.StatusMessager;
import org.fourchan.d.mud.common.Constants;
import org.fourchan.d.mud.common.map.MapPointId;
import org.fourchan.d.mud.common.messages.common.Entity;
import org.fourchan.d.mud.common.messages.common.Player.PlayerKind;
import org.fourchan.d.mud.common.messages.responses.ActionPayload;
import org.fourchan.d.mud.common.messages.responses.CharacterSelectedResponse;
import org.fourchan.d.mud.common.messages.responses.CreateCharacterResponse;
import org.fourchan.d.mud.common.messages.responses.EntityPayload;
import org.fourchan.d.mud.common.messages.responses.InventoryResponse;
import org.fourchan.d.mud.common.messages.responses.LoginResponse;
import org.fourchan.d.mud.common.messages.responses.MapPayload;
import org.fourchan.d.mud.common.messages.responses.MoveResponse;
import org.fourchan.d.mud.common.messages.responses.NewGuestResponse;
import org.fourchan.d.mud.common.messages.responses.PlayerInfoResponse;
import org.fourchan.d.mud.common.messages.responses.PromoteGuestToNonGuestResponse;
import org.fourchan.d.mud.common.messages.responses.SpawnPointResponse;
import org.fourchan.d.mud.common.messages.responses.TextPayload;

import com.google.inject.Inject;
import com.google.inject.Injector;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;

public class FXMLMainGameController implements LogoutController, LogoutInitiator, GameController, GameSceneChanger, StatusMessager {
	private final Injector injector;
	private final SceneChanger sceneChanger;
	private final ClientMessageSender client;
	private final FXMLCharacterSelectionController fxmlCharacterSelectionController;
	private final FXMLCharacterCreationController fxmlCharacterCreationController;
	private final FXMLMainGameScreen fxmlMainGameScreen;
	private final FXMLInventoryScreen fxmlInventoryScreen;
	private final ScheduledExecutorService scheduler;
	private ClientGameState clientGameState;

	@Inject
	public FXMLMainGameController(
			Injector injector,
			SceneChanger sceneChanger,
			ClientMessageSender client,
			FXMLMainGameScreen fxmlMainGameScreen,
			FXMLCharacterSelectionController fxmlCharacterSelectionController,
			FXMLCharacterCreationController fxmlCharacterCreationController,
			FXMLInventoryScreen fxmlInventoryScreen,
			@UiScheduledThreadPool ScheduledExecutorService scheduler,
			@MapPointIdsToText Map<MapPointId, String> displayMap,
			@EntitiesToText Map<Entity.Type, String> entityMap,
			@AllMapPoints List<MapPointId> mapPoints) {
		this.injector = injector;
		this.sceneChanger = sceneChanger;
		this.client = client;
		this.scheduler = scheduler;
		this.fxmlCharacterSelectionController = fxmlCharacterSelectionController;
		this.fxmlCharacterCreationController = fxmlCharacterCreationController;
		this.fxmlMainGameScreen = fxmlMainGameScreen;
		this.fxmlInventoryScreen = fxmlInventoryScreen;
	}

	@FXML private Text statusMessage;
	@FXML private VBox gameBox;
	@FXML private HBox additionalButtons;

	@Override
	public void handleLogoutResponse(boolean isSuccessful, String error) {
		clearStatusMessage();
		if (!isSuccessful) {
			setErrorStatusMessage(error);
		} else {
	    	Platform.runLater(
	    			() -> sceneChanger.changeScene(FXMLLoaders.LOGIN_FXML));
		}
	}

	@Override
	public void handleLoginResponse(LoginResponse response) {
		clearStatusMessage();
		clientGameState = new ClientGameState(response.getPlayer());
		synchronizeGameState();
		setMessage(String.format("Welcome, %s.", clientGameState.getUsername()));
	}

	@Override
	public void handleNewGuestResponse(NewGuestResponse response) {
		clearStatusMessage();
		clientGameState = new ClientGameState(response.getPlayer());
		synchronizeGameState();
		setMessage(String.format("Welcome, guest."));
	}

	@Override
	public void handlePromoteGuestToNonGuestResponse(PromoteGuestToNonGuestResponse response) {
		clearStatusMessage();
		if (response.isSuccessful()) {
			setMessage(String.format("Successfully converted to a full account, %s!", response.getPlayer().username));
			sceneChanger.goBackOneScene();
			clientGameState = new ClientGameState(response.getPlayer());
			synchronizeGameState();
		} else {
			setErrorStatusMessage(response.getError());
		}
	}

	@Override
	public void doLogout() {
		client.doLogout();
	}

	@Override
	public void handlePlayerInfoResponse(PlayerInfoResponse response) {
		clientGameState = new ClientGameState(response.getPlayer());
		synchronizeGameState();
	}

	@Override
	public void sendCreateCharacterRequest(
			String firstName,
			String lastName,
			String sexChoice,
			String spawnChoice) {
		clearStatusMessage();
		if (firstName.isEmpty()) {
			setErrorStatusMessage("Please enter a first name.");
		} else if (firstName.length() > Constants.MAX_LENGTH_CHARACTER_FIRST_NAME) {
			setErrorStatusMessage("First name is too long.");
		} else if (!firstName.matches(Constants.CHARACTER_NAME_REGEX)) {
			setErrorStatusMessage("First name must be upper and lower english letters only.");
		} else if (lastName.isEmpty()) {
			setErrorStatusMessage("Please enter a last name.");
		} else if (lastName.length() > Constants.MAX_LENGTH_CHARACTER_LAST_NAME) {
			setErrorStatusMessage("Last name is too long.");
		} else if (!lastName.matches(Constants.CHARACTER_NAME_REGEX)) {
			setErrorStatusMessage("Last name must be upper and lower english letters only.");
		} else if (!Constants.ALL_STARTING_SEXES.contains(sexChoice)) {
			setErrorStatusMessage("Invalid sex choice.");
		} else if (spawnChoice.isEmpty()) {
			setErrorStatusMessage("Please choose a spawn location.");
		} else {
			client.doCreateCharacter(firstName, lastName, sexChoice, spawnChoice);
		}
	}

	@Override
	public void handleCreateCharacterResponse(CreateCharacterResponse response) {
		if (!response.isSuccessful()) {
			setErrorStatusMessage(response.getError());
		} else {
			setMessage(String.format("Successfully created a new character."));
			client.doPlayerInfo(clientGameState.getUsername());
			Platform.runLater(() -> fxmlCharacterCreationController.clear());
			toCharacterSelection();
		}
	}

	public void initialize() {
		additionalButtons.managedProperty().bind(additionalButtons.visibleProperty());
		toCharacterSelection();
	}

	@Override
	public void toCharacterSelection() {
		changeScene(FXMLLoaders.CHARACTER_SELECTION_FXML);
	}

	@Override
	public void toCharacterCreation() {
		client.doRequestSpawnPoints();
		changeScene(FXMLLoaders.CHARACTER_CREATION_FXML);
	}

	@Override
	public void toMainGameScreen() {
		changeScene(FXMLLoaders.MAIN_GAME_SCREEN_FXML);
	}

	@Override
	public void toInventoryScreen() {
		client.doRequestInventory();
		changeScene(FXMLLoaders.INVENTORY_FXML);
	}

	@Override
	public void handleCharacterSelectedResponse(CharacterSelectedResponse response) {
		clearStatusMessage();
		if (!response.isSuccessful()) {
			setErrorStatusMessage(response.getError());
			return;
		}
		clientGameState.setSelectedCharacter(response.getCharacter());
		synchronizeGameState();
		toMainGameScreen();
	}

	@Override
	public void handleMapPayload(MapPayload payload) {
		Platform.runLater(() -> fxmlMainGameScreen.handleMapPayload(payload));
	}

	@Override
	public void handleMoveResponse(MoveResponse response) {
		if (!response.isSuccessful()) {
			setErrorStatusMessage(response.getError());
			return;
		}
	}

	@Override
	public void handleSpawnPointResponse(SpawnPointResponse response) {
		if (!response.isSuccessful()) {
			setErrorStatusMessage(response.getError());
			return;
		}
		fxmlCharacterCreationController.notifySpawnPoints(response.getSpawns());
	}

	@Override
	public void handleEntityPayload(EntityPayload payload) {
		Platform.runLater(() -> fxmlMainGameScreen.handleEntityPayload(payload));
	}

	@Override
	public void handleActionPayload(ActionPayload payload) {
		Platform.runLater(() -> fxmlMainGameScreen.handleActionPayload(payload));
	}

	@Override
	public void handleTextPayload(TextPayload payload) {
		Platform.runLater(() -> fxmlMainGameScreen.handleTextPayload(payload));
	}

	private void changeScene(String name) {
		clearStatusMessage();
	    Node node = FXMLLoaders.load(name, injector);
		Platform.runLater(() -> gameBox.getChildren().setAll(node));
	}

	private void synchronizeGameState() {
		Platform.runLater(() -> this.synchronize());
		Platform.runLater(() -> fxmlCharacterSelectionController.synchronize(clientGameState));
		Platform.runLater(() -> fxmlMainGameScreen.synchronize(clientGameState));
	}

	@Override
	public void setMessage(String message) {
		Platform.runLater(() -> {
			statusMessage.setFill(Color.BLACK);
			statusMessage.setText(message);
			scheduler.schedule(() -> clearStatusMessage(), 5, TimeUnit.SECONDS);
		});
	}

	@Override
	public void setErrorStatusMessage(String error) {
		Platform.runLater(() -> {
			statusMessage.setFill(Color.FIREBRICK);
			statusMessage.setText(error);
			scheduler.schedule(() -> clearStatusMessage(), 5, TimeUnit.SECONDS);
		});
	}

	private void clearStatusMessage() {
		Platform.runLater(() -> statusMessage.setText(""));
	}

	@Override
	public void handleInventoryResponse(InventoryResponse response) {
		Platform.runLater(() -> fxmlInventoryScreen.handleInventoryResponse(response));
	}

	private void synchronize() {
		additionalButtons.getChildren().clear();
		if (clientGameState.getPlayerKind().equals(PlayerKind.GUEST)) {
			Button convertButton = new Button();
			convertButton.setOnMouseClicked(action -> sceneChanger.changeScene(FXMLLoaders.PROMOTE_GUEST_FXML));
			convertButton.setText("Convert to a full account");
			additionalButtons.getChildren().add(convertButton);
		}
		if (additionalButtons.getChildren().size() > 0) {
			additionalButtons.setVisible(true);
		} else {
			additionalButtons.setVisible(false);
		}
	}
}
