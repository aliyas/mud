package org.fourchan.d.mud.client;

import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;

import org.fourchan.d.mud.common.map.Direction;
import org.fourchan.d.mud.common.map.MapPointId;
import org.fourchan.d.mud.common.map.Point;
import org.fourchan.d.mud.common.messages.ActionMessage;

public interface ClientMessageSender {
	void doRegister(String username, X509Certificate cert) throws CertificateEncodingException;
	void askLoginChallenge();
	void doLogout();
	void doPlayerInfo(String username);
	void doCreateCharacter(
			String firstName,
			String lastName,
			String sexChoice,
			String spawnChoice);
	void doCharacterSelect(String firstName, String lastName);
	void doMove(Direction direction);
	void doRequestSpawnPoints();
	void doMapUpdateRequest(Point point, MapPointId mapPointId);
	void doActionMessage(ActionMessage message);
	void doRequestInventory();
	void doNewGuest();
	void doPromoteGuestToNonGuest(String username, X509Certificate cert) throws CertificateEncodingException;
}
