package org.fourchan.d.mud.client.controller;

import java.security.PrivateKey;

import org.fourchan.d.mud.client.ClientMessageSender;
import org.fourchan.d.mud.client.SceneChanger;
import org.fourchan.d.mud.client.fxml.FXMLLoaders;
import org.fourchan.d.mud.client.keys.KeyStore;
import org.fourchan.d.mud.client.ui.LoginController;

import com.google.common.base.Strings;
import com.google.inject.Inject;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;

public class FXMLLoginController implements LoginController {
	private final KeyStore keyStore;
	private final SceneChanger sceneChanger;
	private final ClientMessageSender client;
	private PrivateKey loginKey;
	private String username;

	@Inject
	public FXMLLoginController(
			KeyStore keyStore,
			SceneChanger sceneChanger,
			ClientMessageSender client) {
		this.keyStore = keyStore;
		this.sceneChanger = sceneChanger;
		this.client = client;
	}

	@FXML private TextField usernameField;
	@FXML private PasswordField passwordField;
	@FXML private Text statusMessage;

    @FXML
    protected void handleSubmitButtonAction(ActionEvent event) {
		this.username = Strings.nullToEmpty(usernameField.getText()).trim();
		String password = Strings.nullToEmpty(passwordField.getText()).trim();
		this.loginKey = keyStore.getKey(username, password);
		if (loginKey == null) {
			statusMessage.setFill(Color.FIREBRICK);
			statusMessage.setText("Invalid login.");
			return;
		}
		client.askLoginChallenge();
	}

	@FXML
	protected void handleRegisterButtonPressed() {
		clear();
		sceneChanger.changeScene(FXMLLoaders.REGISTER_FXML);
	}

	@FXML
	protected void handleEnterAsGuestPressed() {
		clear();
		sceneChanger.changeScene(FXMLLoaders.GUEST_ACKNOWLEDGE_FXML);
	}

	@Override
	public PrivateKey getLoginKey() {
		return loginKey;
	}

	@Override
	public String getUsername() {
		return username;
	}

	private void clear() {
		usernameField.clear();
		passwordField.clear();
		statusMessage.setText("");
	}

	@Override
	public void handleLoginResponse(boolean successful, String error) {
		if (!successful) {
			statusMessage.setFill(Color.FIREBRICK);
			statusMessage.setText(error);
		} else {
	    	clear();
	    	Platform.runLater(
	    			() -> sceneChanger.changeScene(FXMLLoaders.MAIN_GAME_FXML));
		}
	}
}
