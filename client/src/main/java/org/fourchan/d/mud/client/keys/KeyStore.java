package org.fourchan.d.mud.client.keys;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.security.InvalidAlgorithmParameterException;
import java.security.Key;
import java.security.KeyManagementException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.SecureRandom;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.cert.PKIXBuilderParameters;
import java.security.cert.TrustAnchor;
import java.security.cert.X509CertSelector;
import java.security.cert.X509Certificate;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.net.ssl.CertPathTrustManagerParameters;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;

import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x500.X500NameBuilder;
import org.bouncycastle.asn1.x509.Extension;
import org.bouncycastle.asn1.x509.KeyPurposeId;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import org.bouncycastle.cert.X509v3CertificateBuilder;
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

public class KeyStore {
	private static final Logger logger = LoggerFactory.getLogger(KeyStore.class);
	private static final String BC_STORE_TYPE = "BKS";
	private static final String BC_PROVIDER = "BC";
	private static final String ALGORITHM = "SHA256WithRSA";
	private static final int DAYS_VALID = 3650; // Ten years
	private static final String KEYSTORE_FORMAT = "%s.keystore";

	private final X509Certificate serverCert;

	@Inject
	KeyStore(X509Certificate serverCert) {
		this.serverCert = serverCert;
	}

	public SSLContext getSSLContextClient() {
		try {
			TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
			
			Set<TrustAnchor> anchors = new HashSet<>();
			anchors.add(new TrustAnchor(serverCert, null));
			PKIXBuilderParameters parameters = new PKIXBuilderParameters(anchors, new X509CertSelector());
			CertPathTrustManagerParameters spec = new CertPathTrustManagerParameters(parameters);
			trustManagerFactory.init(spec);
	
			SSLContext sslContext = SSLContext.getInstance("TLSv1.2");
			TrustManager[] trustManagers = trustManagerFactory.getTrustManagers();
			sslContext.init(null, trustManagers, null);
			return sslContext;
		} catch (NoSuchAlgorithmException e) {
			logger.error("Keystore algorithm issue when obtaining SSL context", e);
		} catch (KeyManagementException e) {
			logger.error("Keystore management issue when obtaining SSL context", e);
		} catch (InvalidAlgorithmParameterException e) {
			logger.error("Keystore invalid algorithm issue when obtaining SSL context", e);
		}
		return null;
	}
	
	public X509Certificate createKey(String username, String keyPass) {
		try {
			KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA", "BC");
			keyPairGenerator.initialize(1024, new SecureRandom());
			KeyPair keyPair = keyPairGenerator.generateKeyPair();
			
			X500NameBuilder nameBuilder = new X500NameBuilder();
			nameBuilder.addRDN(new ASN1ObjectIdentifier("2.5.4.3"), username);
			X500Name name = nameBuilder.build();
			X509v3CertificateBuilder certGen = new X509v3CertificateBuilder(
					name,
					BigInteger.valueOf(System.currentTimeMillis()),
					new Date(System.currentTimeMillis() - 24 * 60 * 60 * 1000),
					new Date(System.currentTimeMillis() + DAYS_VALID * 24 * 60 * 60 * 1000),
					name,
					SubjectPublicKeyInfo.getInstance(keyPair.getPublic().getEncoded()));
			certGen.addExtension(Extension.extendedKeyUsage, true, KeyPurposeId.id_kp_timeStamping);
			
			X509Certificate cert =
					new JcaX509CertificateConverter()
						.setProvider("BC")
						.getCertificate(
								certGen.build(
										new JcaContentSignerBuilder(ALGORITHM)
											.setProvider("BC")
											.build(keyPair.getPrivate())));
	
	        X509Certificate[] chain = new X509Certificate[1];
	
	        chain[0] = cert;
	
	        java.security.KeyStore singleUserKeyStore = java.security.KeyStore.getInstance(BC_STORE_TYPE, BC_PROVIDER);
	        singleUserKeyStore.load(null, keyPass.toCharArray());
	        singleUserKeyStore.setKeyEntry(username, keyPair.getPrivate(), keyPass.toCharArray(), chain);
	        singleUserKeyStore.store(new FileOutputStream(String.format(KEYSTORE_FORMAT, username)), keyPass.toCharArray());
	        return chain[0];
		} catch (KeyStoreException e) {
			logger.error("Keystore error when creating key", e);
		} catch (IOException e) {
			logger.error("I/O error when creating key", e);
		} catch (NoSuchProviderException e) {
			logger.error("No provider when creating key", e);
		} catch (NoSuchAlgorithmException e) {
			logger.error("No algorithm when creating key", e);
		} catch (CertificateException e) {
			logger.error("Certificate issue when creating key", e);
		} catch (OperatorCreationException e) {
			logger.error("Operator creation issue when creating key", e);
		}
		return null;
	}

	public PrivateKey getKey(String username, String keyPass) {
		try {
			java.security.KeyStore singleUserKeyStore = java.security.KeyStore.getInstance(BC_STORE_TYPE, BC_PROVIDER);
			singleUserKeyStore.load(new FileInputStream(String.format(KEYSTORE_FORMAT, username)), keyPass.toCharArray());
			Key key = singleUserKeyStore.getKey(username, keyPass.toCharArray());
			if (!(key instanceof PrivateKey)) {
				logger.error("Cannot get key: Could not obtain a private key");
				return null;
			}
			return (PrivateKey) key;
		} catch (NoSuchAlgorithmException e) {
			logger.error("No algorithm when getting key", e);
		} catch (CertificateException e) {
			logger.error("Certificate issue when getting key", e);
		} catch (IOException e) {
			logger.error("I/O issue when getting key", e);
		} catch (KeyStoreException e) {
			logger.error("Keystore issue when getting key", e);
		} catch (UnrecoverableKeyException e) {
			logger.error("Unrecoverable key issue when getting key", e);
		} catch (NoSuchProviderException e) {
			logger.error("No provider issue when getting key", e);
		}
		return null;
	}
}
