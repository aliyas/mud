package org.fourchan.d.mud.client;

import java.net.URI;

import org.fourchan.d.mud.client.controller.FXMLModule;
import org.fourchan.d.mud.client.keys.KeyStoreModule;
import org.fourchan.d.mud.common.messages.ClientMessage;
import org.fourchan.d.mud.common.messages.ClientMessageDeserializer;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Scopes;
import com.google.inject.Singleton;

public class ClientModule extends AbstractModule {
	private final StockadeCommandLine commandLine;
	
	public ClientModule(StockadeCommandLine commandLine) {
		this.commandLine = commandLine;
	}

	@Override
	protected void configure() {
		bind(StockadeCommandLine.class).toInstance(commandLine);
		bind(StockadeWebsocketClient.class).in(Scopes.SINGLETON);
		bind(ClientMessageHandler.class).to(StockadeClientImpl.class);
		bind(ClientMessageSender.class).to(StockadeClientImpl.class);
		bind(StockadeClientImpl.class).in(Scopes.SINGLETON);
		install(new KeyStoreModule());
		install(new FXMLModule());
	}

	@Provides
	@Singleton
	public URI provideServerURI() {
		return URI.create(String.format("wss://%s:%d", commandLine.getHost(), commandLine.getPort()));
	}

	@Provides
	@Singleton
	Gson provideGson() {
		return new GsonBuilder()
				.registerTypeAdapter(ClientMessage.class, new ClientMessageDeserializer())
				.create();
	}
}
