package org.fourchan.d.mud.client.controller;

import java.util.List;
import java.util.Optional;

import javax.inject.Inject;

import org.fourchan.d.mud.common.messages.common.Item;
import org.fourchan.d.mud.common.messages.responses.InventoryResponse;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.control.ScrollPane;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;

public class FXMLInventoryScreen extends VBox {
	private final GameSceneChanger sceneChanger;
	private Optional<Pane> selectedPane;
	private List<Item> latestInventory;

	@Inject
	public FXMLInventoryScreen(GameSceneChanger sceneChanger) {
		this.sceneChanger = sceneChanger;
		this.selectedPane = Optional.empty();
	}

	public void handleInventoryResponse(InventoryResponse response) {
		latestInventory = response.getItems();
		if (latestInventory != null && !latestInventory.isEmpty()) {
			drawItems();
		} else {
			clearDetailPane();
			itemBox.getChildren().clear();
			itemBox.getChildren().add(new Text("No items in your inventory."));
		}
	}

	@FXML ScrollPane scrollPane;
	@FXML VBox itemBox;
	@FXML GridPane detailPane;
	@FXML HBox inventoryPane;

	public void initialize() {
		inventoryPane.prefWidthProperty().bind(this.widthProperty());
	}

	@FXML
	public void handleBackButtonPressed() {
		sceneChanger.toMainGameScreen();
	}

	private void drawItems() {
		clearDetailPane();
		itemBox.getChildren().clear();
		for (Item item : latestInventory) {
			HBox itemPane = new HBox(5);
			itemPane.getChildren().add(new Text(item.name));
			itemPane.getChildren().add(new Text(item.description));
			itemPane.getChildren().add(new Text(String.format("x%d", item.quantity)));
			itemPane.prefWidthProperty().bind(itemBox.widthProperty());
			itemPane.setOnMouseEntered(this.getMouseEnteredFunction(item));
			itemPane.setOnMouseExited(this.getMouseExitedFunction(item));
			itemPane.setOnMouseClicked(this.getMouseClickedFunction(item));
			itemBox.getChildren().add(itemPane);
		}
	}

	private void setDetailPane(Item item) {
		clearDetailPane();
		Text nameText = null;
		if (item.quantity > 1) {
			nameText = new Text(String.format("%s x%d", item.name, item.quantity));
		} else {
			nameText = new Text(item.name);
		}
		Text descriptionText = new Text(item.description);
		detailPane.add(nameText, 0, 0, 2, 1);
		detailPane.add(descriptionText, 0, 1, 2, 1);
	}

	private void clearDetailPane() {
		detailPane.getChildren().clear();
	}

	private EventHandler<MouseEvent> getMouseEnteredFunction(Item item) {
		return e -> {
			if (!selectedPane.isPresent()) {
				Pane pane = (Pane) e.getSource();
				pane.setBackground(new Background(new BackgroundFill(Color.YELLOW, CornerRadii.EMPTY, Insets.EMPTY)));
				setDetailPane(item);
			}
		};
	}

	private EventHandler<MouseEvent> getMouseExitedFunction(Item item) {
		return e -> {
			if (!selectedPane.isPresent()) {
				Pane pane = (Pane) e.getSource();
				pane.setBackground(Background.EMPTY);
				clearDetailPane();
			}
		};
	}

	private EventHandler<MouseEvent> getMouseClickedFunction(Item item) {
		return e -> {
			Pane pane = (Pane) e.getSource();
			if (!selectedPane.isPresent()) {
				selectPoint(item, pane);
			} else if (pane.equals(selectedPane.get())) {
				deselectPoint();
			}
		};
	}

	private void selectPoint(Item item, Pane pane) {
		selectedPane = Optional.of(pane);
		pane.setBackground(new Background(new BackgroundFill(Color.GREEN, CornerRadii.EMPTY, Insets.EMPTY)));
	}

	private void deselectPoint() {
		selectedPane.ifPresent(p -> {
			p.setBackground(new Background(new BackgroundFill(Color.YELLOW, CornerRadii.EMPTY, Insets.EMPTY)));
			selectedPane = Optional.empty();
		});
	}
}
