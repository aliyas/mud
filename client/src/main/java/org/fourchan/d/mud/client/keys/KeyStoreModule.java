package org.fourchan.d.mud.client.keys;

import java.io.InputStream;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Scopes;
import com.google.inject.Singleton;

public class KeyStoreModule extends AbstractModule {

	@Override
	protected void configure() {
		bind(KeyStore.class).in(Scopes.SINGLETON);
	}

	@Provides
	@Singleton
	public X509Certificate provideSelfSignedCert() throws Exception {
		X509Certificate myGoddamnSelfSignedCert = null;
		InputStream inputStream = KeyStoreModule.class.getResourceAsStream("/org/fourchan/d/mud/client/keys/server.cer");
		if (inputStream == null) {
			throw new RuntimeException("Could not find the cert");
		}
		CertificateFactory cf = CertificateFactory.getInstance("X.509");
		myGoddamnSelfSignedCert = (X509Certificate)cf.generateCertificate(inputStream);
		return myGoddamnSelfSignedCert;
	}
}
