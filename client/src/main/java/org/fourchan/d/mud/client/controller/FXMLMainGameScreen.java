package org.fourchan.d.mud.client.controller;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.inject.Inject;

import org.fourchan.d.mud.client.ClientMessageSender;
import org.fourchan.d.mud.client.controller.FXMLModule.AllMapPoints;
import org.fourchan.d.mud.client.controller.FXMLModule.EntitiesToText;
import org.fourchan.d.mud.client.controller.FXMLModule.MapPointIdsToText;
import org.fourchan.d.mud.client.game.ClientGameState;
import org.fourchan.d.mud.common.map.Direction;
import org.fourchan.d.mud.common.map.MapPointId;
import org.fourchan.d.mud.common.map.Point;
import org.fourchan.d.mud.common.map.Visibility;
import org.fourchan.d.mud.common.messages.common.Entity;
import org.fourchan.d.mud.common.messages.common.MapPoint;
import org.fourchan.d.mud.common.messages.responses.ActionPayload;
import org.fourchan.d.mud.common.messages.responses.EntityPayload;
import org.fourchan.d.mud.common.messages.responses.MapPayload;
import org.fourchan.d.mud.common.messages.responses.TextPayload;

import com.google.common.base.Optional;
import com.google.common.collect.ImmutableMultimap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;

import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;

public class FXMLMainGameScreen extends VBox {
	private static final int MIN_MAP_GRID_PANE_SIZE = 7;
	private static final double PREF_PIXEL_SIZE_PER_POINT = 20;
	private final GameSceneChanger gameSceneChanger;
	private final ClientMessageSender clientMessageSender;
	private final Map<MapPointId, String> displayMap;
	private final Map<Entity.Type, String> entityMap;
	private final List<MapPointId> mapPoints;
	private final FXMLActionButtonsController fxmlActionButtonsController;
	private ClientGameState clientGameState;
	private Point selectedPoint;
	private Pane selectedPane;
	private MapPayload latestMapPayload;
	private EntityPayload latestEntityPayload;

	@Inject
	public FXMLMainGameScreen(
			FXMLActionButtonsController fxmlActionButtonsController,
			GameSceneChanger gameSceneChanger,
			ClientMessageSender clientMessageSender,
			@MapPointIdsToText Map<MapPointId, String> displayMap,
			@EntitiesToText Map<Entity.Type, String> entityMap,
			@AllMapPoints List<MapPointId> mapPoints) {
		this.gameSceneChanger = gameSceneChanger;
		this.clientMessageSender = clientMessageSender;
		this.displayMap = displayMap;
		this.entityMap = entityMap;
		this.mapPoints = mapPoints;
		this.fxmlActionButtonsController = fxmlActionButtonsController;
	}

	@FXML GridPane mapGridPane;
	@FXML Text locationTitle;
	@FXML Text locationCharacter;
	@FXML Text locationType;

	@FXML GridPane mapEditUi;
	@FXML Text mapEditLocation;
	@FXML Text mapEditLocationType;
	@FXML Text mapEditIsSelected;
	@FXML ComboBox<MapPointId> mapEditNewLocationType;
	@FXML Button mapEditRequestButton;

	@FXML TextArea descriptionBox;
	@FXML TextArea mainTextArea;

	@FXML VBox actionList;

	public void initialize() {
		synchronizeProcess();
	}

	public void handleMapPayload(MapPayload payload) {
		latestMapPayload = payload;
		drawMap();
	}

	public void handleEntityPayload(EntityPayload payload) {
		latestEntityPayload = payload;
		drawMap();
	}

	public void handleActionPayload(ActionPayload payload) {
		fxmlActionButtonsController.handleActionPayload(payload);
	}

	public void handleTextPayload(TextPayload payload) {
		mainTextArea.appendText(String.format("\n%s", payload.getText()));
	}

	private void drawMap() {
		if (latestMapPayload == null || latestEntityPayload == null) {
			return;
		}
		mapEditNewLocationType.getItems().setAll(mapPoints);

		Optional<Point> maybePreviouslySelectedPoint = Optional.fromNullable(selectedPoint);
		Optional<Pane> maybePreviouslySelectedPane = Optional.absent();
		Optional<String> maybePreviouslySelectedDescription = Optional.absent();
		deselectPoint();
		descriptionBox.setText("");

		Point currentLocation = latestMapPayload.getCurrentLocation();
		MapPoint currentMapPoint =
				latestMapPayload.getPoints()
				.stream()
				.filter(p -> p.point.getX() == currentLocation.getX()
						&& p.point.getY() == currentLocation.getY()
						&& p.point.getZ() == currentLocation.getZ())
				.findFirst()
				.get();
		locationCharacter.setText(currentLocation.toString());
		locationType.setText(currentMapPoint.pointId.toString());
		List<MapPoint> points =
				latestMapPayload.getPoints()
						.stream()
						.filter(p -> p.point.getZ() == currentLocation.getZ())
						.filter(p -> p.point.getX() != currentLocation.getX() || p.point.getY() != currentLocation.getY())
						.collect(Collectors.toList());
		int minX = currentLocation.getX() - Visibility.DEFAULT_XY_DISTANCE;
		int minY = currentLocation.getY() - Visibility.DEFAULT_XY_DISTANCE;
		int maxX = currentLocation.getX() + Visibility.DEFAULT_XY_DISTANCE;
		int maxY = currentLocation.getY() + Visibility.DEFAULT_XY_DISTANCE;
		mapGridPane.setPrefWidth(PREF_PIXEL_SIZE_PER_POINT *
				maxX - minX < MIN_MAP_GRID_PANE_SIZE ?
						(double) MIN_MAP_GRID_PANE_SIZE
						: (double) (maxX - minX));
		int gridHeight = 
				maxY - minY < MIN_MAP_GRID_PANE_SIZE ?
						MIN_MAP_GRID_PANE_SIZE
						: maxY - minY;
		mapGridPane.setPrefHeight(PREF_PIXEL_SIZE_PER_POINT * (double) gridHeight);
		mapGridPane.getChildren().clear();

		// Map locations to entities
		Multimap<Point, Entity> entityLocationMap = ImmutableMultimap.of();
		if (latestEntityPayload.getEntities() != null
			&& !latestEntityPayload.getEntities().isEmpty()) {
			entityLocationMap = Multimaps.index(latestEntityPayload.getEntities(), (Entity e) -> e.location);
		}

		ImmutableSet.Builder<Point> mappedPointsBuilder = ImmutableSet.builder();
		for (MapPoint p : points) {
			int columnIndex = p.point.getX() - minX;
			int rowIndex = gridHeight - (p.point.getY() - minY);
			Entity firstEntity = Iterables.getFirst(entityLocationMap.get(p.point), null);
			Label label = null;
			Optional<String> description = Optional.absent();
			if (firstEntity != null) {
				label = new Label(entityMap.get(firstEntity.type));
				description =
						Optional.of(
								entityLocationMap.get(p.point).stream()
										.map((Entity e) -> e.description)
										.collect(Collectors.joining("\n\n")));
			} else {
				label = new Label(displayMap.get(p.pointId));
			}
			Pane pane = new FlowPane();
			pane.setOnMouseEntered(this.getMouseEnteredFunction(p, description));
			pane.setOnMouseExited(this.getMouseExitedFunction(p));
			pane.setOnMouseClicked(this.getMouseClickedFunction(p));
			pane.getChildren().add(label);
			setMapCellSize(pane);
			GridPane.setFillWidth(pane, true);
			GridPane.setFillHeight(pane, true);
			mapGridPane.add(pane, columnIndex, rowIndex);
			mappedPointsBuilder.add(p.point);

			// Find point that was selected
			if (maybePreviouslySelectedPoint.isPresent()
					&& maybePreviouslySelectedPoint.get().equals(p.point)) {
				maybePreviouslySelectedPane = Optional.of(pane);
				maybePreviouslySelectedDescription = description;
			}
		}
		ImmutableSet<Point> mappedPoints = mappedPointsBuilder.build();

		// Also populate empty grid cells
		for (int columnIndex = 0; minX + columnIndex <= maxX; columnIndex++) {
			for (int rowIndex = 0; minY + rowIndex <= maxY; rowIndex++) {
				Point currentGamePoint = new Point(minX + columnIndex, (gridHeight - rowIndex) + minY, currentLocation.getZ());
				if (mappedPoints.contains(currentGamePoint)) {
					continue;
				} else if (currentGamePoint.equals(currentLocation)) {
					continue;
				}
				Pane pane = new FlowPane();
				pane.setOnMouseEntered(this.getMouseEnteredNoPointFunction(currentGamePoint));
				pane.setOnMouseExited(this.getMouseExitedNoPointFunction(currentGamePoint));
				pane.setOnMouseClicked(this.getMouseClickedNoPointFunction(currentGamePoint));
				pane.getChildren().add(new Label(" "));
				setMapCellSize(pane);
				GridPane.setFillWidth(pane, true);
				GridPane.setFillHeight(pane, true);
				mapGridPane.add(pane, columnIndex, rowIndex);

				// Find point that was selected
				if (maybePreviouslySelectedPoint.isPresent()
						&& maybePreviouslySelectedPoint.get().equals(currentGamePoint)) {
					maybePreviouslySelectedPane = Optional.of(pane);
				}
			}
		}
		Optional<String> myLocationDescription = Optional.absent();
		if (!entityLocationMap.get(currentLocation).isEmpty()) {
			myLocationDescription =
					Optional.of(
							entityLocationMap.get(currentLocation).stream()
									.map((Entity e) -> e.description)
									.collect(Collectors.joining("\n\n")));
		}
		Pane pane = new FlowPane();
		pane.setOnMouseEntered(this.getMouseEnteredFunction(currentMapPoint, myLocationDescription));
		pane.setOnMouseExited(this.getMouseExitedFunction(currentMapPoint));
		pane.setOnMouseClicked(this.getMouseClickedFunction(currentMapPoint));
		pane.getChildren().add(new Label("@"));
		setMapCellSize(pane);
		mapGridPane.add(
				pane,
				currentLocation.getX() - minX,
				currentLocation.getY() - minY);
		GridPane.setFillWidth(pane, true);
		GridPane.setFillHeight(pane, true);
		// Find point that was selected
		if (maybePreviouslySelectedPoint.isPresent()
				&& maybePreviouslySelectedPoint.get().equals(currentLocation)) {
			maybePreviouslySelectedPane = Optional.of(pane);
			maybePreviouslySelectedDescription = myLocationDescription;
		}

		// Reselect anything that was selected
		if (maybePreviouslySelectedPoint.isPresent()
				&& maybePreviouslySelectedPane.isPresent()) {
			selectPoint(maybePreviouslySelectedPoint.get(), maybePreviouslySelectedPane.get());
			if (maybePreviouslySelectedDescription.isPresent()) {
				descriptionBox.setText(maybePreviouslySelectedDescription.get());
			}
		}
	}

	@FXML
	public void handleGoNorth() {
		clientMessageSender.doMove(Direction.N);
	}

	@FXML
	public void handleGoSouth() {
		clientMessageSender.doMove(Direction.S);
	}

	@FXML
	public void handleGoEast() {
		clientMessageSender.doMove(Direction.E);
	}

	@FXML
	public void handleGoWest() {
		clientMessageSender.doMove(Direction.W);
	}

	@FXML
	public void handleAccountOverview() {
		gameSceneChanger.toCharacterSelection();
	}

	@FXML
	public void handleEditPoint() {
		clientMessageSender.doMapUpdateRequest(this.selectedPoint, mapEditNewLocationType.getValue());
	}

	@FXML
	public void handleInventory() {
		gameSceneChanger.toInventoryScreen();
	}

	private EventHandler<MouseEvent> getMouseEnteredFunction(MapPoint p, Optional<String> description) {
		return e -> {
			if (selectedPoint == null) {
				Pane pane = (Pane) e.getSource();
				pane.setBackground(new Background(new BackgroundFill(Color.YELLOW, CornerRadii.EMPTY, Insets.EMPTY)));
				mapEditLocation.setText(p.point.toString());
				mapEditLocationType.setText(p.pointId.toString());
				mapEditNewLocationType.getSelectionModel().select(p.pointId);
				if (description.isPresent()) {
					descriptionBox.setText(description.get());
				}
			}
		};
	}

	private EventHandler<MouseEvent> getMouseExitedFunction(MapPoint p) {
		return e -> {
			if (selectedPoint == null) {
				Pane pane = (Pane) e.getSource();
				pane.setBackground(Background.EMPTY);
				mapEditLocation.setText("");
				mapEditLocationType.setText("");
				mapEditNewLocationType.getSelectionModel().clearSelection();
				descriptionBox.setText("");
			}
		};
	}

	private EventHandler<MouseEvent> getMouseClickedFunction(MapPoint p) {
		return e -> {
			if (selectedPoint == null) {
				Pane pane = (Pane) e.getSource();
				selectPoint(p.point, pane);
			} else if (p.point.equals(selectedPoint)) {
				deselectPoint();
			}
		};
	}

	private EventHandler<MouseEvent> getMouseEnteredNoPointFunction(Point p) {
		return e -> {
			if (selectedPoint == null) {
				Pane pane = (Pane) e.getSource();
				pane.setBackground(new Background(new BackgroundFill(Color.YELLOW, CornerRadii.EMPTY, Insets.EMPTY)));
				mapEditLocation.setText(p.toString());
				mapEditLocationType.setText("N/A");
				mapEditNewLocationType.getSelectionModel().selectFirst();
			}
		};
	}

	private EventHandler<MouseEvent> getMouseExitedNoPointFunction(Point p) {
		return e -> {
			if (selectedPoint == null) {
				Pane pane = (Pane) e.getSource();
				pane.setBackground(Background.EMPTY);
				mapEditLocation.setText("");
				mapEditLocationType.setText("");
				mapEditNewLocationType.getSelectionModel().clearSelection();
			}
		};
	}

	private EventHandler<MouseEvent> getMouseClickedNoPointFunction(Point p) {
		return e -> {
			if (selectedPoint == null) {
				Pane pane = (Pane) e.getSource();
				selectPoint(p, pane);
			} else if (p.equals(selectedPoint)) {
				deselectPoint();
			}
		};
	}

	private void selectPoint(Point p, Pane pane) {
		selectedPoint = p;
		selectedPane = pane;
		selectedPane.setBackground(new Background(new BackgroundFill(Color.GREEN, CornerRadii.EMPTY, Insets.EMPTY)));
		mapEditIsSelected.setText("Yes");
		mapEditNewLocationType.setDisable(false);
		mapEditRequestButton.setDisable(false);
	}

	private void deselectPoint() {
		if (selectedPane != null) {
			selectedPane.setBackground(new Background(new BackgroundFill(Color.YELLOW, CornerRadii.EMPTY, Insets.EMPTY)));
			selectedPane = null;
		}
		selectedPoint = null;
		mapEditIsSelected.setText("No");
		mapEditNewLocationType.setDisable(true);
		mapEditRequestButton.setDisable(true);
	}

	private void setMapCellSize(Pane pane) {
		pane.setPrefWidth(PREF_PIXEL_SIZE_PER_POINT);
		pane.setMinWidth(PREF_PIXEL_SIZE_PER_POINT);
		pane.setMaxWidth(PREF_PIXEL_SIZE_PER_POINT);
		pane.setPrefHeight(PREF_PIXEL_SIZE_PER_POINT);
		pane.setMinHeight(PREF_PIXEL_SIZE_PER_POINT);
		pane.setMaxHeight(PREF_PIXEL_SIZE_PER_POINT);
	}

	public void synchronize(ClientGameState clientGameState) {
		this.clientGameState = clientGameState;
		synchronizeProcess();
	}

	private void synchronizeProcess() {
		if (mapEditUi != null) {
			clientGameState.getCharacterName().ifPresent(
					text -> Platform.runLater(() -> locationTitle.setText(text)));
			Platform.runLater(() -> {
				mapEditUi.setVisible(clientGameState.canPlayerEditMap());
				mapEditUi.setManaged(clientGameState.canPlayerEditMap());
			});
		}
	}
}
