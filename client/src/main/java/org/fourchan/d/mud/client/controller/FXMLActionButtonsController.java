package org.fourchan.d.mud.client.controller;

import org.fourchan.d.mud.client.ClientMessageSender;
import org.fourchan.d.mud.common.messages.ActionMessage;
import org.fourchan.d.mud.common.messages.common.Action;
import org.fourchan.d.mud.common.messages.requests.ExamineEntityRequest;
import org.fourchan.d.mud.common.messages.responses.ActionPayload;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

import javafx.beans.property.DoubleProperty;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

public class FXMLActionButtonsController {
	private static final Logger logger = LoggerFactory.getLogger(FXMLActionButtonsController.class);
	private final ClientMessageSender clientMessageSender;
	private ActionPayload latestActionPayload;

	@Inject
	public FXMLActionButtonsController(ClientMessageSender clientMessageSender) {
		this.clientMessageSender = clientMessageSender;
	}

	@FXML VBox root;
	@FXML ScrollPane scrollPane;
	@FXML VBox actionListBox;

	public void initialize() {
		scrollPane.prefHeightProperty().bind(root.heightProperty());
		doHandlePayload();
	}

	public void handleActionPayload(ActionPayload payload) {
		latestActionPayload = payload;
		doHandlePayload();
	}

	private void doHandlePayload() {
		if (latestActionPayload == null || actionListBox == null) {
			return;
		}
		if (latestActionPayload.getActions() != null) {
			setActions();
		} else {
			actionListBox.getChildren().clear();
			actionListBox.getChildren().add(new Text("No actions available."));
		}
	}

	private void setActions() {
		actionListBox.getChildren().clear();
		for (Action action : latestActionPayload.getActions()) {
			Button actionButton = new Button();
			VBox vbox = new VBox(5);
			vbox.getChildren().add(getActionTitle(action, actionButton.prefWidthProperty()));
			vbox.getChildren().add(getActionDescription(action, actionButton.prefWidthProperty()));
			actionButton.setGraphic(vbox);
			actionButton.setOnMouseClicked(getActionButtonClicked(action));
			actionButton.prefWidthProperty().bind(actionListBox.widthProperty());
			actionListBox.getChildren().add(actionButton);
		}
	}

	private EventHandler<MouseEvent> getActionButtonClicked(Action action) {
		return e -> {
			ActionMessage message = getActionMessage(action);
			if (message == null) {
				logger.error("Failed to getActionMessage: {}", action);
				return;
			}
			clientMessageSender.doActionMessage(message);
		};
	}

	private ActionMessage getActionMessage(Action action) {
		switch (action.actionType) {
		case EXAMINE:
			return new ExamineEntityRequest(action.targetEntityType, action.targetEntityId);
		default:
			return null;
		}
	}

	private Text getActionTitle(Action action, DoubleProperty wrappingWidthProperty) {
		Text text = new Text(action.title);
		text.wrappingWidthProperty().bind(wrappingWidthProperty);
		return text;
	}

	private Text getActionDescription(Action action, DoubleProperty wrappingWidthProperty) {
		Text text = new Text(action.description);
		text.wrappingWidthProperty().bind(wrappingWidthProperty);
		return text;
	}
}
