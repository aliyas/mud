package org.fourchan.d.mud.client.controller;

import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;

import org.fourchan.d.mud.client.ClientMessageSender;
import org.fourchan.d.mud.client.SceneChanger;
import org.fourchan.d.mud.client.keys.KeyStore;
import org.fourchan.d.mud.client.ui.GuestPromotionController;
import org.fourchan.d.mud.client.ui.StatusMessager;
import org.fourchan.d.mud.common.messages.responses.PromoteGuestToNonGuestResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Strings;
import com.google.inject.Inject;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

public class FXMLPromoteGuestController implements GuestPromotionController {
	private static final Logger logger = LoggerFactory.getLogger(FXMLRegisterController.class);
	private final KeyStore keyStore;
	private final SceneChanger sceneChanger;
	private final ClientMessageSender client;
	private final StatusMessager statusMessager;

	@Inject
	public FXMLPromoteGuestController(
			KeyStore keyStore,
			SceneChanger sceneChanger,
			ClientMessageSender client,
			StatusMessager statusMessager) {
		this.keyStore = keyStore;
		this.sceneChanger = sceneChanger;
		this.client = client;
		this.statusMessager = statusMessager;
	}

	@FXML private TextField usernameField;
	@FXML private PasswordField passwordField;
	@FXML private PasswordField verifyPasswordField;

	@FXML
	protected void handleConvertButtonAction(ActionEvent actionEvent) {
		// TODO: Clean this up by unifying with the logic in the register controller
		String username = Strings.nullToEmpty(usernameField.getText()).trim();
		String password = Strings.nullToEmpty(passwordField.getText()).trim();
		String verifyPassword = Strings.nullToEmpty(verifyPasswordField.getText()).trim();
		if (username.isEmpty()) {
			statusMessager.setErrorStatusMessage("Please enter a username.");
			return;
		} else if (!username.matches("[a-zA-Z]*")) {
			statusMessager.setErrorStatusMessage("Username can only contain lower/upper case latin letters.");
			return;
		} else if (!password.equals(verifyPassword)) {
			statusMessager.setErrorStatusMessage("Passwords do not match");
			return;
		}
		X509Certificate cert = keyStore.createKey(username, password);
		if (cert != null) {
			try {
				client.doPromoteGuestToNonGuest(username, cert);
			} catch (CertificateEncodingException e) {
				logger.error("Could not register due to encoding cert exception", e);
				statusMessager.setErrorStatusMessage("Error encoding certificate to server.");
				return;
			}
		} else {
			statusMessager.setErrorStatusMessage("Error creating private key & certificate.");
			return;
		}
	}

	@FXML
	protected void handleBackButtonPressed(ActionEvent actionEvent) {
		clear();
		sceneChanger.goBackOneScene();
	}

	@Override
	public void handlePromoteGuestToNonGuestResponse(PromoteGuestToNonGuestResponse response) {
		if (response.isSuccessful()) {
			clear();
		}
	}

	private void clear() {
		usernameField.clear();
		passwordField.clear();
		verifyPasswordField.clear();
	}
}
