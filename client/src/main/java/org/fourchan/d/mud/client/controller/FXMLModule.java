package org.fourchan.d.mud.client.controller;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

import org.fourchan.d.mud.client.SceneChanger;
import org.fourchan.d.mud.client.ui.GameController;
import org.fourchan.d.mud.client.ui.GuestController;
import org.fourchan.d.mud.client.ui.GuestPromotionController;
import org.fourchan.d.mud.client.ui.LoginController;
import org.fourchan.d.mud.client.ui.LogoutController;
import org.fourchan.d.mud.client.ui.LogoutInitiator;
import org.fourchan.d.mud.client.ui.PingController;
import org.fourchan.d.mud.client.ui.RegisterController;
import org.fourchan.d.mud.client.ui.StatusMessager;
import org.fourchan.d.mud.common.map.MapPointId;
import org.fourchan.d.mud.common.messages.common.Entity;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.inject.AbstractModule;
import com.google.inject.BindingAnnotation;
import com.google.inject.Provides;
import com.google.inject.Scopes;

public class FXMLModule extends AbstractModule {

	@BindingAnnotation
	@Retention(RetentionPolicy.RUNTIME)
	@Target({ ElementType.FIELD, ElementType.PARAMETER, ElementType.METHOD })
	public @interface UiScheduledThreadPool {}

	@BindingAnnotation
	@Retention(RetentionPolicy.RUNTIME)
	@Target({ ElementType.FIELD, ElementType.PARAMETER, ElementType.METHOD })
	public @interface MapPointIdsToText {}

	@BindingAnnotation
	@Retention(RetentionPolicy.RUNTIME)
	@Target({ ElementType.FIELD, ElementType.PARAMETER, ElementType.METHOD })
	public @interface EntitiesToText {}

	@BindingAnnotation
	@Retention(RetentionPolicy.RUNTIME)
	@Target({ ElementType.FIELD, ElementType.PARAMETER, ElementType.METHOD })
	public @interface AllMapPoints {}

	@Override
	protected void configure() {
		bind(SceneChanger.class).to(FXMLRootController.class);
		bind(LoginController.class).to(FXMLLoginController.class);
		bind(RegisterController.class).to(FXMLRegisterController.class);
		bind(LogoutController.class).to(FXMLMainGameController.class);
		bind(LogoutInitiator.class).to(FXMLMainGameController.class);
		bind(GameController.class).to(FXMLMainGameController.class);
		bind(GameSceneChanger.class).to(FXMLMainGameController.class);
		bind(PingController.class).to(FXMLPingIndicator.class);
		bind(GuestController.class).to(FXMLGuestAckController.class);
		bind(GuestPromotionController.class).to(FXMLPromoteGuestController.class);
		bind(StatusMessager.class).to(FXMLMainGameController.class);
		bind(FXMLActionButtonsController.class).in(Scopes.SINGLETON);
		bind(FXMLCharacterCreationController.class).in(Scopes.SINGLETON);
		bind(FXMLCharacterSelectionController.class).in(Scopes.SINGLETON);
		bind(FXMLGuestAckController.class).in(Scopes.SINGLETON);
		bind(FXMLInventoryScreen.class).in(Scopes.SINGLETON);
		bind(FXMLLoginController.class).in(Scopes.SINGLETON);
		bind(FXMLMainGameController.class).in(Scopes.SINGLETON);
		bind(FXMLMainGameScreen.class).in(Scopes.SINGLETON);
		bind(FXMLPingIndicator.class).in(Scopes.SINGLETON);
		bind(FXMLPromoteGuestController.class).in(Scopes.SINGLETON);
		bind(FXMLRegisterController.class).in(Scopes.SINGLETON);
		bind(FXMLRootController.class).in(Scopes.SINGLETON);
	}

	@Provides
	@UiScheduledThreadPool
	public ScheduledExecutorService provideUiScheduledThreadPool() {
		return Executors.newSingleThreadScheduledExecutor();
	}

	@Provides
	@MapPointIdsToText
	public Map<MapPointId, String> provideMapPointIdsToText() {
		return ImmutableMap.<MapPointId, String>builder()
			.put(MapPointId.GRASS, ".")
			.put(MapPointId.STONE_CLIFF, "\u2588")
			.build();
	}

	@Provides
	@EntitiesToText
	public Map<Entity.Type, String> provideEntitiesToText() {
		return ImmutableMap.<Entity.Type, String>builder()
				.put(Entity.Type.HUMAN_PLAYER_CHARACTER, "H")
				.build();
	}

	@Provides
	@AllMapPoints
	public List<MapPointId> provideAllMapPoints() {
		return ImmutableList.copyOf(MapPointId.values());
	}
}
