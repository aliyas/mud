package org.fourchan.d.mud.client.ui;

import org.fourchan.d.mud.common.messages.responses.PromoteGuestToNonGuestResponse;

public interface GuestPromotionController {
	void handlePromoteGuestToNonGuestResponse(PromoteGuestToNonGuestResponse response);
}
