package org.fourchan.d.mud.client;

import java.io.IOException;

public interface ClientMessageHandler {
	void handleMessage(String message);
	void connect() throws IOException, InterruptedException;
	void disconnect() throws InterruptedException;
	void onUnintentionalDisconnect();
}
