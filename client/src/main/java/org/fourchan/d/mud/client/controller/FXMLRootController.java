package org.fourchan.d.mud.client.controller;

import javax.inject.Inject;

import org.fourchan.d.mud.client.SceneChanger;
import org.fourchan.d.mud.client.fxml.FXMLLoaders;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Injector;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.layout.Pane;

public class FXMLRootController implements SceneChanger {
	private static Logger logger = LoggerFactory.getLogger(FXMLRootController.class);
	private Injector injector;
	private String currentScene;
	private String previousScene;

	@Inject
	public FXMLRootController(Injector injector) {
		this.injector = injector;
	}

	@FXML Pane mainPane;

	@Override
	public void changeScene(String name) {
		previousScene = currentScene;
		currentScene = name;
	    Node node = FXMLLoaders.load(name, injector);
		Platform.runLater(() -> mainPane.getChildren().setAll(node));
	}

	@Override
	public void goBackOneScene() {
		if (previousScene == null) {
			logger.warn("Previous scene is null");
			return;
		}
		changeScene(previousScene);
	}
}
