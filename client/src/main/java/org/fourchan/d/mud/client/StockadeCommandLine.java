package org.fourchan.d.mud.client;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

public class StockadeCommandLine {
	private static final String DEBUG_LONG_OPT = "debug";
	private static final Option DEBUG =
			Option.builder("d").required(false).longOpt(DEBUG_LONG_OPT).build();
	private static final String HOST_LONG_OPT = "host";
	private static final Option HOST =
			Option.builder().required(true).hasArg().longOpt(HOST_LONG_OPT).build();
	private static final String PORT_LONG_OPT = "port";
	private static final Option PORT =
			Option.builder().required(true).hasArg().longOpt(PORT_LONG_OPT).build();

    private static final CommandLineParser parser = new DefaultParser();
    private static final HelpFormatter formatter = new HelpFormatter();
    private final CommandLine commandLine;
    private final Options options;
	
	StockadeCommandLine(String[] args) throws ParseException {
		options = new Options();
        options.addOption(DEBUG);
        options.addOption(HOST);
        options.addOption(PORT);
        commandLine = parser.parse(options, args);
	}

	public void printHelp() {
		formatter.printHelp("stockade-client", options);
	}

	public boolean isDebugEnabled() {
		return toBoolean(commandLine.getOptionValue(DEBUG_LONG_OPT));
	}
	
	public String getHost() {
		return commandLine.getOptionValue(HOST_LONG_OPT);
	}

	public int getPort() {
		return toInt(commandLine.getOptionValue(PORT_LONG_OPT));
	}

	private static int toInt(String s) {
		return new Integer(s);
	}

	private boolean toBoolean(String s) {
		return s != null && commandLine.hasOption(s);
	}
}
