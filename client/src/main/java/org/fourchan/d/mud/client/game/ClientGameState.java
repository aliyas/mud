package org.fourchan.d.mud.client.game;

import java.util.List;
import java.util.Optional;

import org.fourchan.d.mud.common.messages.common.Character;
import org.fourchan.d.mud.common.messages.common.Player;
import org.fourchan.d.mud.common.messages.common.Player.PlayerKind;

public final class ClientGameState {
	private Player player;
	private Character selectedCharacter;
	
	public ClientGameState(Player player) {
		this.player = player;
	}

	public String getUsername() {
		return player.username;
	}

	public List<Character> getPlayerCharacters() {
		return player.characters;
	}

	public Optional<Character> getSelectedCharacter() {
		return Optional.ofNullable(selectedCharacter);
	}

	public Optional<String> getCharacterName() {
		if (selectedCharacter == null) {
			return Optional.empty();
		}
		return Optional.of(String.format("%s %s", selectedCharacter.firstName, selectedCharacter.lastName));
	}

	public void setSelectedCharacter(Character selectedCharacter) {
		this.selectedCharacter = selectedCharacter;
	}

	public boolean canPlayerEditMap() {
		return player.permissions.canEditMap;
	}

	public PlayerKind getPlayerKind() {
		return player.playerKind;
	}
}
