package org.fourchan.d.mud.client;

public interface SceneChanger {
	void changeScene(String name);
	void goBackOneScene();
}
