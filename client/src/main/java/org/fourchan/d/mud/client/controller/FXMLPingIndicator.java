package org.fourchan.d.mud.client.controller;

import java.time.Duration;
import java.time.Instant;

import javax.inject.Inject;

import org.fourchan.d.mud.client.ui.PingController;

import com.google.common.base.Strings;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.text.Text;

public class FXMLPingIndicator implements PingController {
	private static final int MAX_LENGTH = 16;
	private int reconnectAttempts;

	@Inject
	public FXMLPingIndicator() {}

	@FXML private Text pingDisplay;

	@Override
	public void onConnect() {
		reconnectAttempts = 0;
		setText("Connected");
	}

	@Override
	public void onDisconnect() {
		if (reconnectAttempts <= 0) {
			setText("Disconnected (will attempt reconnection)");
		}
	}

	@Override
	public void onPingPong(Instant pingTime, Instant pongTime) {
		Duration duration = Duration.between(pingTime, pongTime);
		setText(String.format("Ping: %d ms", duration.toMillis()));
	}

	@Override
	public void onReconnect() {
		StringBuilder builder = new StringBuilder("Reconnecting...");
		if (reconnectAttempts > 0) {
			builder.append(" 8");
			builder.append(Strings.repeat("=", reconnectAttempts));
			builder.append("D");
			if (reconnectAttempts == MAX_LENGTH - 1) {
				builder.append("~~");
			}
		}
		reconnectAttempts++;
		if (reconnectAttempts >= MAX_LENGTH) {
			reconnectAttempts = 0;
		}
		setText(builder.toString());
	}

	private void setText(String text) {
		Platform.runLater(() -> pingDisplay.setText(text));
	}
}
