package org.fourchan.d.mud.client.ui;

import org.fourchan.d.mud.common.messages.responses.ActionPayload;
import org.fourchan.d.mud.common.messages.responses.CharacterSelectedResponse;
import org.fourchan.d.mud.common.messages.responses.CreateCharacterResponse;
import org.fourchan.d.mud.common.messages.responses.EntityPayload;
import org.fourchan.d.mud.common.messages.responses.InventoryResponse;
import org.fourchan.d.mud.common.messages.responses.LoginResponse;
import org.fourchan.d.mud.common.messages.responses.MapPayload;
import org.fourchan.d.mud.common.messages.responses.MoveResponse;
import org.fourchan.d.mud.common.messages.responses.NewGuestResponse;
import org.fourchan.d.mud.common.messages.responses.PlayerInfoResponse;
import org.fourchan.d.mud.common.messages.responses.PromoteGuestToNonGuestResponse;
import org.fourchan.d.mud.common.messages.responses.SpawnPointResponse;
import org.fourchan.d.mud.common.messages.responses.TextPayload;

public interface GameController {
	void handleLoginResponse(LoginResponse response);
	void handlePlayerInfoResponse(PlayerInfoResponse response);
	void sendCreateCharacterRequest(
			String firstName,
			String lastName,
			String sexChoice,
			String spawnChoice);
	void handleCreateCharacterResponse(CreateCharacterResponse response);
	void handleCharacterSelectedResponse(CharacterSelectedResponse response);
	void handleMapPayload(MapPayload payload);
	void handleMoveResponse(MoveResponse response);
	void handleSpawnPointResponse(SpawnPointResponse response);
	void handleEntityPayload(EntityPayload payload);
	void handleActionPayload(ActionPayload payload);
	void handleTextPayload(TextPayload payload);
	void handleInventoryResponse(InventoryResponse response);
	void handleNewGuestResponse(NewGuestResponse response);
	void handlePromoteGuestToNonGuestResponse(PromoteGuestToNonGuestResponse response);
}
