package org.fourchan.d.mud.client.ui;

import java.time.Instant;

public interface PingController {
	void onConnect();
	void onDisconnect();
	void onPingPong(Instant pingTime, Instant pongTime);
	void onReconnect();
}
