package org.fourchan.d.mud.client.controller;

import org.fourchan.d.mud.client.ClientMessageSender;
import org.fourchan.d.mud.client.SceneChanger;
import org.fourchan.d.mud.client.fxml.FXMLLoaders;
import org.fourchan.d.mud.client.ui.GuestController;

import com.google.inject.Inject;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;

public class FXMLGuestAckController implements GuestController {
	private static final String ACK_TEXT = "A guest account is.... etc";

	private final SceneChanger sceneChanger;
	private final ClientMessageSender client;

	@Inject
	public FXMLGuestAckController(
			SceneChanger sceneChanger,
			ClientMessageSender client) {
		this.sceneChanger = sceneChanger;
		this.client = client;
	}

	@FXML private Text acknowledgement;
	@FXML private Text statusMessage;

	public void initialize() {
		Platform.runLater(() -> acknowledgement.setText(ACK_TEXT));
	}

	@FXML
	protected void handleEnterAsGuestPressed(ActionEvent event) {
		client.doNewGuest();
	}

	@FXML
	protected void handleBackPressed(ActionEvent event) {
		sceneChanger.changeScene(FXMLLoaders.LOGIN_FXML);
	}

	@Override
	public void handleNewGuestResponse(boolean successful, String error) {
		if (!successful) {
			statusMessage.setFill(Color.FIREBRICK);
			statusMessage.setText(error);
		} else {
			clear();
			Platform.runLater(
					() -> sceneChanger.changeScene(FXMLLoaders.MAIN_GAME_FXML));
		}
	}

	private void clear() {
		statusMessage.setText("");
	}
}
