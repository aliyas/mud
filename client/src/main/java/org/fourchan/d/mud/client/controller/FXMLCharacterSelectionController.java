package org.fourchan.d.mud.client.controller;

import javax.inject.Inject;

import org.fourchan.d.mud.client.ClientMessageSender;
import org.fourchan.d.mud.client.game.ClientGameState;
import org.fourchan.d.mud.client.ui.LogoutInitiator;
import org.fourchan.d.mud.common.messages.common.Character;

import com.google.common.collect.ImmutableList;

import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;

public class FXMLCharacterSelectionController {
	private static final int N_COLS = 3;
	private static final int N_ROWS = 2;
	private final LogoutInitiator logoutInitiator;
	private final GameSceneChanger gameSceneChanger;
	private final ClientMessageSender client;
	private ClientGameState clientGameState;
	private ImmutableList<Node> currentErasableNodes;
	private int page;

	@Inject
	public FXMLCharacterSelectionController(
			LogoutInitiator logoutInitiator,
			GameSceneChanger gameSceneChanger,
			ClientMessageSender client) {
		this.logoutInitiator = logoutInitiator;
		this.gameSceneChanger = gameSceneChanger;
		this.client = client;
		this.currentErasableNodes = ImmutableList.of();
	}

	@FXML GridPane selectGrid;

	public void initialize() {
		createButtons();
	}

	@FXML
	public void handleCreateNewCharacter() {
		gameSceneChanger.toCharacterCreation();
	}

	@FXML
	public void handleLogout() {
		logoutInitiator.doLogout();
	}

	public void synchronize(ClientGameState clientGameState) {
		this.clientGameState = clientGameState;
		createButtons();
	}

	private void createButtons() {
		if (clientGameState == null || selectGrid == null) {
			return;
		}
		int rowIndex = 0;
		int columnIndex = 0;
		int currPage = 0;
		for (Node node: currentErasableNodes) {
			selectGrid.getChildren().remove(node);
		}
		ImmutableList.Builder<Node> erasableNodeListBuilder = ImmutableList.builder();
		for (Character character : clientGameState.getPlayerCharacters()) {
			boolean onPreviousPage = columnIndex + rowIndex * N_COLS + currPage * N_COLS * N_ROWS < page * N_COLS * N_ROWS;
			boolean onNextPage = columnIndex + rowIndex * N_COLS + currPage * N_COLS * N_ROWS >= (page+1) * N_COLS * N_ROWS;
			if (onNextPage) {
				// Just stop
				break;
			}
			if (!onPreviousPage) {
				Button button = new Button(String.format("%s %s", character.firstName, character.lastName));
				erasableNodeListBuilder.add(button);
				final Character myCharacter = character;
				button.setOnAction(actionEvent -> characterSelected(myCharacter));
				selectGrid.add(button, columnIndex, rowIndex);
			}
			columnIndex++;
			if (columnIndex > N_COLS) {
				columnIndex = 0;
				rowIndex++;
				if (rowIndex > N_ROWS) {
					rowIndex = 0;
					currPage++;
				}
			}
		}
		if (clientGameState.getPlayerCharacters() == null
				|| clientGameState.getPlayerCharacters().isEmpty()) {
			Label label = new Label("You have no characters.\nCreate one!");
			selectGrid.add(label, 1, 0);
			erasableNodeListBuilder.add(label);
		}
		this.currentErasableNodes = erasableNodeListBuilder.build();
	}

	public void characterSelected(Character character) {
		client.doCharacterSelect(character.firstName, character.lastName);
	}
}
