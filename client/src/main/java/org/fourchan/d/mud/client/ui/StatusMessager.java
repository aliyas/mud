package org.fourchan.d.mud.client.ui;

public interface StatusMessager {
	public void setMessage(String message);
	public void setErrorStatusMessage(String error);
}
