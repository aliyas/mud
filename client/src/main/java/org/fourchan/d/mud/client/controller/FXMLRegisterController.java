package org.fourchan.d.mud.client.controller;

import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;

import org.fourchan.d.mud.client.ClientMessageSender;
import org.fourchan.d.mud.client.SceneChanger;
import org.fourchan.d.mud.client.fxml.FXMLLoaders;
import org.fourchan.d.mud.client.keys.KeyStore;
import org.fourchan.d.mud.client.ui.RegisterController;
import org.fourchan.d.mud.common.messages.responses.RegisterResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Strings;
import com.google.inject.Inject;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;

public class FXMLRegisterController implements RegisterController {
	private static final Logger logger = LoggerFactory.getLogger(FXMLRegisterController.class);
	private final KeyStore keyStore;
	private final SceneChanger sceneChanger;
	private final ClientMessageSender client;

	@Inject
	public FXMLRegisterController(KeyStore keyStore, SceneChanger sceneChanger, ClientMessageSender client) {
		this.keyStore = keyStore;
		this.sceneChanger = sceneChanger;
		this.client = client;
	}
	
	@FXML private TextField usernameField;
	@FXML private PasswordField passwordField;
	@FXML private PasswordField verifyPasswordField;
	@FXML private Text statusMessage;

	@FXML
	protected void handleRegisterButtonAction(ActionEvent actionEvent) {
		statusMessage.setText("");
		String username = Strings.nullToEmpty(usernameField.getText()).trim();
		String password = Strings.nullToEmpty(passwordField.getText()).trim();
		String verifyPassword = Strings.nullToEmpty(verifyPasswordField.getText()).trim();
		if (username.isEmpty()) {
			statusMessage.setFill(Color.FIREBRICK);
			statusMessage.setText("Please enter a username.");
			return;
		} else if (!username.matches("[a-zA-Z]*")) {
			statusMessage.setFill(Color.FIREBRICK);
			statusMessage.setText("Username can only contain lower/upper case latin letters.");
			return;
		} else if (!password.equals(verifyPassword)) {
			statusMessage.setFill(Color.FIREBRICK);
			statusMessage.setText("Passwords do not match");
			return;
		}
		X509Certificate cert = keyStore.createKey(username, password);
		if (cert != null) {
			try {
				client.doRegister(username, cert);
			} catch (CertificateEncodingException e) {
				logger.error("Could not register due to encoding cert exception", e);
				statusMessage.setFill(Color.FIREBRICK);
				statusMessage.setText("Error encoding certificate to server.");
				return;
			}
		} else {
			statusMessage.setFill(Color.FIREBRICK);
			statusMessage.setText("Error creating private key & certificate.");
			return;
		}
	}

	@FXML
	protected void handleBackButtonPressed(ActionEvent actionEvent) {
		clear();
    	sceneChanger.changeScene(FXMLLoaders.LOGIN_FXML);
	}

	@Override
	public void handleRegisterResponse(RegisterResponse response) {
		if (response.isSuccessful()) {
			clear();
			statusMessage.setFill(Color.BLUE);
			statusMessage.setText("Registration successful!");
		} else {
			statusMessage.setFill(Color.FIREBRICK);
			statusMessage.setText(response.getError());
		}
	}

	private void clear() {
		usernameField.clear();
		passwordField.clear();
		verifyPasswordField.clear();
		statusMessage.setText("");
	}
}
