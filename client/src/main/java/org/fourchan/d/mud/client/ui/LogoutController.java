package org.fourchan.d.mud.client.ui;

public interface LogoutController {
	void handleLogoutResponse(boolean isSuccessful, String error);
}
