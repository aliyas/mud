package org.fourchan.d.mud.client;

import java.io.IOException;
import java.net.URI;
import java.time.Instant;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.fourchan.d.mud.client.keys.KeyStore;
import org.fourchan.d.mud.client.ui.PingController;
import org.java_websocket.WebSocket;
import org.java_websocket.WebSocketImpl;
import org.java_websocket.client.WebSocketClient;
import org.java_websocket.framing.Framedata;
import org.java_websocket.handshake.ServerHandshake;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

public class StockadeWebsocketClient {
	private static final Logger logger = LoggerFactory.getLogger(StockadeWebsocketClient.class);

	private final URI serverURI;
	private final ClientMessageHandler client;
	private final StockadeCommandLine commandLine;
	private final KeyStore keyStore;
	private final ScheduledExecutorService scheduledService;
	private PingController pingController;
	private Instant pingTime;
	private Boolean pingInFlight; // Must synchronize
	private Boolean doAttemptReconnects; // Must synchronize
	private Boolean isReconnecting; // Must synchronize
	private WebSocketClientImpl currentConn;

	/** Stupid hack since WebSocketClient and descendants are not reusable */
	private class WebSocketClientImpl extends WebSocketClient {
		private StockadeWebsocketClient parent;

		WebSocketClientImpl(StockadeWebsocketClient parent, URI serverUri) {
			super(serverUri);
			this.parent = parent;
		}

		@Override
		public void onClose(int code, String reason, boolean remote) {
			parent.onClose(code, reason, remote);
		}

		@Override
		public void onError(Exception ex) {
			parent.onError(ex);
		}

		@Override
		public void onMessage(String message) {
			parent.onMessage(message);
		}

		@Override
		public void onOpen(ServerHandshake handshakeData) {
			parent.onOpen(handshakeData);
		}

		@Override
		public void onWebsocketPong(WebSocket conn, Framedata f) {
			parent.onWebsocketPong(conn, f);
		}
	}

	@Inject
	public StockadeWebsocketClient(
			ClientMessageHandler client,
			URI serverURI,
			StockadeCommandLine commandLine,
			KeyStore keyStore,
			PingController pingController) {
		this.serverURI = serverURI;
		this.client = client;
		this.commandLine = commandLine;
		this.keyStore = keyStore;
		this.scheduledService = Executors.newSingleThreadScheduledExecutor();
		this.pingController = pingController;
		this.pingInFlight = false;
		this.doAttemptReconnects = false;
		this.isReconnecting = false;
		WebSocketImpl.DEBUG = this.commandLine.isDebugEnabled();
	}

	public void connectSsl()
			throws InterruptedException, IOException {
		synchronized(doAttemptReconnects) {
			doAttemptReconnects = true;
		}
		if (currentConn == null) {
			currentConn = new WebSocketClientImpl(this, serverURI);
			currentConn.setSocket(keyStore.getSSLContextClient().getSocketFactory().createSocket());
		}
		currentConn.connect();
	}

	public void disconnect() throws InterruptedException {
		synchronized(doAttemptReconnects) {
			doAttemptReconnects = false;
		}
		cleanupCurrentConn();
	}

	public void onOpen(ServerHandshake handshakeData) {
		logger.info("Successfully opened the websocket connection");
		pingController.onConnect();
		doAttemptToSendPing();
		synchronized(isReconnecting) {
			isReconnecting = false;
		}
	}

	public void onMessage(String message) {
		logger.trace("Received: {}", message);
		client.handleMessage(message);
	}

	public void onClose(int code, String reason, boolean remote) {
		logger.info("Websocket connection closed");
		cleanupCurrentConn();
		pingController.onDisconnect();
		synchronized(doAttemptReconnects) {
			if (doAttemptReconnects) {
				client.onUnintentionalDisconnect();
				synchronized(isReconnecting) {
					if (!isReconnecting) {
						isReconnecting = true;
						doAttemptToReconnect();
					}
				}
			}
		}
	}

	public void onWebsocketPong(WebSocket conn, Framedata f) {
		synchronized(pingInFlight) {
			if (pingTime != null) {
				pingController.onPingPong(pingTime, Instant.now());
			}
			pingInFlight = false;
		}
	}

	public void onError(Exception ex) {
		logger.error("Exception occurred within websocket", ex);
		cleanupCurrentConn();
	}

	public void send(String text) {
		logger.trace("Sending: '{}'", text);
		currentConn.send(text);
	}

	private void doAttemptToSendPing() {
		logger.trace("Scheduling ping");
		scheduledService.schedule(
				() -> {
					if (currentConn != null && currentConn.isOpen()) {
						synchronized(pingInFlight) {
							if (!pingInFlight) {
								pingInFlight = true;
								pingTime = Instant.now();
								currentConn.sendPing();
							}
							doAttemptToSendPing();
						}
					}
				},
				5,
				TimeUnit.SECONDS);
	}

	private void doAttemptToReconnect() {
		logger.trace("Scheduling reconnect attempt");
		scheduledService.schedule(
				() -> {
					if (currentConn == null || currentConn.isClosed()) {
						synchronized(doAttemptReconnects) {
							if (doAttemptReconnects) {
								pingController.onReconnect();
								try {
									if (currentConn == null) {
										currentConn = new WebSocketClientImpl(this, serverURI);
										currentConn.setSocket(keyStore.getSSLContextClient().getSocketFactory().createSocket());
									}
									currentConn.connect();
									Thread.sleep(1000);
								} catch (InterruptedException | IOException e) {
									logger.warn("Failed to reconnect websocket", e);
								}
								if (!currentConn.isOpen()) {
									cleanupCurrentConn();
									doAttemptToReconnect();
								}
							}
						}
					}
				},
				5,
				TimeUnit.SECONDS);
	}

	private void cleanupCurrentConn() {
		if (currentConn == null) {
			logger.trace("Current connection is already cleaned up");
			return;
		}
		synchronized(currentConn) {
			if (currentConn != null &&  (currentConn.isClosed() || currentConn.isClosing())) {
				logger.trace("Cleaning up current connection: Only set it to null");
				currentConn = null;
			} else if (currentConn != null) {
				logger.trace("Cleaning up current connection: Close connection and set it to null");
				currentConn.close();
				currentConn = null;
			}
		}
	}
}
