package org.fourchan.d.mud.client.ui;

public interface GuestController {
	void handleNewGuestResponse(boolean successful, String error);
}
