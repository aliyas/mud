package org.fourchan.d.mud.client;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.Signature;
import java.security.SignatureException;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;
import java.util.Base64;

import org.fourchan.d.mud.client.fxml.FXMLLoaders;
import org.fourchan.d.mud.client.ui.GameController;
import org.fourchan.d.mud.client.ui.GuestController;
import org.fourchan.d.mud.client.ui.GuestPromotionController;
import org.fourchan.d.mud.client.ui.LoginController;
import org.fourchan.d.mud.client.ui.LogoutController;
import org.fourchan.d.mud.client.ui.RegisterController;
import org.fourchan.d.mud.common.StockadeClient;
import org.fourchan.d.mud.common.map.Direction;
import org.fourchan.d.mud.common.map.MapPointId;
import org.fourchan.d.mud.common.map.Point;
import org.fourchan.d.mud.common.messages.ActionMessage;
import org.fourchan.d.mud.common.messages.ClientMessage;
import org.fourchan.d.mud.common.messages.requests.CharacterSelectedRequest;
import org.fourchan.d.mud.common.messages.requests.CreateCharacterRequest;
import org.fourchan.d.mud.common.messages.requests.InventoryRequest;
import org.fourchan.d.mud.common.messages.requests.LoginChallengeRequest;
import org.fourchan.d.mud.common.messages.requests.LoginRequest;
import org.fourchan.d.mud.common.messages.requests.LogoutRequest;
import org.fourchan.d.mud.common.messages.requests.MoveRequest;
import org.fourchan.d.mud.common.messages.requests.NewGuestRequest;
import org.fourchan.d.mud.common.messages.requests.PlayerInfoRequest;
import org.fourchan.d.mud.common.messages.requests.PromoteGuestToNonGuestRequest;
import org.fourchan.d.mud.common.messages.requests.RegisterRequest;
import org.fourchan.d.mud.common.messages.requests.SpawnPointRequest;
import org.fourchan.d.mud.common.messages.requests.privileged.MapUpdateRequest;
import org.fourchan.d.mud.common.messages.responses.ActionPayload;
import org.fourchan.d.mud.common.messages.responses.CharacterSelectedResponse;
import org.fourchan.d.mud.common.messages.responses.CreateCharacterResponse;
import org.fourchan.d.mud.common.messages.responses.EntityPayload;
import org.fourchan.d.mud.common.messages.responses.InventoryResponse;
import org.fourchan.d.mud.common.messages.responses.LoginChallengeResponse;
import org.fourchan.d.mud.common.messages.responses.LoginResponse;
import org.fourchan.d.mud.common.messages.responses.LogoutResponse;
import org.fourchan.d.mud.common.messages.responses.MapPayload;
import org.fourchan.d.mud.common.messages.responses.MoveResponse;
import org.fourchan.d.mud.common.messages.responses.NewGuestResponse;
import org.fourchan.d.mud.common.messages.responses.PlayerInfoResponse;
import org.fourchan.d.mud.common.messages.responses.PromoteGuestToNonGuestResponse;
import org.fourchan.d.mud.common.messages.responses.RegisterResponse;
import org.fourchan.d.mud.common.messages.responses.SpawnPointResponse;
import org.fourchan.d.mud.common.messages.responses.TextPayload;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.inject.Inject;

public class StockadeClientImpl implements StockadeClient, ClientMessageHandler, ClientMessageSender {
	private static final Logger logger = LoggerFactory.getLogger(StockadeClientImpl.class);
	private static final String BEGIN_CERT = "-----BEGIN CERTIFICATE-----";
	private static final String END_CERT = "-----END CERTIFICATE-----";
	private static final String LINE_SEPERATOR = System.getProperty("line.separator");
	private static final int ENCODING_CERT_LINE_LENGTH = 64;

	private final StockadeWebsocketClient client;
	private final Gson gson;
	private final RegisterController registerController;
	private final LoginController loginController;
	private final LogoutController logoutController;
	private final GameController gameController;
	private final GuestController guestController;
	private final GuestPromotionController guestPromotionController;
	private final SceneChanger sceneChanger;

	@Inject
	public StockadeClientImpl(
			StockadeWebsocketClient client,
			Gson gson,
			RegisterController registerController,
			LoginController loginController,
			LogoutController logoutController,
			GameController gameController,
			GuestController guestController,
			GuestPromotionController guestPromotionController,
			SceneChanger sceneChanger) {
		this.client = client;
		this.gson = gson;
		this.registerController = registerController;
		this.loginController = loginController;
		this.logoutController = logoutController;
		this.gameController = gameController;
		this.guestController = guestController;
		this.guestPromotionController = guestPromotionController;
		this.sceneChanger = sceneChanger;
	}

	@Override
	public void connect()
			throws IOException, InterruptedException {
		client.connectSsl();
	}

	@Override
	public void disconnect() throws InterruptedException {
		client.disconnect();
	}

	@Override
	public void onUnintentionalDisconnect() {
		sceneChanger.changeScene(FXMLLoaders.LOGIN_FXML);
	}
	
	@Override
	public void handleMessage(String message) {
		ClientMessage clientMessage = gson.fromJson(message, ClientMessage.class);
		clientMessage.visit(this);
	}

	@Override
	public void doRegister(String username, X509Certificate cert) throws CertificateEncodingException {
	    final String certString = encode(cert);
		RegisterRequest request = new RegisterRequest(username, certString);
		send(gson.toJson(request));
	}

	@Override
	public void handleRegisterResponse(RegisterResponse response) {
		this.registerController.handleRegisterResponse(response);
	}

	@Override
	public void askLoginChallenge() {
		send(gson.toJson(new LoginChallengeRequest()));
	}

	@Override
	public void handleLoginChallengeResponse(LoginChallengeResponse response) {
		if (!response.isSuccessful()) {
			loginController.handleLoginResponse(response.isSuccessful(), response.getError());
			return;
		}
		try {
			Base64.Encoder encoder = Base64.getEncoder();
			Base64.Decoder decoder = Base64.getDecoder();
			Signature signature = Signature.getInstance("SHA256withRSA");
			signature.initSign(loginController.getLoginKey(), new SecureRandom());
			signature.update(decoder.decode(response.getChallenge()));
			String encodedAnswer = encoder.encodeToString(signature.sign());
			send(gson.toJson(new LoginRequest(loginController.getUsername(), encodedAnswer)));
		} catch (NoSuchAlgorithmException e) {
			logger.error("Algorithm issue when handling login challenge", e);
		} catch (InvalidKeyException e) {
			logger.error("Invalid key issue when handling login challenge", e);
		} catch (SignatureException e) {
			logger.error("Signature issue when handling login challenge", e);
		}
	}

	@Override
	public void handleLoginResponse(LoginResponse response) {
		loginController.handleLoginResponse(response.isSuccessful(), response.getError());
		if (response.isSuccessful()) {
			gameController.handleLoginResponse(response);
		}
	}

	@Override
	public void doLogout() {
		send(gson.toJson(new LogoutRequest()));
	}

	@Override
	public void handleLogoutResponse(LogoutResponse response) {
		logoutController.handleLogoutResponse(response.isSuccessful(), response.getError());
	}

	@Override
	public void doPlayerInfo(String username) {
		send(gson.toJson(new PlayerInfoRequest(username)));
	}

	@Override
	public void handlePlayerInfoResponse(PlayerInfoResponse response) {
		gameController.handlePlayerInfoResponse(response);
	}

	@Override
	public void doCreateCharacter(
			String firstName,
			String lastName,
			String sexChoice,
			String spawnChoice) {
		send(gson.toJson(new CreateCharacterRequest(firstName, lastName, sexChoice, spawnChoice)));
	}

	@Override
	public void handleCreateCharacterResponse(CreateCharacterResponse response) {
		gameController.handleCreateCharacterResponse(response);
	}

	@Override
	public void doCharacterSelect(String firstName, String lastName) {
		send(gson.toJson(new CharacterSelectedRequest(firstName, lastName)));
	}

	@Override
	public void handleCharacterSelectedResponse(CharacterSelectedResponse response) {
		gameController.handleCharacterSelectedResponse(response);
	}

	@Override
	public void handleMapPayload(MapPayload payload) {
		gameController.handleMapPayload(payload);
	}

	@Override
	public void doMove(Direction direction) {
		send(gson.toJson(new MoveRequest(direction)));
	}

	@Override
	public void handleMoveResponse(MoveResponse response) {
		gameController.handleMoveResponse(response);
	}

	@Override
	public void doRequestSpawnPoints() {
		send(gson.toJson(new SpawnPointRequest()));
	}

	@Override
	public void handleSpawnPointResponse(SpawnPointResponse response) {
		gameController.handleSpawnPointResponse(response);
	}

	@Override
	public void doMapUpdateRequest(Point point, MapPointId mapPointId) {
		send(gson.toJson(new MapUpdateRequest(point, mapPointId)));
	}

	@Override
	public void handleEntityPayload(EntityPayload response) {
		gameController.handleEntityPayload(response);
	}

	@Override
	public void handleActionPayload(ActionPayload response) {
		gameController.handleActionPayload(response);
	}

	@Override
	public void doActionMessage(ActionMessage message) {
		send(gson.toJson(message));
	}

	@Override
	public void handleTextPayload(TextPayload response) {
		gameController.handleTextPayload(response);
	}

	@Override
	public void doRequestInventory() {
		send(gson.toJson(new InventoryRequest()));
	}

	@Override
	public void handleInventoryResponse(InventoryResponse response) {
		gameController.handleInventoryResponse(response);
	}

	@Override
	public void doNewGuest() {
		send(gson.toJson(new NewGuestRequest()));
	}

	@Override
	public void doPromoteGuestToNonGuest(String username, X509Certificate cert) throws CertificateEncodingException {
		final String publicKey = encode(cert);
		send(gson.toJson(new PromoteGuestToNonGuestRequest(username, publicKey)));
	}

	@Override
	public void handleNewGuestResponse(NewGuestResponse response) {
		guestController.handleNewGuestResponse(response.isSuccessful(), response.getError());
		if (response.isSuccessful()) {
			gameController.handleNewGuestResponse(response);
		}
	}

	@Override
	public void handlePromoteGuestToNonGuestResponse(PromoteGuestToNonGuestResponse response) {
		guestPromotionController.handlePromoteGuestToNonGuestResponse(response);
		gameController.handlePromoteGuestToNonGuestResponse(response);
	}

	private void send(String message) {
		client.send(message);
	}

	private String encode(X509Certificate cert) throws CertificateEncodingException {
		// Could put in custom serializer and deserializer.
		Base64.Encoder encoder = Base64.getMimeEncoder(ENCODING_CERT_LINE_LENGTH, LINE_SEPERATOR.getBytes());
		final byte[] rawCert = cert.getEncoded();
	    final String encodedCert = new String(encoder.encode(rawCert));
	    return BEGIN_CERT + LINE_SEPERATOR + encodedCert + LINE_SEPERATOR + END_CERT;
	}
}
