package org.fourchan.d.mud.client.controller;

public interface GameSceneChanger {
	void toCharacterCreation();
	void toCharacterSelection();
	void toMainGameScreen();
	void toInventoryScreen();
}
