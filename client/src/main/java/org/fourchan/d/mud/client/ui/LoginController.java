package org.fourchan.d.mud.client.ui;

import java.security.PrivateKey;

public interface LoginController {
	PrivateKey getLoginKey();
	String getUsername();
	void handleLoginResponse(boolean successful, String error);
}
