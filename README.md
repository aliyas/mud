# 4chan's /d/ MUD Game

This code is open source. See LICENSE for the details.

## Overview

The repository is monolithic and contains all the code for the client, server,
tools, and more. Gradle is the build tool required to setup the projects and
dependencies.

### client

This contains the Java-websocket-based game client.

### common-api

This contains shared code for the game server and client. It mainly houses
common serialization and API definitions for the custom websocket protocol.

### common-data

This contains shared code for the server and other tools that need to interact
with the database directly.

### maptool

This contains the map editing tool used to build the game world, set spawn
points, etc.

### server

This repository contains the java-websocket-based game server.

An installation of Postgres is required. By default, the server will attempt to
connect to `postgresql://localhost:5432/mud` authenticating with the user and
password `mudaccess`.

## Things To Know

The code leverages a lot of concepts and external libraries which can seem
overwhelming and are poorly documented. This section aims to provide a bullet
list of ideas and technologies to be familiar with.

* Java 1.8
* Dependency Injection, as implemented by Guice
* Websockets (ws & wss), HTTP, TCP/IP, TLS
* Certificates, as supported by Java (bleh)
* Asymmetric crypto, as provided by bouncycastle
* JSON, as implemented by Gson
* Java Persistence API (JPA) (ex: EclipseLink), SQL
* Databases, transactions (ex: Postgres)
* Concurrent programming, multithreading, Futures
* JavaFX, for the Java UI
* Gradle, for the build tooling

## A Note on Authentication

A user logging in through the client is responsible for both remembering a
password and keeping their keystore containing a private key safe. The use of
the private key allows a user to authenticate with the server without having
to transmit a password.

The server is authenticated to the client by a self-signed certificate. The
client logs in by providing signature proof to the server, requiring no
exchange of passwords over the wire. It does mean the client is responsible for
not losing its private key.

This means Anon can trust the people running the server as there is no way for
the people that run the server to have the passwords be intercepted, stolen,
hacked, leaked, or legally compelled to divulge.