# Contributing to the mu/d/

First off, thanks for being interested in contributing. There is a lot of
exciting opportunities, and I want to ensure contributions are meaningful to
both a contributor and the project. I respect your time, please respect mine.

The prevailing philosophy for the project is
*"Don't be a jerk, but have thick skin."*

The project is governed
[DFL-style](https://en.wikipedia.org/wiki/Benevolent_dictator_for_life). The
code is licensed under the permissive
[MIT License](https://opensource.org/licenses/MIT). People that disagree with
the DFL are free to fork as are the greedy bastards that want to leverage the
project to make proprietary derivatives and money.

## Providing Feedback & Filing Issues

Filing issues and providing feedback is the quickest way to contribute to the
project. Non-game-breaking feedback, such as praise or condemnation, can be
provided on the latest /dgg/ thread.

Game-breaking feedback should ultimately end up as an
[issue on the repository](https://gitlab.com/aliyas/mud/issues/new?issue).
**First please search the
[existing issues](https://gitlab.com/aliyas/mud/issues)
to ensure you are not creating a duplicate**. If you are uncomfortable with
creating an issue due to the fact it requires a Gitlab account, please find a
fellow Anon in /dgg/ to file it on your behalf. Finally, provide as much detail
as possible when describing an issue.

Feature requests must be **mechanically-concrete** and have **closed-scope**.
Mechanically-concrete means the request must consider other mechanics of the
game (but does not require software development expertise). Closed scope means
vague bullshit will be rejected.

## Contributing Code

Contributing with a Gitlab account:

1. **If you have never contributed to this project before, your first merge
request must be to add your handle to the AUTHORS file**. Failing to do so will
result in all other merge requests to be rejected regardless of content.
1. Assign an [issue](https://gitlab.com/aliyas/mud/issues) to yourself.
1. If there is not an issue for what you want to work on, follow the guidance
above under *Providing Feedback & Filing Issues* to file an issue. Take the
issue.
1. Start a design discussion in the issue, and state how you plan to solve the
problem. The burden is on the assignee to begin a design discussion and avoid
wasting their time doing work that will ultimately be rejected.
1. After the DFL gives the go-ahead on the design, do the work.
1. Once the work is done, create a
[merge request](https://docs.gitlab.com/ce/user/project/merge_requests/index.html)
1. The DFL will review the code and provide feedback.
1. Address the feedback.
1. Repeat the above two steps until there is no feedback left to resolve. The
DFL will accept the merge request.

## Code Style

Use tabs, don't let the code overflow too much horizontally, and generally try
to follow the existing style as much as possible. Fair warning, the DFL will be
anal and not in a fun way.

## Code of Conduct

See the prevailing philosophy:
*"Don't be a jerk, but have thick skin."*

Any behavior that is not constructive to, or is damaging, project health is a
guaranteed path to expulsion.