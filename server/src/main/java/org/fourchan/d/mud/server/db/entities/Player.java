package org.fourchan.d.mud.server.db.entities;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "players")
public class Player {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column(unique = true, columnDefinition = "text")
	private String name;

	@OneToMany(
		mappedBy = "player",
		fetch = FetchType.EAGER
	)
    private List<Character> characters;

	@OneToOne(optional = false, mappedBy = "player", cascade = CascadeType.PERSIST)
	private PlayerPermissions permissions;

	@OneToOne(optional = false, mappedBy = "player", cascade = CascadeType.PERSIST)
	private PlayerPreferences preferences;

	@Column(nullable = false)
	private int playerKind;

	@Column(name = "created", nullable = false)
	protected LocalDateTime created;

	Player() {}

	public Player(String name, PlayerKind playerKind) {
		this.name = name;
		this.playerKind = playerKind.getId();
		this.permissions = new PlayerPermissions(this);
		this.preferences = new PlayerPreferences(this);
		this.created = LocalDateTime.now();
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public List<Character> getCharacters() {
		return characters;
	}

	public void addCharacter(Character c) {
		this.characters.add(c);
	}

	public PlayerPermissions getPermissions() {
		return permissions;
	}

	public PlayerPreferences getPreferences() {
		return preferences;
	}

	public PlayerKind getPlayerKind() {
		return PlayerKind.parseFrom(playerKind);
	}

	public void convertToNonGuest(String name) {
		this.name = name;
		playerKind = PlayerKind.NORMAL.getId();
	}
}
