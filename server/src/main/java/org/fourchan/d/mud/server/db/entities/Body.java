package org.fourchan.d.mud.server.db.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.fourchan.d.mud.server.db.entities.sex.Anus;
import org.fourchan.d.mud.server.db.entities.sex.Ballsack;
import org.fourchan.d.mud.server.db.entities.sex.Breasts;
import org.fourchan.d.mud.server.db.entities.sex.Hands;
import org.fourchan.d.mud.server.db.entities.sex.Mouth;
import org.fourchan.d.mud.server.db.entities.sex.Penis;
import org.fourchan.d.mud.server.db.entities.sex.Stomach;
import org.fourchan.d.mud.server.db.entities.sex.Vagina;

@Entity
@Table(
	name = "bodies",
	uniqueConstraints = {
		@UniqueConstraint(columnNames = {"character_id"}),
		@UniqueConstraint(columnNames = {"npc_id"})
	}
)
public class Body {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@OneToOne(optional = true)
	@JoinColumn(name = "characters_id", referencedColumnName = "id")
	private Character character;

	@OneToOne(optional = true)
	@JoinColumn(name = "npcs_id", referencedColumnName = "id")
	private Npc npc;

	@OneToMany(
		mappedBy = "body",
		fetch = FetchType.LAZY
	)
	private List<Penis> penises;

	@OneToMany(
		mappedBy = "body",
		fetch = FetchType.LAZY
	)
	private List<Vagina> vaginas;

	@OneToMany(
		mappedBy = "body",
		fetch = FetchType.LAZY
	)
	private List<Ballsack> ballsacks;

	@OneToMany(
		mappedBy = "body",
		fetch = FetchType.LAZY
	)
	private List<Anus> anuses;

	@OneToMany(
		mappedBy = "body",
		fetch = FetchType.LAZY
	)
	private List<Breasts> breasts;

	@OneToMany(
		mappedBy = "body",
		fetch = FetchType.LAZY
	)
	private List<Mouth> mouths;

	@OneToMany(
		mappedBy = "body",
		fetch = FetchType.LAZY
	)
	private List<Stomach> stomachs;

	@OneToMany(
		mappedBy = "body",
		fetch = FetchType.LAZY
	)
	private List<Hands> hands;

	@OneToMany(
		mappedBy = "body",
		fetch = FetchType.LAZY
	)
	private List<Item> items;

	Body() {}

	Body(Character character) {
		this.character = character;
		penises = new ArrayList<Penis>();
		vaginas = new ArrayList<Vagina>();
		ballsacks = new ArrayList<Ballsack>();
		anuses = new ArrayList<Anus>();
		breasts = new ArrayList<Breasts>();
		mouths = new ArrayList<Mouth>();
		stomachs = new ArrayList<Stomach>();
		hands = new ArrayList<Hands>();
		items = new ArrayList<Item>();
	}

	Body(Npc npc) {
		this.npc = npc;
		penises = new ArrayList<Penis>();
		vaginas = new ArrayList<Vagina>();
		ballsacks = new ArrayList<Ballsack>();
		anuses = new ArrayList<Anus>();
		breasts = new ArrayList<Breasts>();
		mouths = new ArrayList<Mouth>();
		stomachs = new ArrayList<Stomach>();
		hands = new ArrayList<Hands>();
		items = new ArrayList<Item>();
	}

	public List<Penis> getPenises() {
		return penises;
	}

	public void addPenis(Penis penis) {
		penises.add(penis);
	}

	public List<Vagina> getVaginas() {
		return vaginas;
	}

	public void addVagina(Vagina vagina) {
		vaginas.add(vagina);
	}

	public List<Ballsack> getBallsacks() {
		return ballsacks;
	}

	public void addBallsack(Ballsack ballsack) {
		ballsacks.add(ballsack);
	}

	public List<Anus> getAnuses() {
		return anuses;
	}

	public void addAnus(Anus anus) {
		anuses.add(anus);
	}

	public List<Breasts> getBreasts() {
		return breasts;
	}

	public void addBreasts(Breasts breasts) {
		this.breasts.add(breasts);
	}

	public List<Mouth> getMouths() {
		return mouths;
	}

	public void addMouth(Mouth mouth) {
		mouths.add(mouth);
	}

	public List<Stomach> getStomachs() {
		return stomachs;
	}

	public void addStomach(Stomach stomach) {
		stomachs.add(stomach);
	}

	public List<Hands> getHands() {
		return hands;
	}

	public void addHands(Hands hands) {
		this.hands.add(hands);
	}

	public List<Item> getItem() {
		return items;
	}

	public void addItem(Item item) {
		this.items.add(item);
	}
}
