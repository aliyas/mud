package org.fourchan.d.mud.server.service.privileged;

import org.fourchan.d.mud.server.service.privileged.PrivilegedInterceptor.Privileged;

import com.google.inject.AbstractModule;
import com.google.inject.matcher.Matchers;

public class PrivilegedModule extends AbstractModule {
	protected void configure() {
		bindInterceptor(
				Matchers.any(),
				Matchers.annotatedWith(Privileged.class),
				new PrivilegedInterceptor(getProvider(PlayerProvider.class)));
	}
}
