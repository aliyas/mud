package org.fourchan.d.mud.server.service;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.fourchan.d.mud.common.StockadeService;
import org.fourchan.d.mud.common.messages.ServerMessage;
import org.fourchan.d.mud.common.messages.ServerMessageDeserializer;
import org.fourchan.d.mud.server.game.ConnectionContext;
import org.fourchan.d.mud.server.game.GameModule;
import org.fourchan.d.mud.server.service.StockadeServiceImpl.AuthenticatedMap;
import org.fourchan.d.mud.server.service.StockadeServiceImpl.UnauthenticatedMap;
import org.fourchan.d.mud.server.service.privileged.PlayerProvider;
import org.fourchan.d.mud.server.service.privileged.PrivilegedModule;
import org.java_websocket.WebSocket;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.inject.AbstractModule;
import com.google.inject.BindingAnnotation;
import com.google.inject.Provides;
import com.google.inject.Scopes;
import com.google.inject.Singleton;

public class ServiceModule extends AbstractModule {
	private static final int DEFAULT_MAP_SIZE = 100;

	@BindingAnnotation
	@Retention(RetentionPolicy.RUNTIME)
	@Target({ ElementType.FIELD, ElementType.PARAMETER, ElementType.METHOD })
	public @interface ServerThreadpool {}

	@Override
	protected void configure() {
		bind(StockadeService.class).to(StockadeServiceImpl.class);
		bind(PlayerProvider.class).to(StockadeServiceImpl.class);
		bind(StockadeServiceImpl.class).in(Scopes.SINGLETON);
		install(new GameModule());
		install(new PrivilegedModule());
	}

	@Provides
	@Singleton
	Gson provideGson() {
		return new GsonBuilder()
				.registerTypeAdapter(ServerMessage.class, new ServerMessageDeserializer())
				.create();
	}

	@Provides
	@Singleton
	@UnauthenticatedMap
	ConcurrentMap<WebSocket, ConnectionContext> provideUnauthenticatedMap() {
		return new ConcurrentHashMap<>(DEFAULT_MAP_SIZE);
	}

	@Provides
	@Singleton
	@AuthenticatedMap
	ConcurrentMap<WebSocket, ConnectionContext> provideAuthenticatedMap() {
		return new ConcurrentHashMap<>(DEFAULT_MAP_SIZE);
	}

	@Provides
	@Singleton
	ConcurrentMap<Integer, WebSocket> provideCharacterToSocketMap() {
		return new ConcurrentHashMap<>(DEFAULT_MAP_SIZE);
	}

	@Provides
	@Singleton
	@ServerThreadpool
	ExecutorService provideServerThreadpool() {
		return Executors.newFixedThreadPool(2);
	}
}
