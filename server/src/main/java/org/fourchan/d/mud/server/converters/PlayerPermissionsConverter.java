package org.fourchan.d.mud.server.converters;

import org.fourchan.d.mud.common.messages.common.Player.PlayerPermissions;

public final class PlayerPermissionsConverter {
	private PlayerPermissionsConverter() {}

	public static PlayerPermissions toMessage(
			org.fourchan.d.mud.server.db.entities.PlayerPermissions from) {
		PlayerPermissions to = new PlayerPermissions();
		to.canEditMap = from.canEditMap();
		return to;
	}
}
