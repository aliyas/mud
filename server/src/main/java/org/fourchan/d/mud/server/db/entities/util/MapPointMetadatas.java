package org.fourchan.d.mud.server.db.entities.util;

import java.util.Map;

import org.fourchan.d.mud.common.map.MapPointId;

import com.google.common.collect.ImmutableMap;

public final class MapPointMetadatas {
	private MapPointMetadatas() {}

	public static final Map<MapPointId, MapPointMetadata> MAP_POINT_ID_METADATA_MAP =
			ImmutableMap.<MapPointId, MapPointMetadata>builder()
					.put(MapPointId.GRASS, new MapPointMetadata(MapPointMovement.MOVEMENT_ALWAYS_ALLOWED))
					.put(MapPointId.STONE_CLIFF, new MapPointMetadata(MapPointMovement.MOVEMENT_ALWAYS_DISALLOWED))
					.build();

	public static class MapPointMetadata {
		public MapPointMovement movement;
		public MapPointMetadata(MapPointMovement movement) {
			this.movement = movement;
		}
	}
}
