package org.fourchan.d.mud.server.game.description;

import java.util.Set;

import org.fourchan.d.mud.server.db.entities.Item;
import org.fourchan.d.mud.server.db.entities.ItemProperties;
import org.fourchan.d.mud.server.game.types.ItemDescriber;

public class ItemDescriptionGenerator implements ItemDescriber {
	private StringBuilder stringBuilder;

	public ItemDescriptionGenerator() {
		stringBuilder = new StringBuilder();
	}

	@Override
	public Set<ItemProperties> propertyFilter() {
		return null;
	}

	@Override
	public void visitItem(Item item) {
		// TODO: Implement
	}

	@Override
	public void visitStringProperty(ItemProperties property, String value) {}

	@Override
	public void visitIntegerProperty(ItemProperties property, int value) {}

	@Override
	public String getDescription() {
		return stringBuilder.toString();
	}

}
