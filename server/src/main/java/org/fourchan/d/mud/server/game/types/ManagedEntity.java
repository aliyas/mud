package org.fourchan.d.mud.server.game.types;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

public abstract class ManagedEntity {
	protected EntityManagerFactory emf;
	private EntityManager currentTransaction;

	protected ManagedEntity(EntityManagerFactory em) {
		this.emf = em;
	}

	EntityManager getEntityManager() {
		if (currentTransaction != null && currentTransaction.isOpen()) {
			return currentTransaction;
		}
		return emf.createEntityManager();
	}

	/**
	 * Indicates that {@link #useTransaction} must be called before calling
	 * this annotated method.
	 */
	public @interface RequiresTransaction {};

	/**
	 * Indicates that {@link #useTransaction} must be called before calling
	 * this annotated method if the call would have a write-effect.
	 */
	public @interface RequiresTransactionToModify {};

	public void useTransaction(EntityManager em) {
		currentTransaction = em;
		fetchInTransaction();
	}

	protected abstract void fetchInTransaction();
}
