package org.fourchan.d.mud.server.converters;

import java.util.List;

import org.fourchan.d.mud.common.map.Point;
import org.fourchan.d.mud.common.messages.common.MapPoint;
import org.fourchan.d.mud.server.db.entities.EmbeddedPoint;

import com.google.common.collect.ImmutableList;

public final class MapPointConverter {

	private MapPointConverter() {}

	public static Point toMessage(EmbeddedPoint from) {
		return new Point(from.getX(), from.getY(), from.getZ());
	}

	public static MapPoint toMessage(
			org.fourchan.d.mud.server.db.entities.MapPoint from) {
		MapPoint to = new MapPoint();
		to.point = toMessage(from.getEmbeddedPoint());
		to.pointId = from.getMapPointId();
		return to;
	}

	public static List<MapPoint> toMessage(
			List<org.fourchan.d.mud.server.db.entities.MapPoint> from) {
		ImmutableList.Builder<MapPoint> builder =
				ImmutableList.builder();
		for (org.fourchan.d.mud.server.db.entities.MapPoint fromElement : from) {
			builder.add(toMessage(fromElement));
		}
		return builder.build();
	}
}
