package org.fourchan.d.mud.server.service.privileged;

import java.util.Optional;

import org.fourchan.d.mud.server.db.entities.Player;
import org.java_websocket.WebSocket;

public interface PlayerProvider {
	Optional<Player> getPlayer(WebSocket socket);
}
