package org.fourchan.d.mud.server.db.entities.util;

public enum MapPointMovement {
	MOVEMENT_ALWAYS_ALLOWED,
	MOVEMENT_ALWAYS_DISALLOWED,
}
