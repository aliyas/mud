package org.fourchan.d.mud.server;

import java.security.Security;

import org.apache.commons.cli.ParseException;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.fourchan.d.mud.server.keys.KeyStore;
import org.java_websocket.server.DefaultSSLWebSocketServerFactory;

import com.google.inject.Guice;
import com.google.inject.Injector;

public class Application {

	public static void main(String[] args) throws Exception {
		Security.addProvider(new BouncyCastleProvider());
		StockadeCommandLine commandLine = null;
		try {
			commandLine = new StockadeCommandLine(args);
		} catch (ParseException e) {
			System.out.print(e.getMessage());
			System.exit(1);
		}
		
		Injector injector = Guice.createInjector(new ServerModule(commandLine));
		StockadeWebsocketServer server = injector.getInstance(StockadeWebsocketServer.class);
		server.setWebSocketFactory(
				new DefaultSSLWebSocketServerFactory(
						injector.getInstance(KeyStore.class).getSSLContextServer()));
		System.out.println("Server starting");
		server.start();
		return;
	}
}
