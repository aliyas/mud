package org.fourchan.d.mud.server.game.types;

public interface ItemDescriber extends ItemVisitor {
	public String getDescription();
}
