package org.fourchan.d.mud.server.game.types;

import java.util.Set;

import javax.persistence.EntityManagerFactory;

import org.fourchan.d.mud.server.db.entities.Item;
import org.fourchan.d.mud.server.db.entities.ItemProperties;
import org.fourchan.d.mud.server.db.entities.ItemProperty;

public class ItemAdapter extends ManagedEntity {
	private Item item;

	public ItemAdapter(Item item, EntityManagerFactory em) {
		super(em);
		this.item = item;
	}

	@RequiresTransactionToModify
	public void visit(ItemVisitor visitor) {
		visitor.visitItem(item);

		Set<ItemProperties> filter = visitor.propertyFilter();
		for (ItemProperty property : item.getItemProperties()) {
			if (filter == null || !filter.contains(property.getItemProperty())) {
				continue;
			}
			if (property.isInteger()) {
				visitor.visitIntegerProperty(property.getItemProperty(), property.getIntegerValue());
			} else {
				visitor.visitStringProperty(property.getItemProperty(), property.getStringValue());
			}
		}
	}

	public org.fourchan.d.mud.common.messages.common.Item toItemMessage(ItemDescriber itemDescriber) {
		visit(itemDescriber);
		org.fourchan.d.mud.common.messages.common.Item item = new org.fourchan.d.mud.common.messages.common.Item();
		item.name = this.item.getItemKind().getName();
		item.quantity = this.item.getQuantity();
		item.description = itemDescriber.getDescription();
		return item;
	}

	@Override
	protected void fetchInTransaction() {
		item = super.getEntityManager().find(Item.class, item.getId());
	}
}
