package org.fourchan.d.mud.server.db.entities;

import java.util.List;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.fourchan.d.mud.common.map.MapPointId;

@Entity
@Table(name = "mapPoint")
public class MapPoint {
	@EmbeddedId
	private EmbeddedPoint embeddedPoint;

	@Enumerated(EnumType.ORDINAL)
	private MapPointId mapPointId;

	@OneToMany(
		mappedBy = "location",
		fetch = FetchType.LAZY
	)
    private List<Character> characters;

	@OneToOne(
		mappedBy = "spawnLocation",
		fetch = FetchType.LAZY
	)
	private SpawnPoint spawnPoint;

	MapPoint() {}

	public MapPoint(EmbeddedPoint embeddedPoint, MapPointId mapPointId) {
		this.embeddedPoint = embeddedPoint;
		this.mapPointId = mapPointId;
	}

	public EmbeddedPoint getEmbeddedPoint() {
		return embeddedPoint;
	}

	public MapPointId getMapPointId() {
		return mapPointId;
	}

	public void setMapPointId(MapPointId mapPointId) {
		this.mapPointId = mapPointId;
	}

	public List<Character> getCharacters() {
		return characters;
	}

	@Override
	public String toString() {
		return String.format("{id: %s, point: %s}", mapPointId, embeddedPoint);
	}
}
