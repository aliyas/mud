package org.fourchan.d.mud.server.db.entities;

public enum ItemKind {
	CREEP_VINE_GOO(1, "Creep Vine Goo");

	private final int id;
	private final String name;

	private ItemKind(int id, String name) {
		this.id = id;
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	private static final ItemKind[] VALUES = values();

	public static ItemKind parseFrom(int id) {
		for (ItemKind itemKind : VALUES) {
			if (itemKind.getId() == id) {
				return itemKind;
			}
		}
		return null;
	}
}
