package org.fourchan.d.mud.server.game.actions;

import java.util.List;

import org.fourchan.d.mud.common.messages.common.Action;
import org.fourchan.d.mud.common.messages.common.Entity;
import org.fourchan.d.mud.server.db.entities.Character;
import org.fourchan.d.mud.server.db.entities.sex.Anus;
import org.fourchan.d.mud.server.db.entities.sex.Ballsack;
import org.fourchan.d.mud.server.db.entities.sex.Breasts;
import org.fourchan.d.mud.server.db.entities.sex.Hands;
import org.fourchan.d.mud.server.db.entities.sex.Mouth;
import org.fourchan.d.mud.server.db.entities.sex.Penis;
import org.fourchan.d.mud.server.db.entities.sex.Stomach;
import org.fourchan.d.mud.server.db.entities.sex.Vagina;
import org.fourchan.d.mud.server.game.types.CharacterVisitor;

import com.google.common.collect.ImmutableList;

public class CharacterActionDeterminer implements ActionProducer, CharacterVisitor {
	private ImmutableList.Builder<Action> actionList;

	public CharacterActionDeterminer() {
		actionList = ImmutableList.builder();
	}

	@Override
	public List<VisitType> visitOrder() {
		return ImmutableList.of(VisitType.CHARACTER);
	}

	@Override
	public void accept(Character character) {
		Action action = new Action();
		action.actionType = Action.Type.EXAMINE;
		action.description = String.format("Get more detail on %s %s.", character.getFirstName(), character.getLastName());
		action.title = String.format("Examine %s %s", character.getFirstName(), character.getLastName());
		action.targetEntityId = character.getId();
		action.targetEntityType = Entity.Type.HUMAN_PLAYER_CHARACTER;
		actionList.add(action);
	}

	@Override
	public void accept(Anus anus) {}

	@Override
	public void accept(Ballsack ballsack) {}

	@Override
	public void accept(Breasts breasts) {}

	@Override
	public void accept(Hands hands) {}

	@Override
	public void accept(Mouth mouth) {}

	@Override
	public void accept(Penis penis) {}

	@Override
	public void accept(Stomach stomach) {}

	@Override
	public void accept(Vagina vagina) {}

	@Override
	public List<Action> getActions() {
		return actionList.build();
	}
}
