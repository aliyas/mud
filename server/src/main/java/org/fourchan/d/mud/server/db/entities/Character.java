package org.fourchan.d.mud.server.db.entities;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(
	name = "characters",
	uniqueConstraints = {@UniqueConstraint(columnNames = {"firstName", "lastName"})}
)
public class Character {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column(nullable = false, columnDefinition = "text")
	private String firstName;

	@Column(nullable = false, columnDefinition = "text")
	private String lastName;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(
		name = "players_id",
		nullable = false,
		referencedColumnName = "id"
	)
	private Player player;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumns({
		@JoinColumn(name = "mapPoint_embeddedPoint_x", referencedColumnName = "x", nullable = false),
		@JoinColumn(name = "mapPoint_embeddedPoint_y", referencedColumnName = "y", nullable = false),
		@JoinColumn(name = "mapPoint_embeddedPoint_z", referencedColumnName = "z", nullable = false),
	})
	private MapPoint location;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "spawnPoint_id", nullable = false, referencedColumnName = "id")
	private SpawnPoint spawnPoint;

	@OneToOne(optional = false, mappedBy = "character", cascade = CascadeType.PERSIST)
	private Body body;

	Character() {}

	public Character(String firstName, String lastName, Player player, SpawnPoint spawnPoint) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.player = player;
		this.location = spawnPoint.getMapPoint();
		this.spawnPoint = spawnPoint;
		this.body = new Body(this);
	}

	public int getId() {
		return id;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}
	
	public Player getPlayer() {
		return player;
	}

	public MapPoint getMapPoint() {
		return location;
	}

	public SpawnPoint getSpawnPoint() {
		return spawnPoint;
	}

	public void setMapPoint(MapPoint location) {
		this.location = location;
	}

	public Body getBody() {
		return body;
	}
}
