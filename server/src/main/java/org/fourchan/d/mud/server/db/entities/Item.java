package org.fourchan.d.mud.server.db.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "items")
public class Item {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column(nullable = false)
	private int itemKind;

	@Column(nullable = false)
	private int quantity;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(
		name = "body_id",
		nullable = false,
		referencedColumnName = "id"
	)
	private Body body;

	@OneToMany(
		mappedBy = "item",
		fetch = FetchType.LAZY
	)
	private List<ItemProperty> itemProperties;

	Item() {}
	public Item(Body body, ItemKind itemKind) {
		this.body = body;
		this.itemKind = itemKind.getId();
		this.itemProperties = new ArrayList<ItemProperty>();
	}

	public int getId() {
		return id;
	}

	public ItemKind getItemKind() {
		return ItemKind.parseFrom(itemKind);
	}

	public int getQuantity() {
		return quantity;
	}

	public List<ItemProperty> getItemProperties() {
		return itemProperties;
	}

	public void addItemProperty(ItemProperty itemProperties) {
		this.itemProperties.add(itemProperties);
	}

	public Body getBody() {
		return body;
	}
}
