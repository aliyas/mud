package org.fourchan.d.mud.server.game;

import org.fourchan.d.mud.server.db.DatabaseModule;

import com.google.inject.AbstractModule;

public class GameModule extends AbstractModule {

	@Override
	protected void configure() {
		install(new DatabaseModule());
	}

}
