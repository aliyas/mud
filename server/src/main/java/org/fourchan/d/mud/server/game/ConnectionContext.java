package org.fourchan.d.mud.server.game;

public class ConnectionContext {
	private String challenge;
	private int playerId;
	private String playerUsername;
	private int characterId;
	
	public ConnectionContext() {}

	public int getPlayerId() {
		return playerId;
	}

	public String getPlayerUsername() {
		return playerUsername;
	}

	public void setPlayer(String username, int playerId) {
		this.playerUsername = username;
		this.playerId = playerId;
	}

	public void setCharacter(int characterId) {
		this.characterId = characterId;
	}

	public int getCharacterId() {
		return characterId;
	}

	public String getChallenge() {
		return challenge;
	}

	public void setChallenge(String challenge) {
		this.challenge = challenge;
	}
}
