package org.fourchan.d.mud.server.game.actions;

import java.util.List;

import org.fourchan.d.mud.common.messages.common.Action;

public interface ActionProducer {
	List<Action> getActions();
}
