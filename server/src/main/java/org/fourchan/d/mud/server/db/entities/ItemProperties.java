package org.fourchan.d.mud.server.db.entities;

public enum ItemProperties {
	QUALITY("quality", true);

	private final String id;
	private final boolean isIntegerProperty;

	private ItemProperties(String id, boolean isIntegerProperty) {
		this.id = id;
		this.isIntegerProperty = isIntegerProperty;
	}

	public String getId() {
		return id;
	}

	public boolean isIntegerProperty() {
		return isIntegerProperty;
	}

	private static final ItemProperties[] VALUES = values();

	public static ItemProperties parseFrom(String id) {
		for (ItemProperties itemProperty : VALUES) {
			if (itemProperty.getId().equals(id)) {
				return itemProperty;
			}
		}
		return null;
	}
}
