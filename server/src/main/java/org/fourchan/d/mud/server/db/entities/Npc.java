package org.fourchan.d.mud.server.db.entities;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(
	name = "npcs",
	uniqueConstraints = {@UniqueConstraint(columnNames = {"", ""})}
)
public class Npc {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumns({
		@JoinColumn(name = "mapPoint_embeddedPoint_x", referencedColumnName = "x", nullable = false),
		@JoinColumn(name = "mapPoint_embeddedPoint_y", referencedColumnName = "y", nullable = false),
		@JoinColumn(name = "mapPoint_embeddedPoint_z", referencedColumnName = "z", nullable = false),
	})
	private MapPoint location;

	@Column(nullable = false)
	private int npcKind;

	@OneToOne(optional = false, mappedBy = "npc", cascade = CascadeType.PERSIST)
	private Body body;

	Npc() {}

	public Npc(NpcKind npcKind, MapPoint location) {
		this.npcKind = npcKind.getId();
		this.location = location;
		this.body = new Body(this);
	}

	public int getId() {
		return id;
	}

	public MapPoint getMapPoint() {
		return location;
	}

	public void setMapPoint(MapPoint location) {
		this.location = location;
	}

	public Body getBody() {
		return body;
	}

	public NpcKind getNpcKind() {
		return NpcKind.parseFrom(npcKind);
	}
}
