package org.fourchan.d.mud.server.game.map.types;

import org.fourchan.d.mud.common.map.MapPointId;

import com.google.common.collect.ImmutableMap;

public final class AbstractMapPoints {
	private AbstractMapPoints() {}

	public static final Grass GRASS = new Grass();
	public static final ImmutableMap<MapPointId, AbstractMapPoint> MAP_POINTS =
			ImmutableMap.<MapPointId, AbstractMapPoint>builder()
			.put(GRASS.getId(), GRASS)
			.build();
}
