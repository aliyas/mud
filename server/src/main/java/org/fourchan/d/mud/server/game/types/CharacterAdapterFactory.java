package org.fourchan.d.mud.server.game.types;

import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Fetch;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Root;
import javax.persistence.metamodel.EntityType;

import org.fourchan.d.mud.server.db.entities.Body;
import org.fourchan.d.mud.server.db.entities.Character;
import org.fourchan.d.mud.server.db.entities.sex.Anus;
import org.fourchan.d.mud.server.db.entities.sex.Ballsack;
import org.fourchan.d.mud.server.db.entities.sex.Breasts;
import org.fourchan.d.mud.server.db.entities.sex.Hands;
import org.fourchan.d.mud.server.db.entities.sex.Mouth;
import org.fourchan.d.mud.server.db.entities.sex.Penis;
import org.fourchan.d.mud.server.db.entities.sex.Stomach;
import org.fourchan.d.mud.server.db.entities.sex.Vagina;
import org.fourchan.d.mud.server.db.entities.util.Transactions;

public final class CharacterAdapterFactory {
	private CharacterAdapterFactory() {}

	public static Optional<CharacterAdapter> getByName(String firstName, String lastName, EntityManagerFactory emf) {
		EntityManager em = emf.createEntityManager();
		return Transactions.tryWithRollbackException(() -> {
				CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

				CriteriaQuery<Character> cq = criteriaBuilder.createQuery(Character.class);
				Root<Character> from = cq.from(Character.class);
				cq.where(
						criteriaBuilder.and(
								criteriaBuilder.equal(from.get("firstName"), firstName)),
								criteriaBuilder.equal(from.get("lastName"), lastName));

				final TypedQuery<Character> tq = em.createQuery(cq);
				return Optional.of(new CharacterAdapter(tq.getSingleResult(), emf));
		},
		em,
		Optional.empty());
	}

	public static Optional<CharacterAdapter> getById(int id, EntityManagerFactory emf) {
		EntityManager em = emf.createEntityManager();
		return Transactions.tryWithRollbackException(() -> {
				return Optional.of(new CharacterAdapter(em.find(Character.class, id), emf));
		},
		em,
		Optional.empty());
	}

	public static Optional<CharacterAdapter> getByIdWithBodyParts(int id, EntityManagerFactory emf) {
		EntityManager em = emf.createEntityManager();
		return Transactions.tryWithRollbackException(() -> {
				CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

				CriteriaQuery<Character> cq = criteriaBuilder.createQuery(Character.class);
				Root<Character> from = cq.from(Character.class);
				Fetch<Character, Body> bodyFetch = from.fetch("body", JoinType.LEFT);
				EntityType<Body> bodyModel = em.getMetamodel().entity(Body.class);

				bodyFetch.fetch(bodyModel.getList("anuses", Anus.class), JoinType.LEFT);
				bodyFetch.fetch(bodyModel.getList("penises", Penis.class), JoinType.LEFT);
				bodyFetch.fetch(bodyModel.getList("vaginas", Vagina.class), JoinType.LEFT);
				bodyFetch.fetch(bodyModel.getList("ballsacks", Ballsack.class), JoinType.LEFT);
				bodyFetch.fetch(bodyModel.getList("breasts", Breasts.class), JoinType.LEFT);
				bodyFetch.fetch(bodyModel.getList("mouths", Mouth.class), JoinType.LEFT);
				bodyFetch.fetch(bodyModel.getList("stomachs", Stomach.class), JoinType.LEFT);
				bodyFetch.fetch(bodyModel.getList("hands", Hands.class), JoinType.LEFT);

				cq.select(from);
				cq.where(criteriaBuilder.equal(from.get("id"), id));
				cq.distinct(true);

				final TypedQuery<Character> tq = em.createQuery(cq);
				return Optional.of(new CharacterAdapter(tq.getSingleResult(), emf));
			},
			em,
			Optional.empty());
	}
}
