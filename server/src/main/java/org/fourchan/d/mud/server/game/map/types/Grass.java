package org.fourchan.d.mud.server.game.map.types;

import org.fourchan.d.mud.common.map.MapPointId;

class Grass extends AbstractMapPoint {
	Grass() {
		super(Transparency.TRANSPARENT,
				Traversal.CAN_MOVE_THROUGH,
				MapPointId.GRASS);
	}
}
