package org.fourchan.d.mud.server.db;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Singleton;

public class DatabaseModule extends AbstractModule {

	@Override
	protected void configure() {}

	@Provides
	@Singleton
	EntityManagerFactory provideEntityManagerFactory() {
		return Persistence.createEntityManagerFactory("org.fourchan.d.mud.server");
	}
}
