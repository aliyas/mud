package org.fourchan.d.mud.server;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

public class StockadeCommandLine {
	private static final String DEBUG_LONG_OPT = "debug";
	private static final Option DEBUG =
			Option.builder("d").required(false).longOpt(DEBUG_LONG_OPT).build();
	private static final String HOST_LONG_OPT = "host";
	private static final Option HOST =
			Option.builder().required(true).hasArg().longOpt(HOST_LONG_OPT).build();
	private static final String PORT_LONG_OPT = "port";
	private static final Option PORT =
			Option.builder().required(true).hasArg().longOpt(PORT_LONG_OPT).build();
	private static final String KEYSTORE_PASS_LONG_OPT = "keystorePass";
	private static final Option KEYSTORE_PASS =
			Option.builder().required(true).hasArg().longOpt(KEYSTORE_PASS_LONG_OPT).build();
	private static final String PRIVATE_KEY_LONG_OPT = "privateKeyPass";
	private static final Option PRIVATE_KEY_PASS =
			Option.builder().required(true).hasArg().longOpt(PRIVATE_KEY_LONG_OPT).build();
	private static final String USER_KEYSTORE_PASS_LONG_OPT = "userKeystorePass";
	private static final Option USER_KEYSTORE_PASS =
			Option.builder().required(true).hasArg().longOpt(USER_KEYSTORE_PASS_LONG_OPT).build();

    private static final CommandLineParser parser = new DefaultParser();
    private static final HelpFormatter formatter = new HelpFormatter();
    private final CommandLine commandLine;
    private final Options options;
	
	StockadeCommandLine(String[] args) throws ParseException {
		options = new Options();
        options.addOption(DEBUG);
        options.addOption(HOST);
        options.addOption(PORT);
        options.addOption(KEYSTORE_PASS);
        options.addOption(PRIVATE_KEY_PASS);
        options.addOption(USER_KEYSTORE_PASS);
        commandLine = parser.parse(options, args);
	}

	public void printHelp() {
		formatter.printHelp("stockade-server", options);
	}

	public boolean isDebugEnabled() {
		return toBoolean(DEBUG_LONG_OPT);
	}
	
	public String getHost() {
		return commandLine.getOptionValue(HOST_LONG_OPT);
	}

	public int getPort() {
		return toInt(commandLine.getOptionValue(PORT_LONG_OPT));
	}

	public String getServerKeyStorePassword() {
		return commandLine.getOptionValue(KEYSTORE_PASS_LONG_OPT);
	}

	public String getServerPrivateKeyPassword() {
		return commandLine.getOptionValue(PRIVATE_KEY_LONG_OPT);
	}

	public String getUserKeyStorePassword() {
		return commandLine.getOptionValue(USER_KEYSTORE_PASS_LONG_OPT);
	}

	private static int toInt(String s) {
		return new Integer(s);
	}

	private boolean toBoolean(String s) {
		return s != null && commandLine.hasOption(s);
	}
}
