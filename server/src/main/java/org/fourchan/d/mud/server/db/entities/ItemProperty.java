package org.fourchan.d.mud.server.db.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(
	name = "itemProperties",
	uniqueConstraints = {@UniqueConstraint(columnNames = {"itemProperty", "item_id"})}
)
public class ItemProperty {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(
		name = "item_id",
		nullable = false,
		referencedColumnName = "id"
	)
	private Item item;

	@Column(nullable = false, columnDefinition = "text")
	private String itemProperty;

	@Column(nullable = false, columnDefinition = "text")
	private String value;

	@Column(nullable = false)
	private boolean isInt;

	ItemProperty() {}
	public ItemProperty(Item item, ItemProperties itemProperty, String value) {
		this.item = item;
		this.itemProperty = itemProperty.getId();
		this.value = value;
		this.isInt = false;
	}

	public ItemProperty(Item item, ItemProperties itemProperty, int value) {
		this.item = item;
		this.itemProperty = itemProperty.getId();
		this.value = String.format("%d", value);
		this.isInt = true;
	}

	public ItemProperties getItemProperty() {
		return ItemProperties.parseFrom(itemProperty);
	}

	public String getStringValue() {
		if (isInt) {
			throw new RuntimeException("Do not call getStringValue for an ItemProperty that is an int value.");
		}
		return this.value;
	}

	public int getIntegerValue() {
		if (!isInt) {
			throw new RuntimeException("Do not call getIntegerValue for an ItemProperty that is NOT an int value.");
		}
		return Integer.valueOf(this.value);
	}

	public boolean isInteger() {
		return isInt;
	}
}
