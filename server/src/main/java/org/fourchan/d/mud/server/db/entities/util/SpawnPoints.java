package org.fourchan.d.mud.server.db.entities.util;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.fourchan.d.mud.server.db.entities.SpawnPoint;

public final class SpawnPoints {
	private SpawnPoints() {}

	public static List<SpawnPoint> getAllSpawnPoints(EntityManager manager) {
		CriteriaBuilder criteriaBuilder = manager.getCriteriaBuilder();
		
		CriteriaQuery<SpawnPoint> cq = criteriaBuilder.createQuery(SpawnPoint.class);
		Root<SpawnPoint> from = cq.from(SpawnPoint.class);
		
        CriteriaQuery<SpawnPoint> all = cq.select(from);
        TypedQuery<SpawnPoint> allQuery = manager.createQuery(all);

		return allQuery.getResultList();
	}

	public static SpawnPoint getByName(String name, EntityManager manager) {
		CriteriaBuilder criteriaBuilder = manager.getCriteriaBuilder();

		CriteriaQuery<SpawnPoint> cq = criteriaBuilder.createQuery(SpawnPoint.class);
		Root<SpawnPoint> from = cq.from(SpawnPoint.class);
		
		cq.where(criteriaBuilder.equal(from.get("name"), name));
		
		final TypedQuery<SpawnPoint> tq = manager.createQuery(cq);
		return tq.getSingleResult();
	}
}
