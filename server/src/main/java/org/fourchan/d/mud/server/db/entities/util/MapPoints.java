package org.fourchan.d.mud.server.db.entities.util;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.fourchan.d.mud.common.map.MapPointId;
import org.fourchan.d.mud.common.map.Visibility;
import org.fourchan.d.mud.server.db.entities.Character;
import org.fourchan.d.mud.server.db.entities.EmbeddedPoint;
import org.fourchan.d.mud.server.db.entities.MapPoint;

public final class MapPoints {
	private MapPoints() {}

	public static boolean exists(EmbeddedPoint p, EntityManager manager) {
		CriteriaBuilder criteriaBuilder = manager.getCriteriaBuilder();

		CriteriaQuery<Long> cq = criteriaBuilder.createQuery(Long.class);
		Root<MapPoint> from = cq.from(MapPoint.class);

		cq.select(criteriaBuilder.count(from));
		cq.where(
				criteriaBuilder.and(
						criteriaBuilder.equal(from.get("embeddedPoint").get("x"), p.getX()),
						criteriaBuilder.equal(from.get("embeddedPoint").get("y"), p.getY()),
						criteriaBuilder.equal(from.get("embeddedPoint").get("z"), p.getZ())));

	    final TypedQuery<Long> tq = manager.createQuery(cq);
	    return tq.getSingleResult() > 0;
	}

	public static MapPoint get(EmbeddedPoint p, EntityManager manager) {
		CriteriaBuilder criteriaBuilder = manager.getCriteriaBuilder();

		CriteriaQuery<MapPoint> criteriaQuery = criteriaBuilder.createQuery(MapPoint.class);
		Root<MapPoint> from = criteriaQuery.from(MapPoint.class);
		criteriaQuery.where(
				criteriaBuilder.and(
						criteriaBuilder.equal(from.get("embeddedPoint").get("x"), p.getX()),
						criteriaBuilder.equal(from.get("embeddedPoint").get("y"), p.getY()),
						criteriaBuilder.equal(from.get("embeddedPoint").get("z"), p.getZ())));

		TypedQuery<MapPoint> tq = manager.createQuery(criteriaQuery);
		return tq.getSingleResult();
	}

	public static enum MapPointCreateOrUpdatedResult {
		SUCCESSFUL,
		ERROR,
	}

	public static MapPointCreateOrUpdatedResult createOrUpdate(int x, int y, int z, MapPointId mapPointId, EntityManager manager) {
		return Transactions.tryWithRollbackException(
				() -> {
					EmbeddedPoint p = new EmbeddedPoint(x, y ,z);
					if (exists(p, manager)) {
						MapPoint mapPoint = get(p, manager);
						mapPoint.setMapPointId(mapPointId);
						return MapPointCreateOrUpdatedResult.SUCCESSFUL;
					}
					manager.persist(new MapPoint(p, mapPointId));
					return MapPointCreateOrUpdatedResult.SUCCESSFUL;
				},
				manager,
				MapPointCreateOrUpdatedResult.ERROR);
	}

	public static List<MapPoint> getDelta(int x, int y, int z, int delta, EntityManager manager) {
		CriteriaBuilder criteriaBuilder = manager.getCriteriaBuilder();

		CriteriaQuery<MapPoint> criteriaQuery = criteriaBuilder.createQuery(MapPoint.class);
		Root<MapPoint> from = criteriaQuery.from(MapPoint.class);
		criteriaQuery.where(
				criteriaBuilder.and(
						criteriaBuilder.between(from.get("embeddedPoint").get("x"), x-delta, x+delta),
						criteriaBuilder.between(from.get("embeddedPoint").get("y"), y-delta, y+delta),
						criteriaBuilder.equal(from.get("embeddedPoint").get("z"), z)));

		TypedQuery<MapPoint> tq = manager.createQuery(criteriaQuery);
		return tq.getResultList();
	}

	public static List<MapPoint> getVisibleAround(int characterId, EntityManager manager) {
		Character character = manager.find(Character.class, characterId);
		EmbeddedPoint point = character.getMapPoint().getEmbeddedPoint();
		return getDelta(point.getX(), point.getY(), point.getZ(), Visibility.DEFAULT_XY_DISTANCE, manager);
	}
}
