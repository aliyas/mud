package org.fourchan.d.mud.server.db.entities;

public enum PlayerKind {
	NORMAL(1, "Normal"),
	GUEST(2, "Guest");

	private int id;
	private String name;

	private PlayerKind(int id, String name) {
		this.id = id;
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	private static final PlayerKind[] VALUES = values();

	public static PlayerKind parseFrom(int id) {
		for (PlayerKind playerKind : VALUES) {
			if (playerKind.getId() == id) {
				return playerKind;
			}
		}
		return null;
	}
}
