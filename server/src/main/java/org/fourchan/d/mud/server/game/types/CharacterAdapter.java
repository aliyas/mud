package org.fourchan.d.mud.server.game.types;

import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;

import org.fourchan.d.mud.common.map.Direction;
import org.fourchan.d.mud.common.messages.common.Entity;
import org.fourchan.d.mud.server.converters.MapPointConverter;
import org.fourchan.d.mud.server.db.entities.Character;
import org.fourchan.d.mud.server.db.entities.EmbeddedPoint;
import org.fourchan.d.mud.server.db.entities.MapPoint;
import org.fourchan.d.mud.server.db.entities.sex.Anus;
import org.fourchan.d.mud.server.db.entities.sex.Ballsack;
import org.fourchan.d.mud.server.db.entities.sex.Breasts;
import org.fourchan.d.mud.server.db.entities.sex.Hands;
import org.fourchan.d.mud.server.db.entities.sex.Mouth;
import org.fourchan.d.mud.server.db.entities.sex.Penis;
import org.fourchan.d.mud.server.db.entities.sex.Stomach;
import org.fourchan.d.mud.server.db.entities.sex.Vagina;
import org.fourchan.d.mud.server.db.entities.util.MapPointMetadatas;
import org.fourchan.d.mud.server.db.entities.util.MapPoints;
import org.fourchan.d.mud.server.db.entities.util.Transactions;
import org.fourchan.d.mud.server.game.types.CharacterVisitor.VisitType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.ImmutableList;

public class CharacterAdapter extends ManagedEntity {
	private static final Logger logger = LoggerFactory.getLogger(CharacterAdapter.class);
	private Character character;

	public CharacterAdapter(Character character, EntityManagerFactory em) {
		super(em);
		this.character = character;
	}

	@RequiresTransactionToModify
	public void visit(CharacterVisitor visitor) {
		List<VisitType> visitOrder = visitor.visitOrder();
		if (visitOrder == null || visitOrder.isEmpty()) {
			defaultVisitOrder(visitor);
		} else {
			customVisitOrder(visitor, visitOrder);
		}
	}

	public org.fourchan.d.mud.common.messages.common.Character toCharacterMessage() {
		return toCharacterMessage(character);
	}

	public static org.fourchan.d.mud.common.messages.common.Character toCharacterMessage(Character from) {
		org.fourchan.d.mud.common.messages.common.Character to = new org.fourchan.d.mud.common.messages.common.Character();
		to.firstName = from.getFirstName();
		to.lastName = from.getLastName();
		return to;
	}

	public Entity toEntityMessage() {
		Entity entity = new Entity();
		entity.type = Entity.Type.HUMAN_PLAYER_CHARACTER;
		entity.id = character.getId();
		entity.name = String.format("%s %s", character.getFirstName(), character.getLastName());
		entity.description = "A player character.";
		entity.location = MapPointConverter.toMessage(character.getMapPoint().getEmbeddedPoint());
		return entity;
	}

	public List<CharacterAdapter> getCharactersNear(int xyDelta, int zDelta) {
		EntityManager em = super.getEntityManager();
		return Transactions.tryWithRollbackException(() -> {
				int x = character.getMapPoint().getEmbeddedPoint().getX();
				int y = character.getMapPoint().getEmbeddedPoint().getY();
				int z = character.getMapPoint().getEmbeddedPoint().getZ();

				CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

				CriteriaQuery<Character> cq = criteriaBuilder.createQuery(Character.class);
				Root<Character> root = cq.from(Character.class);
				Join<Character, MapPoint> from = root.join("location");
				if (zDelta > 0) {
					cq.where(
							criteriaBuilder.and(
									criteriaBuilder.between(from.get("embeddedPoint").get("x"), x-xyDelta, x+xyDelta),
									criteriaBuilder.between(from.get("embeddedPoint").get("y"), y-xyDelta, y+xyDelta),
									criteriaBuilder.between(from.get("embeddedPoint").get("z"), z-zDelta, z+zDelta),
									criteriaBuilder.notEqual(root.get("id"), character.getId())));
				} else {
					cq.where(
							criteriaBuilder.and(
									criteriaBuilder.between(from.get("embeddedPoint").get("x"), x-xyDelta, x+xyDelta),
									criteriaBuilder.between(from.get("embeddedPoint").get("y"), y-xyDelta, y+xyDelta),
									criteriaBuilder.equal(from.get("embeddedPoint").get("z"), z),
									criteriaBuilder.notEqual(root.get("id"), character.getId())));
				}

				final TypedQuery<Character> tq = em.createQuery(cq);
				return tq.getResultList().stream()
						.map(c -> new CharacterAdapter(c, super.emf))
						.collect(Collectors.toList());
			},
			em,
			ImmutableList.of());
	}

	public static enum MoveResult {
		SUCCESSFUL,
		NO_END_POINT,
		MOVEMENT_DISALLOWED,
		ERROR,
	}

	@RequiresTransaction
	public MoveResult moveCharacter(Direction d) {
		EntityManager em = super.getEntityManager();
		return Transactions.tryWithRollbackException(
				() -> {
					MoveResult result = MoveResult.SUCCESSFUL;
					EmbeddedPoint toLocation =
							character.getMapPoint().getEmbeddedPoint().getNeighbor(d);
					if (!MapPoints.exists(toLocation, em)) {
						result = MoveResult.NO_END_POINT;
					} else {
						MapPoint toPoint = MapPoints.get(toLocation, em);
						switch (MapPointMetadatas.MAP_POINT_ID_METADATA_MAP
								.get(toPoint.getMapPointId())
								.movement) {
						case MOVEMENT_ALWAYS_ALLOWED:
							character.setMapPoint(toPoint);
							break;
						case MOVEMENT_ALWAYS_DISALLOWED:
						default:
							result = MoveResult.MOVEMENT_DISALLOWED;
							break;
						}
					}
					return result;
				},
				em,
				MoveResult.ERROR);
	}

	public EmbeddedPoint getLocation() {
		return character.getMapPoint().getEmbeddedPoint();
	}

	public int getId() {
		return character.getId();
	}

	Character getCharacter() {
		return character;
	}

	private void customVisitOrder(CharacterVisitor visitor, List<VisitType> visitOrder) {
		for (VisitType type : visitOrder) {
			switch(type) {
			case ANUS:
				visitAnus(visitor);
				break;
			case BALLSACK:
				visitBallsack(visitor);
				break;
			case BREASTS:
				visitBreasts(visitor);
				break;
			case CHARACTER:
				visitCharacter(visitor);
				break;
			case HANDS:
				visitHands(visitor);
				break;
			case MOUTH:
				visitMouth(visitor);
				break;
			case PENIS:
				visitPenis(visitor);
				break;
			case STOMACH:
				visitStomach(visitor);
				break;
			case VAGINA:
				visitVagina(visitor);
				break;
			default:
				logger.error("Unknown custom visit order type {}", type);
			}
		}
	}

	private void defaultVisitOrder(CharacterVisitor visitor) {
		visitCharacter(visitor);
		visitMouth(visitor);
		visitBreasts(visitor);
		visitHands(visitor);
		visitStomach(visitor);
		visitVagina(visitor);
		visitPenis(visitor);
		visitBallsack(visitor);
		visitAnus(visitor);
	}

	private void visitCharacter(CharacterVisitor visitor) {
		visitor.accept(character);
	}

	private void visitMouth(CharacterVisitor visitor) {
		for (Mouth mouth : character.getBody().getMouths()) {
			visitor.accept(mouth);
		}
	}

	private void visitBreasts(CharacterVisitor visitor) {
		for (Breasts breasts : character.getBody().getBreasts()) {
			visitor.accept(breasts);
		}
	}

	private void visitHands(CharacterVisitor visitor) {
		for (Hands hands : character.getBody().getHands()) {
			visitor.accept(hands);
		}
	}

	private void visitStomach(CharacterVisitor visitor) {
		for (Stomach stomach : character.getBody().getStomachs()) {
			visitor.accept(stomach);
		}
	}

	private void visitVagina(CharacterVisitor visitor) {
		for (Vagina vagina : character.getBody().getVaginas()) {
			visitor.accept(vagina);
		}
	}

	private void visitPenis(CharacterVisitor visitor) {
		for (Penis penis : character.getBody().getPenises()) {
			visitor.accept(penis);
		}
	}

	private void visitBallsack(CharacterVisitor visitor) {
		for (Ballsack ballsack : character.getBody().getBallsacks()) {
			visitor.accept(ballsack);
		}
	}

	private void visitAnus(CharacterVisitor visitor) {
		for (Anus anus : character.getBody().getAnuses()) {
			visitor.accept(anus);
		}
	}

	@Override
	protected void fetchInTransaction() {
		character = super.getEntityManager().find(Character.class, character.getId());
	}
}
