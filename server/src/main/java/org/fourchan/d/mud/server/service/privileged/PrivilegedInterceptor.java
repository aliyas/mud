package org.fourchan.d.mud.server.service.privileged;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.Optional;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.fourchan.d.mud.server.db.entities.Player;
import org.fourchan.d.mud.server.db.entities.PlayerPermissions;
import org.java_websocket.WebSocket;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Provider;

public class PrivilegedInterceptor implements MethodInterceptor {
	private static final Logger logger = LoggerFactory.getLogger(PrivilegedInterceptor.class);

	public enum Permission {
		INVALID, MAP_EDIT
	}

	@Retention(RetentionPolicy.RUNTIME)
	@Target(ElementType.METHOD)
	public @interface Privileged {
		Permission[] requiredPermissions() default Permission.INVALID;
	}

	private final Provider<PlayerProvider> provider;

	public PrivilegedInterceptor(Provider<PlayerProvider> provider) {
		this.provider = provider;
	}

	@Override
	public Object invoke(MethodInvocation invocation) throws Throwable {
		Optional<Privileged> maybePrivilegedAnnotation =
				Optional.ofNullable(invocation.getMethod().getAnnotation(Privileged.class));
		Optional<WebSocket> maybeWebSocket = getWebSocketArgument(invocation.getArguments());
		if (!maybePrivilegedAnnotation.isPresent()
				|| !maybeWebSocket.isPresent()) {
			logger.error("Deny proceed: No privileged annotation or no websocket arg for {}", invocation.getMethod().getName());
			return null;
		}
		Optional<Player> maybePlayer =
				provider.get().getPlayer(maybeWebSocket.get());
		if (!maybePlayer.isPresent()) {
			logger.info("Deny proceed: No player for the given connection at {}", maybeWebSocket.get().getRemoteSocketAddress());
			return null;
		}
		Player player = maybePlayer.get();
		PlayerPermissions playerPermissions = player.getPermissions();
		for (Permission permissionToTest : maybePrivilegedAnnotation.get().requiredPermissions()) {
			switch (permissionToTest) {
			case INVALID:
				logger.warn("Deny proceed: Player {} requesting invalid permission", player.getName());
				return null;
			case MAP_EDIT:
				if (!playerPermissions.canEditMap()) {
					logger.warn("Deny proceed: Player {} does not have permission to edit the map", player.getName());
					return null;
				}
				break;
			default:
				logger.warn("Deny proceed: Player {} requesting invalid permission", player.getName());
				return null;
			}
		}
		return invocation.proceed();
	}

	private Optional<WebSocket> getWebSocketArgument(Object[] arguments) {
		for (Object o : arguments) {
			if (o instanceof WebSocket) {
				return Optional.of((WebSocket) o);
			}
		}
		return Optional.empty();
	}
}
