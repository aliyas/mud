package org.fourchan.d.mud.server.converters;

import java.util.stream.Collectors;

import org.fourchan.d.mud.common.messages.common.Player;
import org.fourchan.d.mud.server.db.entities.PlayerKind;
import org.fourchan.d.mud.server.game.types.CharacterAdapter;

import com.google.common.collect.ImmutableList;

public class PlayerConverter {
	private PlayerConverter() {}

	public static Player toMessage(
			org.fourchan.d.mud.server.db.entities.Player from) {
		Player to = new Player();
		if (from.getCharacters() == null || from.getCharacters().isEmpty()) {
			to.characters = ImmutableList.of();
		} else {
			to.characters = from.getCharacters().stream()
					.map(character -> CharacterAdapter.toCharacterMessage(character))
					.collect(Collectors.toList());
		}
		to.username = from.getName();
		to.permissions = PlayerPermissionsConverter.toMessage(from.getPermissions());
		to.playerKind = convert(from.getPlayerKind());
		return to;
	}

	private static org.fourchan.d.mud.common.messages.common.Player.PlayerKind
			convert(PlayerKind kind) {
		switch(kind) {
		case NORMAL:
			return org.fourchan.d.mud.common.messages.common.Player.PlayerKind.NORMAL;
		case GUEST:
			return org.fourchan.d.mud.common.messages.common.Player.PlayerKind.GUEST;
		default:
			return org.fourchan.d.mud.common.messages.common.Player.PlayerKind.UNKNOWN;
		}
	}
}
