package org.fourchan.d.mud.server.db.entities.util;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.fourchan.d.mud.server.db.entities.Player;
import org.fourchan.d.mud.server.db.entities.PlayerKind;

public final class Players {
	private Players() {}

	private static final int GUEST_NAME_ATTEMPTS = 3;

	public static boolean exists(String name, EntityManager manager) {
	    CriteriaBuilder criteriaBuilder = manager.getCriteriaBuilder();

	    CriteriaQuery<Long> cq = criteriaBuilder.createQuery(Long.class);
	    Root<Player> from = cq.from(Player.class);

	    cq.select(criteriaBuilder.count(from));
	    cq.where(criteriaBuilder.equal(from.get("name"), name));

	    final TypedQuery<Long> tq = manager.createQuery(cq);
	    return tq.getSingleResult() > 0;
	}

	public static enum PlayerCreated {
		SUCCESSFUL,
		NAME_TAKEN,
		ERROR,
	}

	public static class PlayerCreatedResult {
		public PlayerCreated playerCreated;
		public Player player;
		public PlayerCreatedResult() {
			playerCreated = PlayerCreated.ERROR;
		}
	}
	
	public static PlayerCreatedResult createNonGuestPlayer(String name, EntityManager manager) {
		Player player = new Player(name, PlayerKind.NORMAL);
		return doCreatePlayer(player, manager);
	}

	public static PlayerCreatedResult createGuestPlayer(EntityManager manager) {
		PlayerCreatedResult result = new PlayerCreatedResult();
		result.playerCreated = PlayerCreated.NAME_TAKEN;
		int numberAttempts = 0;
		while (numberAttempts < GUEST_NAME_ATTEMPTS && result.playerCreated == PlayerCreated.NAME_TAKEN) {
			String name = GuestNames.getRandomName();
			Player player = new Player(name, PlayerKind.GUEST);
			result = doCreatePlayer(player, manager);
			numberAttempts++;
		}
		return result;
	}

	public static PlayerCreatedResult promotePlayerFromGuestToNormal(int id, String username, EntityManager manager) {
		return Transactions.tryWithRollbackException(
				() -> {
					PlayerCreatedResult result = new PlayerCreatedResult();
					result.playerCreated = PlayerCreated.NAME_TAKEN;
					if (exists(username, manager)) {
						return result;
					}
					result.player = manager.find(Player.class, id);
					result.player.convertToNonGuest(username);
					result.playerCreated = PlayerCreated.SUCCESSFUL;
					return result;
				},
				manager,
				new PlayerCreatedResult());
	}

	public static Player getPlayer(int id, EntityManager manager) {
		return
			Transactions.tryWithRollbackException(() -> {
				CriteriaBuilder criteriaBuilder = manager.getCriteriaBuilder();
		
				CriteriaQuery<Player> cq = criteriaBuilder.createQuery(Player.class);
				Root<Player> from = cq.from(Player.class);

				cq = cq.select(from).where(criteriaBuilder.equal(from.get("id"), id));

				final TypedQuery<Player> tq = manager.createQuery(cq);
				return tq.getSingleResult();
			}, manager, null);
	}

	public static Player getPlayer(String name, EntityManager manager) {
		return
			Transactions.tryWithRollbackException(() -> {
				CriteriaBuilder criteriaBuilder = manager.getCriteriaBuilder();
		
				CriteriaQuery<Player> cq = criteriaBuilder.createQuery(Player.class);
				Root<Player> from = cq.from(Player.class);

				cq = cq.select(from).where(criteriaBuilder.equal(from.get("name"), name));

				final TypedQuery<Player> tq = manager.createQuery(cq);
				return tq.getSingleResult();
			}, manager, null);
	}

	private static PlayerCreatedResult doCreatePlayer(Player player, EntityManager manager) {
		return Transactions.tryWithRollbackException(
				() -> {
					PlayerCreatedResult result = new PlayerCreatedResult();
					result.player = player;
					result.playerCreated = PlayerCreated.NAME_TAKEN;
					if (!exists(player.getName(), manager)) {
						manager.persist(player);
						result.playerCreated = PlayerCreated.SUCCESSFUL;
					}
					return result;
				},
				manager,
				new PlayerCreatedResult());
	}
}
