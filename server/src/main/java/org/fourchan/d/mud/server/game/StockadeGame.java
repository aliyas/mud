package org.fourchan.d.mud.server.game;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import org.fourchan.d.mud.common.map.Direction;
import org.fourchan.d.mud.common.map.MapPointId;
import org.fourchan.d.mud.common.map.Point;
import org.fourchan.d.mud.common.map.Visibility;
import org.fourchan.d.mud.common.messages.common.Entity;
import org.fourchan.d.mud.common.messages.common.Item;
import org.fourchan.d.mud.common.messages.responses.ActionPayload;
import org.fourchan.d.mud.common.messages.responses.EntityPayload;
import org.fourchan.d.mud.common.messages.responses.MapPayload;
import org.fourchan.d.mud.server.converters.MapPointConverter;
import org.fourchan.d.mud.server.db.entities.MapPoint;
import org.fourchan.d.mud.server.db.entities.Player;
import org.fourchan.d.mud.server.db.entities.SpawnPoint;
import org.fourchan.d.mud.server.db.entities.util.Characters;
import org.fourchan.d.mud.server.db.entities.util.MapPoints;
import org.fourchan.d.mud.server.db.entities.util.Characters.CharacterCreated;
import org.fourchan.d.mud.server.db.entities.util.MapPoints.MapPointCreateOrUpdatedResult;
import org.fourchan.d.mud.server.db.entities.util.Players;
import org.fourchan.d.mud.server.db.entities.util.SpawnPoints;
import org.fourchan.d.mud.server.game.actions.CharacterActionDeterminer;
import org.fourchan.d.mud.server.game.description.ExamineCharacterDescriber;
import org.fourchan.d.mud.server.game.description.ItemDescriptionGenerator;
import org.fourchan.d.mud.server.game.types.CharacterAdapter;
import org.fourchan.d.mud.server.game.types.CharacterAdapter.MoveResult;
import org.fourchan.d.mud.server.game.types.CharacterAdapterFactory;
import org.fourchan.d.mud.server.game.types.ItemAdapter;
import org.fourchan.d.mud.server.game.types.ItemAdapterFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fourchan.d.mud.server.db.entities.util.Players.PlayerCreatedResult;

import com.google.common.collect.ImmutableList;
import com.google.inject.Inject;

public class StockadeGame {
	private static final Logger logger = LoggerFactory.getLogger(StockadeGame.class);
	private static final int NEAR_DELTA = 2;

	private final EntityManagerFactory entityManagerFactory;

	@Inject
	public StockadeGame(EntityManagerFactory entityManagerFactory) {
		this.entityManagerFactory = entityManagerFactory;
	}

	public Player getPlayer(int id) {
		return doWithEntityManager(em -> Players.getPlayer(id, em));
	}

	public Player getPlayer(String username) {
		return doWithEntityManager(em -> Players.getPlayer(username, em));
	}

	public boolean playerExists(String username) {
		return doWithEntityManager(em -> Players.exists(username, em));
	}

	public PlayerCreatedResult createNormalPlayer(String username) {
		return doWithEntityManager(em -> Players.createNonGuestPlayer(username, em));
	}

	public PlayerCreatedResult createGuestPlayer() {
		return doWithEntityManager(em -> Players.createGuestPlayer(em));
	}

	public PlayerCreatedResult promoteGuestPlayer(int id, String username) {
		return doWithEntityManager(em -> Players.promotePlayerFromGuestToNormal(id, username, em));
	}

	public Optional<CharacterAdapter> getCharacter(String firstName, String lastName) {
		return CharacterAdapterFactory.getByName(firstName, lastName, getEntityManagerFactory());
	}

	public CharacterCreated createCharacter(
			String firstName,
			String lastName,
			int playerId,
			String startingSex,
			SpawnPoint spawnPoint) {
		return doWithEntityManager(em -> Characters.createCharacter(firstName, lastName, playerId, spawnPoint, startingSex, em));
	}

	public List<SpawnPoint> getSpawnPoints() {
		return doWithEntityManager(em -> SpawnPoints.getAllSpawnPoints(em));
	}

	public SpawnPoint getSpawnPoint(String name) {
		return doWithEntityManager(em -> SpawnPoints.getByName(name, em));
	}

	public MoveResult moveCharacter(int characterId, Direction direction) {
		Optional<CharacterAdapter> maybeCharacter =
				CharacterAdapterFactory.getById(characterId, getEntityManagerFactory());
		if (!maybeCharacter.isPresent()) {
			logger.error("Cannot move character with id {}: Does not exist", characterId);
			return MoveResult.ERROR;
		}
		CharacterAdapter character = maybeCharacter.get();
		character.useTransaction(getEntityManagerFactory().createEntityManager());
		return character.moveCharacter(direction);
	}

	public MapPointCreateOrUpdatedResult updateMap(Point point, MapPointId id) {
		return doWithEntityManager(em -> MapPoints.createOrUpdate(point.getX(), point.getY(), point.getZ(), id, em));
	}

	public Optional<MapPayload> getMapPayload(int characterId) {
		List<MapPoint> points = doWithEntityManager(em -> MapPoints.getVisibleAround(characterId, em));
		Optional<CharacterAdapter> maybeCharacter = CharacterAdapterFactory.getById(characterId, getEntityManagerFactory());
		if (!maybeCharacter.isPresent()) {
			logger.error("Cannot create map payload for character with id {}: Does not exist", characterId);
			return Optional.empty();
		}
		return Optional.of(new MapPayload(MapPointConverter.toMessage(points), MapPointConverter.toMessage(maybeCharacter.get().getLocation())));
	}

	public Optional<EntityPayload> getEntityPayload(int characterId, Set<Integer> activeCharacterIds) {
		Optional<CharacterAdapter> maybeCharacter = CharacterAdapterFactory.getById(characterId, getEntityManagerFactory());
		if (!maybeCharacter.isPresent()) {
			logger.error("Cannot create entity payload for character with id {}: Does not exist", characterId);
			return Optional.empty();
		}
		CharacterAdapter character = maybeCharacter.get();
		List<CharacterAdapter> nearbyCharacters =
				character.getCharactersNear(Visibility.DEFAULT_XY_DISTANCE, Visibility.DEFAULT_Z_DISTANCE);
		List<Entity> nearbyEntites =
				nearbyCharacters.stream()
						.filter((CharacterAdapter c) -> activeCharacterIds.contains(c.getId()))
						.map(adapter -> adapter.toEntityMessage())
						.collect(Collectors.toList());
		return Optional.of(new EntityPayload(nearbyEntites));
	}

	public Optional<ActionPayload> getActionPayload(int characterId, Set<Integer> activeCharacterIds) {
		Optional<CharacterAdapter> maybeCharacter = CharacterAdapterFactory.getById(characterId, getEntityManagerFactory());
		if (!maybeCharacter.isPresent()) {
			logger.error("Cannot create action payload for character with id {}: Does not exist", characterId);
			return Optional.empty();
		}
		CharacterAdapter character = maybeCharacter.get();
		List<CharacterAdapter> nearbyCharacters =
				character.getCharactersNear(Visibility.DEFAULT_XY_DISTANCE, Visibility.DEFAULT_Z_DISTANCE)
						.stream()
						.filter((CharacterAdapter c) -> activeCharacterIds.contains(c.getId()))
						.collect(Collectors.toList());
		CharacterActionDeterminer actions = new CharacterActionDeterminer();
		nearbyCharacters.stream().forEach(c -> c.visit(actions));
		return Optional.of(new ActionPayload(actions.getActions()));
	}

	public List<CharacterAdapter> getNearbyCharacters(int characterId, Set<Integer> activeCharacterIds) {
		Optional<CharacterAdapter> maybeCharacter = CharacterAdapterFactory.getById(characterId, getEntityManagerFactory());
		if (!maybeCharacter.isPresent()) {
			return ImmutableList.of();
		}
		return maybeCharacter.get()
				.getCharactersNear(
						Visibility.DEFAULT_XY_DISTANCE + NEAR_DELTA,
						Visibility.DEFAULT_Z_DISTANCE + NEAR_DELTA);
	}

	public String examineEntity(Entity.Type entityType, int entityId) {
		switch (entityType) {
		case HUMAN_PLAYER_CHARACTER:
			// TODO: Better engine
			Optional<CharacterAdapter> maybeCharacter =
					CharacterAdapterFactory.getById(entityId, getEntityManagerFactory());
			if (!maybeCharacter.isPresent()) {
				logger.error("Cannot examine HUMAN_PLAYER_CHARACTER for character with id {}: Does not exist", entityId);
				throw new RuntimeException("Cannot describe human player character.");
			}
			CharacterAdapter character = maybeCharacter.get();
			ExamineCharacterDescriber describer = new ExamineCharacterDescriber();
			character.visit(describer);
			return describer.getDescription();
		default:
			logger.error("Cannot examine entity for entity id {} and type {}: Type does not exist", entityId, entityType);
			throw new RuntimeException("Cannot describe entity with type: " + entityType.toString());
		}
	}

	public List<Item> getInventoryItems(int characterId) {
		List<ItemAdapter> items = ItemAdapterFactory.getInventoryOfCharacter(characterId, getEntityManagerFactory());
		ImmutableList.Builder<Item> itemsListBuilder = ImmutableList.builder();
		for (ItemAdapter item : items) {
			itemsListBuilder.add(item.toItemMessage(new ItemDescriptionGenerator()));
		}
		return itemsListBuilder.build();
	}

	private <T> T doWithEntityManager(Function<EntityManager, T> function) {
		return function.apply(getEntityManagerFactory().createEntityManager());
	}

	private EntityManagerFactory getEntityManagerFactory() {
		return entityManagerFactory;
	}
}
