package org.fourchan.d.mud.server.keys;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.fourchan.d.mud.server.ServerModule;
import org.fourchan.d.mud.server.keys.KeyStore.ServerKeyStore;

import com.google.inject.AbstractModule;
import com.google.inject.BindingAnnotation;
import com.google.inject.Provides;
import com.google.inject.Scopes;
import com.google.inject.Singleton;

public class KeyStoreModule extends AbstractModule {
	private static final String STORE_TYPE = "JKS";
	private static final String BC_STORE_TYPE = "BKS";
	private static final String BC_PROVIDER = "BC";

	@BindingAnnotation
	@Target({ElementType.METHOD, ElementType.PARAMETER})
	@Retention(RetentionPolicy.RUNTIME)
	public @interface ServerKeyStorePassword {}

	@BindingAnnotation
	@Target({ElementType.METHOD, ElementType.PARAMETER})
	@Retention(RetentionPolicy.RUNTIME)
	public @interface UserKeyStorePassword {}
	
	@BindingAnnotation
	@Target({ElementType.METHOD, ElementType.PARAMETER})
	@Retention(RetentionPolicy.RUNTIME)
	public @interface UserKeyStoreFilePath {}
	
	@BindingAnnotation
	@Target({ElementType.METHOD, ElementType.PARAMETER})
	@Retention(RetentionPolicy.RUNTIME)
	public @interface UserKeyStore {}

	@Override
	protected void configure() {
		bind(KeyStore.class).in(Scopes.SINGLETON);
	}

	@Provides
	@Singleton
	@ServerKeyStore
	java.security.KeyStore provideKeyStorePath(@ServerKeyStorePassword String keyStorePassword) throws Exception {
		java.security.KeyStore keyStore = java.security.KeyStore.getInstance(STORE_TYPE);
		InputStream inputStream = ServerModule.class.getResourceAsStream("/org/fourchan/d/mud/server/keystoreServer");
		if (inputStream == null) {
			throw new RuntimeException("Could not find the key store");
		}
        keyStore.load(inputStream, keyStorePassword.toCharArray());
        return keyStore;
	}

	@Provides
	@Singleton
	@UserKeyStore
	java.security.KeyStore provideUserKeyStore(@UserKeyStorePassword String keyStorePassword, @UserKeyStoreFilePath String filePath) throws Exception {
		java.security.KeyStore keyStore = java.security.KeyStore.getInstance(BC_STORE_TYPE, BC_PROVIDER);
		try (InputStream inputStream = new FileInputStream(filePath)) {
	        keyStore.load(inputStream, keyStorePassword.toCharArray());
	        return keyStore;
		} catch (FileNotFoundException e) {}
        keyStore.load(null, keyStorePassword.toCharArray());
        return keyStore;
	}

	@Provides
	@Singleton
	@UserKeyStoreFilePath
	public String provideUserKeyStoreFilePath() {
		return "usersKeystore.bks";
	}
}
