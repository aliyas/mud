package org.fourchan.d.mud.server.service;

import java.security.SecureRandom;

public final class SecureRandomString {
	private SecureRandomString() {}
	private static final char[] CHARS = {
			'0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
			'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j',
			'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't',
			'u', 'v', 'w', 'x', 'y', 'z',
			'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J',
			'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T',
			'U', 'V', 'W', 'X', 'Y', 'Z'
	};
	private static final SecureRandom random = new SecureRandom();
	
	public static String getString(int length) {
		char[] buffer = new char[length];
		for (int i = 0; i < length; i++) {
			buffer[i] = CHARS[random.nextInt(CHARS.length)];
		}
		return buffer.toString();
	}
}
