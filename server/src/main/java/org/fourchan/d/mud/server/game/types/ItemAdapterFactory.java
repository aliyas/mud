package org.fourchan.d.mud.server.game.types;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Root;

import org.fourchan.d.mud.server.db.entities.Body;
import org.fourchan.d.mud.server.db.entities.Item;
import org.fourchan.d.mud.server.db.entities.ItemKind;
import org.fourchan.d.mud.server.db.entities.ItemProperty;
import org.fourchan.d.mud.server.db.entities.util.Transactions;

import com.google.common.collect.ImmutableList;

public class ItemAdapterFactory {
	private ItemAdapterFactory() {}

	/** EntityManager must be the same one that loaded the CharacterAdapter. */
	public static Optional<ItemAdapter> create(CharacterAdapter character, ItemKind itemKind) {
		Item item = new Item(character.getCharacter().getBody(), itemKind);
		EntityManager em = character.getEntityManager();
		return Transactions.tryWithRollbackException(() -> {
			character.getCharacter().getBody().addItem(item);
			em.persist(item);
			return Optional.of(new ItemAdapter(item, character.emf));
		},
		em,
		Optional.empty());
	}

	public static Optional<ItemAdapter> fetchWithProperties(int itemId, EntityManagerFactory emf) {
		EntityManager em = emf.createEntityManager();
		return Transactions.tryWithRollbackException(() -> {
			CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

			CriteriaQuery<Item> cq = criteriaBuilder.createQuery(Item.class);
			Root<Item> from = cq.from(Item.class);

			from.fetch(from.getModel().getList("itemProperties", ItemProperty.class), JoinType.LEFT);

			cq.select(from);
			cq.where(criteriaBuilder.equal(from.get("id"), itemId));
			cq.distinct(true);

			final TypedQuery<Item> tq = em.createQuery(cq);
			return Optional.of(new ItemAdapter(tq.getSingleResult(), emf));
		},
		em,
		Optional.empty());
	}

	public static List<ItemAdapter> getInventoryOfCharacter(int characterId, EntityManagerFactory emf) {
		EntityManager em = emf.createEntityManager();
		return Transactions.tryWithRollbackException(() -> {
			CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

			CriteriaQuery<Item> cq = criteriaBuilder.createQuery(Item.class);
			Root<Item> from = cq.from(Item.class);
			Join<Item, Body> bodyJoin = from.join("body", JoinType.LEFT);
			Join<Item, Character> characterJoin = bodyJoin.join("character", JoinType.LEFT);

			cq.select(from);
			cq.where(criteriaBuilder.equal(characterJoin.get("id"), characterId));

			final TypedQuery<Item> tq = em.createQuery(cq);
			return tq.getResultList().stream()
					.map(item -> new ItemAdapter(item, emf))
					.collect(Collectors.toList());
		},
		em,
		ImmutableList.of());
	}
}
