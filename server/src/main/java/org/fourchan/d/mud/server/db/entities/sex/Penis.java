package org.fourchan.d.mud.server.db.entities.sex;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.fourchan.d.mud.common.messages.common.sex.GirthWidth;
import org.fourchan.d.mud.common.messages.common.sex.Kind;
import org.fourchan.d.mud.common.messages.common.sex.LengthDepth;
import org.fourchan.d.mud.server.db.entities.Body;

@Entity
@Table(name = "penises")
public class Penis {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(
		name = "body_id",
		nullable = false,
		referencedColumnName = "id"
	)
	private Body body;

	@Column(nullable = false)
	private int length;

	@Column(nullable = false)
	private int girth;

	@Column(nullable = false)
	private Kind kind;

	Penis() {}
	public Penis(Body body) {
		this.body = body;
	}

	public LengthDepth getLength() {
		return LengthDepth.parseFrom(length);
	}

	public void setLength(LengthDepth length) {
		this.length = length.getSize();
	}

	public GirthWidth getGirth() {
		return GirthWidth.parseFrom(girth);
	}

	public void setGirth(GirthWidth girth) {
		this.girth = girth.getSize();
	}

	public Kind getKind() {
		return kind;
	}

	public void setKind(Kind kind) {
		this.kind = kind;
	}
}
