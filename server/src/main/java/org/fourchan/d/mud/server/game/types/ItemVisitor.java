package org.fourchan.d.mud.server.game.types;

import java.util.Set;

import org.fourchan.d.mud.server.db.entities.Item;
import org.fourchan.d.mud.server.db.entities.ItemProperties;

public interface ItemVisitor {

	/** Returning null or empty set implies no properties. */
	Set<ItemProperties> propertyFilter();

	void visitItem(Item item);
	void visitStringProperty(ItemProperties property, String value);
	void visitIntegerProperty(ItemProperties property, int value);
}
