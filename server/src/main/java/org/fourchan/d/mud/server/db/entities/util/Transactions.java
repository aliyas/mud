package org.fourchan.d.mud.server.db.entities.util;

import java.util.concurrent.Callable;

import javax.persistence.EntityManager;
import javax.persistence.RollbackException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class Transactions {
	private Transactions() {}

	private static final Logger logger = LoggerFactory.getLogger(Transactions.class);

	public static <T> T tryWithRollbackException(Callable<T> toTry, EntityManager manager, T errorValue) {
		manager.getTransaction().begin();
		try {
			T value = toTry.call();
			manager.getTransaction().commit();
			return value;
		} catch (RollbackException e) {
			logger.error("Rollback exception occurred within a transaction, rolling back", e);
			manager.getTransaction().rollback();
		} catch (Exception e) {
			logger.error("Exception occurred within a transaction, rolling back", e);
			e.printStackTrace();
			manager.getTransaction().rollback();
		} finally {
			manager.close();
		}
		return errorValue;
	}
}
