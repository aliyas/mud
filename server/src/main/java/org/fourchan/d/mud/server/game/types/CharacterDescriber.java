package org.fourchan.d.mud.server.game.types;

public interface CharacterDescriber extends CharacterVisitor {
	public String getDescription();
}
