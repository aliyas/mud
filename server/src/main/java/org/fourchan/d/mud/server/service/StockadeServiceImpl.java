package org.fourchan.d.mud.server.service;

import java.io.ByteArrayInputStream;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.nio.charset.Charset;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.Base64;
import java.util.Optional;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ExecutorService;

import org.fourchan.d.mud.common.Constants;
import org.fourchan.d.mud.common.StockadeService;
import org.fourchan.d.mud.common.map.Point;
import org.fourchan.d.mud.common.messages.ServerMessage;
import org.fourchan.d.mud.common.messages.requests.CharacterSelectedRequest;
import org.fourchan.d.mud.common.messages.requests.CreateCharacterRequest;
import org.fourchan.d.mud.common.messages.requests.ExamineEntityRequest;
import org.fourchan.d.mud.common.messages.requests.InventoryRequest;
import org.fourchan.d.mud.common.messages.requests.MoveRequest;
import org.fourchan.d.mud.common.messages.requests.PlayerInfoRequest;
import org.fourchan.d.mud.common.messages.requests.privileged.MapUpdateRequest;
import org.fourchan.d.mud.common.messages.responses.CharacterSelectedResponse;
import org.fourchan.d.mud.common.messages.responses.CreateCharacterResponse;
import org.fourchan.d.mud.common.messages.responses.InventoryResponse;
import org.fourchan.d.mud.common.messages.responses.LoginChallengeResponse;
import org.fourchan.d.mud.common.messages.responses.LoginResponse;
import org.fourchan.d.mud.common.messages.responses.LogoutResponse;
import org.fourchan.d.mud.common.messages.responses.NewGuestResponse;
import org.fourchan.d.mud.common.messages.responses.PlayerInfoResponse;
import org.fourchan.d.mud.common.messages.responses.PromoteGuestToNonGuestResponse;
import org.fourchan.d.mud.common.messages.responses.RegisterResponse;
import org.fourchan.d.mud.common.messages.responses.SpawnPointResponse;
import org.fourchan.d.mud.common.messages.responses.TextPayload;
import org.fourchan.d.mud.common.messages.responses.TextPayload.Kind;
import org.fourchan.d.mud.server.converters.PlayerConverter;
import org.fourchan.d.mud.server.converters.SpawnPointConverter;
import org.fourchan.d.mud.server.db.entities.Player;
import org.fourchan.d.mud.server.db.entities.SpawnPoint;
import org.fourchan.d.mud.server.db.entities.util.MapPoints.MapPointCreateOrUpdatedResult;
import org.fourchan.d.mud.server.db.entities.util.Players.PlayerCreatedResult;
import org.fourchan.d.mud.server.game.ConnectionContext;
import org.fourchan.d.mud.server.game.StockadeGame;
import org.fourchan.d.mud.server.game.types.CharacterAdapter;
import org.fourchan.d.mud.server.game.types.CharacterAdapter.MoveResult;
import org.fourchan.d.mud.server.keys.KeyStore;
import org.fourchan.d.mud.server.service.ServiceModule.ServerThreadpool;
import org.fourchan.d.mud.server.service.privileged.PlayerProvider;
import org.fourchan.d.mud.server.service.privileged.PrivilegedInterceptor.Permission;
import org.fourchan.d.mud.server.service.privileged.PrivilegedInterceptor.Privileged;
import org.java_websocket.WebSocket;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.inject.BindingAnnotation;
import com.google.inject.Inject;

public class StockadeServiceImpl implements StockadeService, PlayerProvider {
	private static final Logger logger = LoggerFactory.getLogger(StockadeServiceImpl.class);

	@BindingAnnotation
	@Target({ElementType.METHOD, ElementType.PARAMETER})
	@Retention(RetentionPolicy.RUNTIME)
	public @interface UnauthenticatedMap {}

	@BindingAnnotation
	@Target({ElementType.METHOD, ElementType.PARAMETER})
	@Retention(RetentionPolicy.RUNTIME)
	public @interface AuthenticatedMap {}
	
	private static final String BEGIN_CERT = "-----BEGIN CERTIFICATE-----";
	private static final String END_CERT = "-----END CERTIFICATE-----";
	private static final int CHALLENGE_LENGTH = 2048;
	private static final Charset UTF8 = Charset.forName("UTF-8");

	private final ConcurrentMap<WebSocket, ConnectionContext> unauthenticatedConnections;
	private final ConcurrentMap<WebSocket, ConnectionContext> authenticatedConnections;
	private final ConcurrentMap<Integer, WebSocket> charactersToSockets;
	private final ExecutorService executorService;
	private final StockadeGame game;
	private final Gson gson;
	private final KeyStore keyStore;

	@Inject
	StockadeServiceImpl(
			@UnauthenticatedMap ConcurrentMap<WebSocket, ConnectionContext> unauthenticatedConnections,
			@AuthenticatedMap ConcurrentMap<WebSocket, ConnectionContext> authenticatedConnections,
			ConcurrentMap<Integer, WebSocket> charactersToSockets,
			@ServerThreadpool ExecutorService executorService,
			StockadeGame game,
			Gson gson,
			KeyStore keyStore) {
		this.unauthenticatedConnections = unauthenticatedConnections;
		this.authenticatedConnections = authenticatedConnections;
		this.charactersToSockets = charactersToSockets;
		this.executorService = executorService;
		this.game = game;
		this.gson = gson;
		this.keyStore = keyStore;
	}

	@Override
	public Optional<Player> getPlayer(WebSocket socket) {
		ConnectionContext context = authenticatedConnections.get(socket);
		if (context == null) {
			return Optional.empty();
		}
		Player player = game.getPlayer(context.getPlayerId());
		return Optional.of(player);
	}

	public void addUnauthenticatedConnection(WebSocket socket) {
		unauthenticatedConnections.put(socket, new ConnectionContext());
	}

	public void removeConnection(WebSocket socket) {
		unauthenticatedConnections.remove(socket);
		authenticatedConnections.remove(socket);
		executorService.execute(() -> {
			for (int i : charactersToSockets.keySet()) {
				if (charactersToSockets.get(i).equals(socket)) {
					charactersToSockets.remove(i);
					updateNearbyCharacters(i);
				}
			}
		});
	}

	public void handleMessage(WebSocket socket, String message) {
		ServerMessage serverMessage = gson.fromJson(message, ServerMessage.class);
		serverMessage.visit(this, socket);
	}

	@Override
	public void handleRegisterRequest(WebSocket socket, String username, String publicKey) {
		if (username.isEmpty()) {
			send(socket, new RegisterResponse("Username is empty"));
			return;
		} else if (!username.matches(Constants.USERNAME_REGEX)) {
			send(socket, new RegisterResponse("Username must contain lower or upper case latin characters only"));
			return;
		}
		String publicKeyTrimmed = publicKey.substring(BEGIN_CERT.length(), publicKey.length()-END_CERT.length());
		Base64.Decoder decoder = Base64.getMimeDecoder();
		X509Certificate cert = null;
		try {
			CertificateFactory certFactory = CertificateFactory.getInstance("X.509", "BC");
			cert = (X509Certificate) certFactory.generateCertificate(new ByteArrayInputStream(decoder.decode(publicKeyTrimmed)));
		} catch (CertificateException e) {
			logger.warn("Failed to parse certificate given by client", e);
			send(socket, new RegisterResponse("Cannot understand certificate"));
			return;
		} catch (NoSuchProviderException e) {
			logger.error("Could not use the provider to parse client certificate", e);
		}
		RegisterResponse response = null;
		PlayerCreatedResult playerCreatedResult = game.createNormalPlayer(username);
		switch (playerCreatedResult.playerCreated) {
		case NAME_TAKEN:
			response = new RegisterResponse("Username is already taken");
			send(socket, response);
			return;
		case ERROR:
		default:
			response = new RegisterResponse("An internal error occurred");
			send(socket, response);
			return;
		case SUCCESSFUL:
			// fallthrough
		}
		switch (keyStore.addCertToKeyStore(username, cert)) {
		case SUCCESSFUL:
			response = new RegisterResponse();
			break;
		case ALREADY_CONTAINS_ALIAS:
			response = new RegisterResponse("Username is already taken");
			break;
		case ERROR_OCCURED:
			response = new RegisterResponse("An internal error occurred");
			break;
		default:
			response = new RegisterResponse("An internal error occurred. Shame the developer!");
			break;
		}
		send(socket, response);
	}

	@Override
	public void handleNewGuestRequest(WebSocket socket) {
		ConnectionContext context = unauthenticatedConnections.get(socket);
		if (context == null) {
			logger.error("Cannot find context in handleNewGuestRequest");
			send(socket, new NewGuestResponse("Bad request."));
			return;
		}
		NewGuestResponse response = null;
		PlayerCreatedResult result = game.createGuestPlayer();
		switch (result.playerCreated) {
		case NAME_TAKEN:
			response = new NewGuestResponse("Username is already taken");
			send(socket, response);
			return;
		case ERROR:
		default:
			response = new NewGuestResponse("An internal error occurred");
			send(socket, response);
			return;
		case SUCCESSFUL:
			// fallthrough
		}
		Player player = result.player;
		if (player == null) {
			logger.error("Guest player is null");
			send(socket, new LoginResponse("Failed to login to guest account: internal error."));
			return;
		}
		if (authenticatedConnections.values()
				.stream()
				.anyMatch(c -> c.getPlayerUsername().equals(player.getName()))) {
			logger.error("Somehow another guest is logged into the same guest as guest {}", player.getName());
			send(socket, new LoginResponse("Failed to login to guest account: internal error."));
			return;
		}
	    context.setPlayer(player.getName(), player.getId());
	    response = new NewGuestResponse(PlayerConverter.toMessage(player));
	    unauthenticatedConnections.remove(socket);
	    authenticatedConnections.put(socket, context);
	    send(socket, response);
	}

	@Override
	public void handlePromoteGuestToNonGuestRequest(WebSocket socket, String username, String publicKey) {
		ConnectionContext context = authenticatedConnections.get(socket);
		if (context == null) {
			logger.error("Cannot find context in handlePromoteGuestToNonGuestRequest");
			send(socket, new PromoteGuestToNonGuestResponse("Bad request."));
			return;
		}
		if (username.isEmpty()) {
			send(socket, new PromoteGuestToNonGuestResponse("Username is empty"));
			return;
		} else if (!username.matches(Constants.USERNAME_REGEX)) {
			send(socket, new PromoteGuestToNonGuestResponse("Username must contain lower or upper case latin characters only"));
			return;
		}
		String publicKeyTrimmed = publicKey.substring(BEGIN_CERT.length(), publicKey.length()-END_CERT.length());
		Base64.Decoder decoder = Base64.getMimeDecoder();
		X509Certificate cert = null;
		try {
			CertificateFactory certFactory = CertificateFactory.getInstance("X.509", "BC");
			cert = (X509Certificate) certFactory.generateCertificate(new ByteArrayInputStream(decoder.decode(publicKeyTrimmed)));
		} catch (CertificateException e) {
			logger.warn("Failed to parse certificate given by client", e);
			send(socket, new PromoteGuestToNonGuestResponse("Cannot understand certificate"));
			return;
		} catch (NoSuchProviderException e) {
			logger.error("Could not use the provider to parse client certificate", e);
		}
		PromoteGuestToNonGuestResponse response = null;
		if (game.playerExists(username)) {
			send(socket, new PromoteGuestToNonGuestResponse("Username is already taken"));
		}
		switch (keyStore.addCertToKeyStore(username, cert)) {
		case SUCCESSFUL:
			break;
		case ALREADY_CONTAINS_ALIAS:
			logger.error("Guest player name is available in DB but unavailable in keystore: {}", username);
			response = new PromoteGuestToNonGuestResponse("Username is already taken");
			break;
		case ERROR_OCCURED:
			response = new PromoteGuestToNonGuestResponse("An internal error occurred");
			break;
		default:
			response = new PromoteGuestToNonGuestResponse("An internal error occurred. Shame the developer!");
			break;
		}
		PlayerCreatedResult playerCreated = game.promoteGuestPlayer(context.getPlayerId(), username);
		switch (playerCreated.playerCreated) {
		case ERROR:
			logger.warn("Could not promote guest to a guest requesting name '{}'", username);
			response = new PromoteGuestToNonGuestResponse("Internal error occurred");
		case NAME_TAKEN:
			response = new PromoteGuestToNonGuestResponse("Username is already taken");
		case SUCCESSFUL:
			response = new PromoteGuestToNonGuestResponse(PlayerConverter.toMessage(playerCreated.player));
		}
		send(socket, response);
	}

	@Override
	public void handleLoginChallengeRequest(WebSocket socket) {
		ConnectionContext context = unauthenticatedConnections.get(socket);
		if (context == null) {
			logger.error("Cannot find context in handleLoginChallengeRequest");
			send(socket, new LoginChallengeResponse("Bad request.", null));
			return;
		}
		Base64.Encoder encoder = Base64.getEncoder();
		String challenge = encoder.encodeToString(SecureRandomString.getString(CHALLENGE_LENGTH).getBytes(UTF8));
		context.setChallenge(challenge);
		send(socket, new LoginChallengeResponse(challenge));
	}

	@Override
	public void handleLoginRequest(WebSocket socket, String username, String answer) {
		ConnectionContext context = unauthenticatedConnections.get(socket);
		PublicKey publicKey = keyStore.getKey(username);
		if (publicKey == null || context == null) {
			logger.error("Cannot find context or public key in handleLoginRequest for user {}", username);
			send(socket, new LoginResponse("Failed to login: incorrect username or password."));
			return;
		}
		try {
			Base64.Decoder decoder = Base64.getDecoder();
			Signature signature = Signature.getInstance("SHA256withRSA");
	        signature.initVerify(publicKey);
	        signature.update(decoder.decode(context.getChallenge()));
		    if (!signature.verify(decoder.decode(answer))) {
				send(socket, new LoginResponse("Failed to login: incorrect username or password."));
		    	return;
		    }
			if (authenticatedConnections.values()
					.stream()
					.anyMatch(c -> c.getPlayerUsername().equals(username))) {
				send(socket, new LoginResponse("Failed to login: user is already logged in."));
				return;
			}
			Player player = game.getPlayer(username);
			if (player == null) {
				send(socket, new LoginResponse("Failed to login: incorrect username or password."));
		    	return;
			}
		    context.setPlayer(player.getName(), player.getId());
		    LoginResponse response = new LoginResponse(PlayerConverter.toMessage(player));
		    unauthenticatedConnections.remove(socket);
		    authenticatedConnections.put(socket, context);
		    send(socket, response);
		    return;
		} catch (NoSuchAlgorithmException | InvalidKeyException | SignatureException e) {
			logger.error("Cannot handle login request for {}: {}", username, e);
		}
		send(socket, new LoginResponse("Failed to login: incorrect username or password."));
	}

	@Override
	public void handleLogoutRequest(WebSocket socket) {
		ConnectionContext context = authenticatedConnections.get(socket);
		if (context == null) {
			logger.error("Cannot find context in handleLogoutRequest");
			send(socket, new LogoutResponse("Failed to logout."));
			return;
		}
		authenticatedConnections.remove(socket);
		unauthenticatedConnections.put(socket, new ConnectionContext());
		send(socket, new LogoutResponse());
		if (context.getCharacterId() > 0) {
			charactersToSockets.remove(context.getCharacterId(), socket);
			updateNearbyCharacters(context.getCharacterId());
		}
	}

	@Override
	public void handlePlayerInfoRequest(WebSocket socket, PlayerInfoRequest request) {
		ConnectionContext context = authenticatedConnections.get(socket);
		if (context == null) {
			logger.error("Cannot find context in handlePlayerInfoRequest");
			send(socket, new PlayerInfoResponse("Bad request."));
			return;
		}
		Player player = game.getPlayer(context.getPlayerId());
	    send(socket, new PlayerInfoResponse(PlayerConverter.toMessage(player)));
	}

	@Override
	public void handleCreateCharacterRequest(WebSocket socket, CreateCharacterRequest request) {
		ConnectionContext context = authenticatedConnections.get(socket);
		if (context == null) {
			logger.error("Cannot find context in handleCreateCharacterRequest");
			send(socket, new CreateCharacterResponse("Bad request."));
			return;
		}
		String firstName = request.getFirstName();
		String lastName = request.getLastName();
		String sexChoice = request.getSexChoice();
		String spawnChoice = request.getSpawnChoice();
		if (firstName.isEmpty()) {
			send(socket, new CreateCharacterResponse("Please enter a first name."));
			return;
		} else if (firstName.length() > Constants.MAX_LENGTH_CHARACTER_FIRST_NAME) {
			send(socket, new CreateCharacterResponse("First name is too long."));
			return;
		} else if (!firstName.matches(Constants.CHARACTER_NAME_REGEX)) {
			send(socket, new CreateCharacterResponse("First name must be upper and lower english letters only."));
			return;
		} else if (lastName.isEmpty()) {
			send(socket, new CreateCharacterResponse("Please enter a last name."));
			return;
		} else if (lastName.length() > Constants.MAX_LENGTH_CHARACTER_LAST_NAME) {
			send(socket, new CreateCharacterResponse("Last name is too long."));
			return;
		} else if (!lastName.matches(Constants.CHARACTER_NAME_REGEX)) {
			send(socket, new CreateCharacterResponse("Last name must be upper and lower english letters only."));
			return;
		} else if (!Constants.ALL_STARTING_SEXES.contains(sexChoice)) {
			send(socket, new CreateCharacterResponse("Invalid sex choice."));
			return;
		} else if (spawnChoice.isEmpty()) {
			send(socket, new CreateCharacterResponse("Please choose a spawn location."));
			return;
		}
		SpawnPoint spawnPoint = game.getSpawnPoint(spawnChoice);
		switch (game.createCharacter(firstName, lastName, context.getPlayerId(), sexChoice, spawnPoint)) {
		case NAME_TAKEN:
			send(socket, new CreateCharacterResponse("Username is already taken"));
			return;
		case INVALID_STARTING_SEX:
			send(socket, new CreateCharacterResponse("Invalid sex choice."));
			return;
		case ERROR:
		default:
			send(socket, new CreateCharacterResponse("An internal error occurred"));
			return;
		case SUCCESSFUL:
			// fallthrough
		}
		send(socket, new CreateCharacterResponse());
	}

	@Override
	public void handleSelectCharacterRequest(WebSocket socket, CharacterSelectedRequest request) {
		ConnectionContext context = authenticatedConnections.get(socket);
		if (context == null) {
			logger.error("Cannot find context in handleSelectCharacterRequest");
			send(socket, new CharacterSelectedResponse("Bad request."));
			return;
		}
		String firstName = request.getFirstName();
		String lastName = request.getLastName();
		Optional<CharacterAdapter> maybeCharacter = game.getCharacter(firstName, lastName);
		if (!maybeCharacter.isPresent()) {
			logger.error("Cannot select non-existent character '{} {}'", request.getFirstName(), request.getLastName());
			send(socket, new CharacterSelectedResponse("Bad request."));
			return;
		}
		CharacterAdapter character = maybeCharacter.get();
		context.setCharacter(character.getId());
		charactersToSockets.put(character.getId(), socket);
		send(socket, new CharacterSelectedResponse(character.toCharacterMessage()));
		updateNearbyCharacters(context.getCharacterId());
		sendAllUpdates(socket, context);
	}

	@Override
	public void handleMoveRequest(WebSocket socket, MoveRequest request) {
		ConnectionContext context = authenticatedConnections.get(socket);
		if (context == null) {
			logger.error("Cannot find context in handleMoveRequest");
			send(socket, new CharacterSelectedResponse("Bad request."));
			return;
		}
		MoveResult moveResult = game.moveCharacter(context.getCharacterId(), request.getDirection());
		switch (moveResult) {
		case SUCCESSFUL:
			updateNearbyCharacters(context.getCharacterId());
			sendAllUpdates(socket, context);
		case ERROR:
		case MOVEMENT_DISALLOWED:
		case NO_END_POINT:
		default:
			logger.error("Unrecognized move result '{}'", moveResult);
		}
	}

	@Override
	public void handleSpawnPointRequest(WebSocket socket) {
		ConnectionContext context = authenticatedConnections.get(socket);
		if (context == null) {
			logger.error("Cannot find context in handleSpawnPointRequest");
			send(socket, new SpawnPointResponse("Bad request."));
			return;
		}
		SpawnPointResponse response = new SpawnPointResponse(SpawnPointConverter.toMessage(game.getSpawnPoints()));
		send(socket, response);
	}

	@Privileged(requiredPermissions = {Permission.MAP_EDIT})
	@Override
	public void handleMapUpdateRequest(WebSocket socket, MapUpdateRequest request) {
		ConnectionContext context = authenticatedConnections.get(socket);
		if (context == null) {
			logger.error("Cannot find context in handleMapUpdateRequest");
			return;
		}
		Point point = request.getPoint();
		MapPointCreateOrUpdatedResult result = game.updateMap(point, request.getMapPointId());
		switch (result) {
		case ERROR:
			logger.error("Error updating map point {} to {}", request.getPoint(), request.getMapPointId());
			break;
		case SUCCESSFUL:
			updateNearbyCharacters(context.getCharacterId());
			executorService.execute(() -> sendMapPayload(socket, context));
			break;
		default:
			logger.error("Unrecognized update map result '{}'", result);
			break;
		}
	}

	@Override
	public void handleExamineEntityRequest(WebSocket socket, ExamineEntityRequest request) {
		ConnectionContext context = authenticatedConnections.get(socket);
		if (context == null) {
			logger.error("Cannot find context in handleExamineEntityRequest");
			return;
		}
		String examineText = game.examineEntity(request.getTargetEntityType(), request.getTargetEntityId());
		send(socket, new TextPayload(examineText, Kind.EXAMINE_TEXT));
	}

	@Override
	public void handleInventoryRequest(WebSocket socket, InventoryRequest request) {
		ConnectionContext context = authenticatedConnections.get(socket);
		if (context == null) {
			logger.error("Cannot find context in handleInventoryRequest");
			send(socket, new CharacterSelectedResponse("Bad request."));
			return;
		}
		send(socket, new InventoryResponse(game.getInventoryItems(context.getCharacterId())));
	}

	private void updateNearbyCharacters(int characterId) {
		executorService.execute(() -> {
			for (CharacterAdapter character : game.getNearbyCharacters(characterId, charactersToSockets.keySet())) {
				Optional<WebSocket> maybeSocket = Optional.ofNullable(charactersToSockets.get(character.getId()));
				if (!maybeSocket.isPresent()) {
					continue;
				}
				Optional<ConnectionContext> maybeContext = Optional.ofNullable(authenticatedConnections.get(maybeSocket.get()));
				if (!maybeContext.isPresent()) {
					continue;
				}
				sendAllUpdates(maybeSocket.get(), maybeContext.get());
			}
		});
	}

	private void sendAllUpdates(WebSocket socket, ConnectionContext context) {
		executorService.execute(() -> {
			sendMapPayload(socket, context);
			sendEntityPayload(socket, context);
			sendActionPayload(socket, context);
		});
	}

	private void sendMapPayload(WebSocket socket, ConnectionContext context) {
		game.getMapPayload(context.getCharacterId())
			.ifPresent(payload -> send(socket, payload));
	}

	private void sendEntityPayload(WebSocket socket, ConnectionContext context) {
		game.getEntityPayload(context.getCharacterId(), charactersToSockets.keySet())
			.ifPresent(payload -> send(socket, payload));
	}

	private void sendActionPayload(WebSocket socket, ConnectionContext context) {
		game.getActionPayload(context.getCharacterId(), charactersToSockets.keySet())
			.ifPresent(payload -> send(socket, payload));
	}

	private void send(WebSocket socket, Object message) {
		socket.send(gson.toJson(message));
	}
}
