package org.fourchan.d.mud.server.db.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "playerPermissions")
public class PlayerPermissions {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@OneToOne(optional = false)
	@JoinColumn(name = "players_id", referencedColumnName = "id", nullable = false)
	private Player player;

	private boolean canEditMap;

	PlayerPermissions() {}

	public PlayerPermissions(Player player) {
		this.player = player;
	}

	public PlayerPermissions(
			boolean canEditMap) {
		this.canEditMap = canEditMap;
	}

	public Player getPlayer() {
		return player;
	}

	public boolean canEditMap() {
		return canEditMap;
	}
}
