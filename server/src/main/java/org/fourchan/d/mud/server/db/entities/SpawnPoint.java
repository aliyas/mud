package org.fourchan.d.mud.server.db.entities;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "spawnPoint")
public class SpawnPoint {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column(nullable = false, unique = true, columnDefinition = "text")
	private String name;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumns({
		@JoinColumn(name = "mapPoint_embeddedPoint_x", referencedColumnName = "x", nullable = false),
		@JoinColumn(name = "mapPoint_embeddedPoint_y", referencedColumnName = "y", nullable = false),
		@JoinColumn(name = "mapPoint_embeddedPoint_z", referencedColumnName = "z", nullable = false),
	})
	private MapPoint spawnLocation;

	@OneToMany(
		mappedBy = "spawnPoint",
		fetch = FetchType.LAZY
	)
	private List<Character> characters;

	SpawnPoint() {}

	public SpawnPoint(String name, MapPoint spawnLocation) {
		this.name = name;
		this.spawnLocation = spawnLocation;
	}

	public MapPoint getMapPoint() {
		return spawnLocation;
	}

	public String getName() {
		return name;
	}

	@Override
	public String toString() {
		return String.format("{id: %d, name: %s, spawnLocation: %s}", id, name, spawnLocation);
	}
}
