package org.fourchan.d.mud.server.db.entities.util;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.fourchan.d.mud.common.Constants;
import org.fourchan.d.mud.common.messages.common.sex.GirthWidth;
import org.fourchan.d.mud.common.messages.common.sex.Kind;
import org.fourchan.d.mud.common.messages.common.sex.LengthDepth;
import org.fourchan.d.mud.server.db.entities.Body;
import org.fourchan.d.mud.server.db.entities.Character;
import org.fourchan.d.mud.server.db.entities.Player;
import org.fourchan.d.mud.server.db.entities.SpawnPoint;
import org.fourchan.d.mud.server.db.entities.sex.Anus;
import org.fourchan.d.mud.server.db.entities.sex.Ballsack;
import org.fourchan.d.mud.server.db.entities.sex.Breasts;
import org.fourchan.d.mud.server.db.entities.sex.Hands;
import org.fourchan.d.mud.server.db.entities.sex.Mouth;
import org.fourchan.d.mud.server.db.entities.sex.Penis;
import org.fourchan.d.mud.server.db.entities.sex.Stomach;
import org.fourchan.d.mud.server.db.entities.sex.Vagina;

public final class Characters {
	private Characters() {}

	public static boolean exists(String firstName, String lastName, EntityManager manager) {
	    CriteriaBuilder criteriaBuilder = manager.getCriteriaBuilder();

	    CriteriaQuery<Long> cq = criteriaBuilder.createQuery(Long.class);
	    Root<Character> from = cq.from(Character.class);

	    cq.select(criteriaBuilder.count(from));
	    cq.where(
	    		criteriaBuilder.and(
	    				criteriaBuilder.equal(from.get("firstName"), firstName)),
	    				criteriaBuilder.equal(from.get("lastName"), lastName));

	    final TypedQuery<Long> tq = manager.createQuery(cq);
	    return tq.getSingleResult() > 0;
	}

	public static enum CharacterCreated {
		SUCCESSFUL,
		NAME_TAKEN,
		ERROR,
		INVALID_STARTING_SEX,
	}
	
	public static CharacterCreated createCharacter(
			String firstName,
			String lastName,
			int playerId,
			SpawnPoint spawnPoint,
			String startingSex,
			EntityManager manager) {
		return Transactions.tryWithRollbackException(
				() -> {
					CharacterCreated result = CharacterCreated.NAME_TAKEN;
					Player player = manager.find(Player.class, playerId);
					Character character = new Character(firstName, lastName, player, spawnPoint);
					if (!exists(firstName, lastName, manager)) {
						player.addCharacter(character);
						result = createNewCharacterBody(character.getBody(), startingSex, manager);
						manager.persist(character);
					}
					return result;
				},
				manager,
				CharacterCreated.ERROR);
	}

	private static CharacterCreated createNewCharacterBody(Body body, String startingSexChoice, EntityManager manager) {
		if (startingSexChoice.equals(Constants.FEMALE_HUMAN_STARTING_SEX)) {
			Breasts breasts = new Breasts(body);
			breasts.setCount(2);
			breasts.setSize(GirthWidth.AVERAGE);
			breasts.setKind(Kind.HUMAN);
			manager.persist(breasts);
			body.addBreasts(breasts);

			Vagina vagina = new Vagina(body);
			vagina.setGirth(GirthWidth.AVERAGE);
			vagina.setLength(LengthDepth.AVERAGE);
			vagina.setKind(Kind.HUMAN);
			manager.persist(vagina);
			body.addVagina(vagina);
		} else if (startingSexChoice.equals(Constants.MALE_HUMAN_STARTING_SEX)) {
			Ballsack ballsack = new Ballsack(body);
			ballsack.setCount(2);
			ballsack.setSize(GirthWidth.AVERAGE);
			ballsack.setKind(Kind.HUMAN);
			manager.persist(ballsack);
			body.addBallsack(ballsack);

			Breasts breasts = new Breasts(body);
			breasts.setCount(2);
			breasts.setSize(GirthWidth.EXTREMELY_TINY);
			breasts.setKind(Kind.HUMAN);
			manager.persist(breasts);
			body.addBreasts(breasts);

			Penis penis = new Penis(body);
			penis.setGirth(GirthWidth.AVERAGE);
			penis.setLength(LengthDepth.AVERAGE);
			penis.setKind(Kind.HUMAN);
			manager.persist(penis);
			body.addPenis(penis);
		} else {
			return CharacterCreated.INVALID_STARTING_SEX;
		}
		Anus anus = new Anus(body);
		anus.setGirth(GirthWidth.AVERAGE);
		anus.setKind(Kind.HUMAN);
		manager.persist(anus);
		body.addAnus(anus);

		Hands hands = new Hands(body);
		hands.setSize(GirthWidth.AVERAGE);
		hands.setKind(Kind.HUMAN);
		manager.persist(hands);
		body.addHands(hands);

		Mouth mouth = new Mouth(body);
		mouth.setSize(GirthWidth.AVERAGE);
		mouth.setKind(Kind.HUMAN);
		manager.persist(mouth);
		body.addMouth(mouth);

		Stomach stomach = new Stomach(body);
		stomach.setKind(Kind.HUMAN);
		manager.persist(stomach);
		body.addStomach(stomach);

		return CharacterCreated.SUCCESSFUL;
	}
}
