package org.fourchan.d.mud.server.converters;

import java.util.List;

import org.fourchan.d.mud.common.messages.common.SpawnPoint;

import com.google.common.collect.ImmutableList;

public class SpawnPointConverter {
	private SpawnPointConverter() {}

	public static SpawnPoint toMessage(
			org.fourchan.d.mud.server.db.entities.SpawnPoint from) {
		SpawnPoint to = new SpawnPoint();
		to.mapPoint = MapPointConverter.toMessage(from.getMapPoint());
		to.name = from.getName();
		return to;
	}

	public static List<SpawnPoint> toMessage(
			List<org.fourchan.d.mud.server.db.entities.SpawnPoint> from) {
		ImmutableList.Builder<SpawnPoint> builder =
				ImmutableList.builder();
		for (org.fourchan.d.mud.server.db.entities.SpawnPoint fromElement : from) {
			builder.add(toMessage(fromElement));
		}
		return builder.build();
	}
}
