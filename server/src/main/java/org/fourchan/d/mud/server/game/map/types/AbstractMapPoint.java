package org.fourchan.d.mud.server.game.map.types;

import org.fourchan.d.mud.common.map.MapPointId;

public abstract class AbstractMapPoint {
	public static enum Transparency {
		TRANSPARENT,
		OPAQUE
	}
	
	public static enum Traversal {
		CAN_MOVE_THROUGH,
		BLOCKS_MOVEMENT
	}

	private Transparency transparency;
	private Traversal traversal;
	private MapPointId pointId;

	protected AbstractMapPoint(Transparency transparency, Traversal traversal, MapPointId pointId) {
		this.transparency = transparency;
		this.traversal = traversal;
		this.pointId = pointId;
	}

	public Transparency getTransparency() {
		return this.transparency;
	}

	public Traversal getTraversal() {
		return this.traversal;
	}

	public MapPointId getId() {
		return this.pointId;
	}
}
