package org.fourchan.d.mud.server.db.entities.util;

import java.time.Instant;
import java.util.Random;

final class GuestNames {
	private GuestNames() {}

	private static final int LENGTH = 10;
	private static final String SENTINEL = "+";
	private static final String LETTERS = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
	private static final Random random = new Random(Instant.now().toEpochMilli());

	public static String getRandomName() {
		StringBuffer randomName = new StringBuffer(SENTINEL);
		while (randomName.length() < LENGTH) {
            int index = (int) (random.nextFloat() * LETTERS.length());
            randomName.append(LETTERS.charAt(index));
        }
        return randomName.toString();
	}
}
