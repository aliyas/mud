package org.fourchan.d.mud.server;

import java.net.InetSocketAddress;

import org.fourchan.d.mud.server.keys.KeyStoreModule;
import org.fourchan.d.mud.server.keys.KeyStore.CertificatePassword;
import org.fourchan.d.mud.server.keys.KeyStoreModule.ServerKeyStorePassword;
import org.fourchan.d.mud.server.keys.KeyStoreModule.UserKeyStorePassword;
import org.fourchan.d.mud.server.service.ServiceModule;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Scopes;
import com.google.inject.Singleton;

public class ServerModule extends AbstractModule {
	private final StockadeCommandLine commandLine;
	
	ServerModule(StockadeCommandLine commandLine) {
		this.commandLine = commandLine;
	}

	@Override
	protected void configure() {
		bind(InetSocketAddress.class).toInstance(
				new InetSocketAddress(
						this.commandLine.getHost(),
						this.commandLine.getPort()));
		bind(StockadeWebsocketServer.class).in(Scopes.SINGLETON);
		
		install(new ServiceModule());
		install(new KeyStoreModule());
	}

	@Provides
	@Singleton
	StockadeCommandLine provideStockadeCommandLine() {
		return this.commandLine;
	}

	@Provides
	@Singleton
	@ServerKeyStorePassword
	static String provideServerKeyStorePassword(StockadeCommandLine commandLine) {
		return commandLine.getServerKeyStorePassword();
	}

	@Provides
	@Singleton
	@UserKeyStorePassword
	static String provideUserKeyStorePassword(StockadeCommandLine commandLine) {
		return commandLine.getUserKeyStorePassword();
	}

	@Provides
	@Singleton
	@CertificatePassword
	static String provideCertificatePassword(StockadeCommandLine commandLine) {
		return commandLine.getServerPrivateKeyPassword();
	}
}
