package org.fourchan.d.mud.server.keys;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.UnrecoverableKeyException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;

import org.fourchan.d.mud.server.keys.KeyStoreModule.UserKeyStore;
import org.fourchan.d.mud.server.keys.KeyStoreModule.UserKeyStoreFilePath;
import org.fourchan.d.mud.server.keys.KeyStoreModule.UserKeyStorePassword;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.BindingAnnotation;
import com.google.inject.Inject;

public class KeyStore {
	private static final Logger logger = LoggerFactory.getLogger(KeyStore.class);

	@BindingAnnotation
	@Target({ElementType.METHOD, ElementType.PARAMETER})
	@Retention(RetentionPolicy.RUNTIME)
	public @interface ServerKeyStore {}
	
	@BindingAnnotation
	@Target({ElementType.METHOD, ElementType.PARAMETER})
	@Retention(RetentionPolicy.RUNTIME)
	public @interface CertificatePassword {}

	private final java.security.KeyStore serverKeyStore;
	private final java.security.KeyStore userKeyStore;
	private final String userKeyStorePassword;
	private final String userKeyStoreFilePath;
	private final String certificatePassword;

	@Inject
	KeyStore(@ServerKeyStore java.security.KeyStore serverKeyStore,
			@UserKeyStore java.security.KeyStore userKeyStore,
			@UserKeyStorePassword String userKeyStorePassword,
			@UserKeyStoreFilePath String userKeyStoreFilePath,
			@CertificatePassword String certificatePassword) {
		this.serverKeyStore = serverKeyStore;
		this.userKeyStore = userKeyStore;
		this.userKeyStorePassword = userKeyStorePassword;
		this.userKeyStoreFilePath = userKeyStoreFilePath;
		this.certificatePassword = certificatePassword;
	}

	public SSLContext getSSLContextServer() {
		try {
			KeyManagerFactory keyManagerFactory = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
			keyManagerFactory.init(serverKeyStore, certificatePassword.toCharArray());
	
			SSLContext sslContext = SSLContext.getInstance("TLSv1.2");
			KeyManager[] keyManagers = keyManagerFactory.getKeyManagers();
			sslContext.init(keyManagers, null, null);
			return sslContext;
		} catch (KeyStoreException e) {
			logger.error("Keystore issue when obtaining SSL context", e);
		} catch (NoSuchAlgorithmException e) {
			logger.error("Algorithm issue when obtaining SSL context", e);
		} catch (UnrecoverableKeyException e) {
			logger.error("Unrecoverable key issue when obtaining SSL context", e);
		} catch (KeyManagementException e) {
			logger.error("Key management issue when obtaining SSL context", e);
		}
		return null;
	}
	
	public enum AddCertToKeyStoreResult {
		SUCCESSFUL,
		ALREADY_CONTAINS_ALIAS,
		ERROR_OCCURED
	}

	public AddCertToKeyStoreResult addCertToKeyStore(String username, X509Certificate cert) {
		synchronized(userKeyStore) {
			try {
				if (userKeyStore.containsAlias(username)) {
					return AddCertToKeyStoreResult.ALREADY_CONTAINS_ALIAS;
				}
				userKeyStore.setEntry(username, new java.security.KeyStore.TrustedCertificateEntry(cert), null);
				userKeyStore.store(new FileOutputStream(userKeyStoreFilePath), userKeyStorePassword.toCharArray());
				return AddCertToKeyStoreResult.SUCCESSFUL;
			} catch (KeyStoreException e) {
				logger.error("Keystore issue when adding key", e);
			} catch (NoSuchAlgorithmException e) {
				logger.error("Algorithm issue when adding key", e);
			} catch (CertificateException e) {
				logger.error("Certificate issue when adding key", e);
			} catch (FileNotFoundException e) {
				logger.error("File issue when adding key", e);
			} catch (IOException e) {
				logger.error("I/O issue when adding key", e);
			}
			return AddCertToKeyStoreResult.ERROR_OCCURED;
		}
	}

	public PublicKey getKey(String username) {
		try {
			Certificate cert = userKeyStore.getCertificate(username);
			if (cert == null) {
				return null;
			}
			return cert.getPublicKey();
		} catch (KeyStoreException e) {
			logger.error("Keystore issue when getting key", e);
			e.printStackTrace();
		}
		return null;
	}
}
