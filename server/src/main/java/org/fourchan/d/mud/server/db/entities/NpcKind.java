package org.fourchan.d.mud.server.db.entities;

public enum NpcKind {
	CREEP_VINE(1, "Creep Vine");

	private final int id;
	private final String name;

	private NpcKind(int id, String name) {
		this.id = id;
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	private static final NpcKind[] VALUES = values();

	public static NpcKind parseFrom(int id) {
		for (NpcKind npcKind : VALUES) {
			if (npcKind.getId() == id) {
				return npcKind;
			}
		}
		return null;
	}
}
