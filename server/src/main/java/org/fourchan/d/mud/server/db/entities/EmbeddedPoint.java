package org.fourchan.d.mud.server.db.entities;

import java.io.IOException;
import java.io.Serializable;

import javax.persistence.Embeddable;

import org.fourchan.d.mud.common.map.Direction;

@Embeddable
public class EmbeddedPoint implements Serializable {
	/**
	 * Do not touch.
	 */
	private static final long serialVersionUID = -5784076862047505465L;

	public EmbeddedPoint() {}
	public EmbeddedPoint(int x, int y, int z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}

	private int x;
	private int y;
	private int z;

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public int getZ() {
		return z;
	}

	public EmbeddedPoint getNeighbor(Direction d) {
		switch (d) {
		case N:
			return new EmbeddedPoint(this.x, this.y + 1, this.z);
		case E:
			return new EmbeddedPoint(this.x + 1, this.y, this.z);
		case S:
			return new EmbeddedPoint(this.x, this.y - 1, this.z);
		case W:
			return new EmbeddedPoint(this.x - 1, this.y, this.z);
		}
		throw new RuntimeException("Unhandled direction: " + d.toString());
	}

	/**
	 * Do not touch.
	 */
	private void writeObject(java.io.ObjectOutputStream stream) throws IOException {
		stream.defaultWriteObject();
	}

	/**
	 * Do not touch.
	 */
	private void readObject(java.io.ObjectInputStream stream) throws IOException, ClassNotFoundException {
		stream.defaultReadObject();
	}

	@Override
	public String toString() {
		return String.format("(%d,%d,%d)", x, y, z);
	}
}
