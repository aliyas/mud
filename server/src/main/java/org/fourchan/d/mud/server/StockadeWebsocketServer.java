package org.fourchan.d.mud.server;

import java.net.InetSocketAddress;

import org.fourchan.d.mud.server.service.StockadeServiceImpl;
import org.java_websocket.WebSocket;
import org.java_websocket.WebSocketImpl;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

public class StockadeWebsocketServer extends WebSocketServer {
	private static final Logger logger = LoggerFactory.getLogger(StockadeWebsocketServer.class);
	private final StockadeServiceImpl service;
	private final StockadeCommandLine commandLine;
	
	@Inject
	StockadeWebsocketServer(
			InetSocketAddress address,
			StockadeServiceImpl service,
			StockadeCommandLine commandLine) {
		super(address);
		this.service = service;
		this.commandLine = commandLine;
		WebSocketImpl.DEBUG = this.commandLine.isDebugEnabled();
	}

	@Override
	public void onOpen(WebSocket conn, ClientHandshake handshake) {
		logger.trace("A websocket connected");
		service.addUnauthenticatedConnection(conn);
	}

	@Override
	public void onClose(WebSocket conn, int code, String reason, boolean remote) {
		logger.trace("A websocket disconnected: Code {}, Reason {}, Remote {}", code, reason, remote);
		service.removeConnection(conn);
	}

	@Override
	public void onMessage(WebSocket conn, String message) {
		logger.trace("Received: {}", message);
		service.handleMessage(conn, message);
	}

	@Override
	public void onError(WebSocket conn, Exception ex) {
		if (conn != null) {
			logger.error("Websocket error for a connection", ex);
		} else {
			logger.error("Websocket error but null connection", ex);
		}
	}

}
