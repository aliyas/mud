# Authenticating The Server

A file named `keystoreServer` is required at this location in order to
authenticate the server and establish a websocket over TLS. The code expects a
JKS-type store created using `keytool`. The alias must be `server`.

For example, to generate a new keystore with a new private and public key:

`<JDK>\bin\keytool -genkey -validity 365 -keystore "keystoreServer" -storepass "teststorepass" -keypass "testkeypass" -alias "server" -keysize 2048 -keyalg RSA -dname "CN=stockade, OU=stockadeGame"`

This will generate the keystore for the server, but the certificate needs to be
exported for the `stockade-client`. To do so:

`<JDK>\bin\keytool -exportcert -keystore "keystoreServer" -alias "server" -file "server.cer"`

It must be placed as a resource in the stockade-client for gradle to properly
bundle it in an installation.

Using the above example, running the server then requires the following flags
in order to be able to access the keystore and private key:

``