package org.fourchan.d.mud.server.game;

import javax.inject.Inject;

import org.fourchan.d.mud.common.messages.common.Entity;
import org.junit.Before;
import org.junit.Test;

import com.google.inject.Guice;

public class StockadeGameTest {
	@Inject
	StockadeGame game;

	@Before
	public void setUp() {
		Guice.createInjector(new GameModule()).injectMembers(this);
	}

	@Test
	public void loadCharacterAndBody() {
		game.examineEntity(Entity.Type.HUMAN_PLAYER_CHARACTER, 113);
	}
}
