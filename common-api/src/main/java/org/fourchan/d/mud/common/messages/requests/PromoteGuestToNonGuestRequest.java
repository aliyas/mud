package org.fourchan.d.mud.common.messages.requests;

import org.fourchan.d.mud.common.StockadeService;
import org.fourchan.d.mud.common.messages.RequestConstants;
import org.fourchan.d.mud.common.messages.ServerMessage;
import org.java_websocket.WebSocket;

public class PromoteGuestToNonGuestRequest extends ServerMessage {
	private String username;
	private String publicKey;

	public PromoteGuestToNonGuestRequest(
			String username,
			String publicKey) {
		super(RequestConstants.PROMOTE_GUEST_REQUEST_MESSAGE_TYPE);
		this.username = username;
		this.publicKey = publicKey;
	}

	@Override
	public void visit(StockadeService service, WebSocket socket) {
		service.handlePromoteGuestToNonGuestRequest(socket, username, publicKey);
	}
}