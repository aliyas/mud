package org.fourchan.d.mud.common.messages.responses;

import java.util.List;

import org.fourchan.d.mud.common.StockadeClient;
import org.fourchan.d.mud.common.map.Point;
import org.fourchan.d.mud.common.messages.ClientMessage;
import org.fourchan.d.mud.common.messages.ResponseConstants;
import org.fourchan.d.mud.common.messages.common.MapPoint;

public class MapPayload extends ClientMessage {
	private List<MapPoint> points;
	private Point currentLocation;

	public MapPayload(List<MapPoint> points, Point currentLocation) {
		super(ResponseConstants.MAP_PAYLOAD_MESSAGE_TYPE);
		if (points != null && !points.isEmpty()) {
			this.points = points;
		}
		this.currentLocation = currentLocation;
	}

	public List<MapPoint> getPoints() {
		return points;
	}

	public Point getCurrentLocation() {
		return currentLocation;
	}

	@Override
	public void visit(StockadeClient client) {
		client.handleMapPayload(this);
	}
}
