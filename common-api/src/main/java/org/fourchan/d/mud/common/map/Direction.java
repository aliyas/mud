package org.fourchan.d.mud.common.map;

public enum Direction {
	N,
	S,
	E,
	W
}