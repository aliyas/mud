package org.fourchan.d.mud.common.messages.responses;

import java.util.List;

import org.fourchan.d.mud.common.StockadeClient;
import org.fourchan.d.mud.common.messages.ClientMessage;
import org.fourchan.d.mud.common.messages.ResponseConstants;
import org.fourchan.d.mud.common.messages.common.SpawnPoint;

public class SpawnPointResponse extends ClientMessage {
	private String error;
	private boolean successful;
	private List<SpawnPoint> spawns;
	public SpawnPointResponse(String error) {
		super(ResponseConstants.SPAWN_POINT_RESPONSE_MESSAGE_TYPE);
		this.error = error;
		this.successful = false;
	}

	public SpawnPointResponse(List<SpawnPoint> spawns) {
		super(ResponseConstants.SPAWN_POINT_RESPONSE_MESSAGE_TYPE);
		this.successful = true;
		if (spawns != null && !spawns.isEmpty()) {
			this.spawns = spawns;
		}
	}

	public List<SpawnPoint> getSpawns() {
		return spawns;
	}

	public String getError() {
		return error;
	}

	public boolean isSuccessful() {
		return successful;
	}

	@Override
	public void visit(StockadeClient client) {
		client.handleSpawnPointResponse(this);
	}
}
