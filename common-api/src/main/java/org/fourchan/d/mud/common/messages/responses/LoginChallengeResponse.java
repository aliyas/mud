package org.fourchan.d.mud.common.messages.responses;

import org.fourchan.d.mud.common.StockadeClient;
import org.fourchan.d.mud.common.messages.ClientMessage;
import org.fourchan.d.mud.common.messages.ResponseConstants;

public class LoginChallengeResponse extends ClientMessage {
	private String error;
	private boolean successful;
	private String challenge;
	
	public LoginChallengeResponse(String error, Object ignored) {
		super(ResponseConstants.LOGIN_CHALLENGE_RESPONSE_MESSAGE_TYPE);
		this.successful = false;
		this.error = error;
	}
	
	public LoginChallengeResponse(String challenge) {
		super(ResponseConstants.LOGIN_CHALLENGE_RESPONSE_MESSAGE_TYPE);
		this.successful = true;
		this.challenge = challenge;
	}

	public String getError() {
		return error;
	}

	public boolean isSuccessful() {
		return successful;
	}

	public String getChallenge() {
		return challenge;
	}
	
	@Override
	public void visit(StockadeClient client) {
		client.handleLoginChallengeResponse(this);
	}
}
