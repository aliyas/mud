package org.fourchan.d.mud.common.messages.requests;

import org.fourchan.d.mud.common.StockadeService;
import org.fourchan.d.mud.common.messages.RequestConstants;
import org.fourchan.d.mud.common.messages.ServerMessage;
import org.java_websocket.WebSocket;

public class CharacterSelectedRequest extends ServerMessage {
	private String firstName;
	private String lastName;
	
	public CharacterSelectedRequest(String firstName, String lastName) {
		super(RequestConstants.SELECT_CHARACTER_REQUEST_MESSAGE_TYPE);
		this.firstName = firstName;
		this.lastName = lastName;
	}
	
	public String getFirstName() {
		return firstName;
	}
	
	public String getLastName() {
		return lastName;
	}

	@Override
	public void visit(StockadeService service, WebSocket socket) {
		service.handleSelectCharacterRequest(socket, this);
	}
}
