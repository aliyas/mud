package org.fourchan.d.mud.common.messages.requests;

import org.fourchan.d.mud.common.StockadeService;
import org.fourchan.d.mud.common.messages.RequestConstants;
import org.fourchan.d.mud.common.messages.ServerMessage;
import org.java_websocket.WebSocket;

public class RegisterRequest extends ServerMessage {
	private String username;
	private String publicKey;
	
	public RegisterRequest(
			String username,
			String publicKey) {
		super(RequestConstants.REGISTER_REQUEST_MESSAGE_TYPE);
		this.username = username;
		this.publicKey = publicKey;
	}

	@Override
	public void visit(StockadeService service, WebSocket socket) {
		service.handleRegisterRequest(socket, username, publicKey);
	}
}
