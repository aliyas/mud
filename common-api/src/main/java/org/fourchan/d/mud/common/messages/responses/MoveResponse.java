package org.fourchan.d.mud.common.messages.responses;

import org.fourchan.d.mud.common.StockadeClient;
import org.fourchan.d.mud.common.messages.ClientMessage;
import org.fourchan.d.mud.common.messages.ResponseConstants;

public class MoveResponse extends ClientMessage {
	private String error;
	private boolean successful;
	public MoveResponse(String error) {
		super(ResponseConstants.MOVE_RESPONSE_MESSAGE_TYPE);
		this.error = error;
		this.successful = false;
	}

	public MoveResponse() {
		super(ResponseConstants.MOVE_RESPONSE_MESSAGE_TYPE);
		this.successful = true;
	}

	public String getError() {
		return error;
	}

	public boolean isSuccessful() {
		return successful;
	}

	@Override
	public void visit(StockadeClient client) {
		client.handleMoveResponse(this);
	}
}
