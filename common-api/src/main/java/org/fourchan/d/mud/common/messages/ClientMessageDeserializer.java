package org.fourchan.d.mud.common.messages;

import java.lang.reflect.Type;

import org.fourchan.d.mud.common.messages.responses.ActionPayload;
import org.fourchan.d.mud.common.messages.responses.CharacterSelectedResponse;
import org.fourchan.d.mud.common.messages.responses.CreateCharacterResponse;
import org.fourchan.d.mud.common.messages.responses.EntityPayload;
import org.fourchan.d.mud.common.messages.responses.InventoryResponse;
import org.fourchan.d.mud.common.messages.responses.LoginChallengeResponse;
import org.fourchan.d.mud.common.messages.responses.LoginResponse;
import org.fourchan.d.mud.common.messages.responses.LogoutResponse;
import org.fourchan.d.mud.common.messages.responses.MapPayload;
import org.fourchan.d.mud.common.messages.responses.MoveResponse;
import org.fourchan.d.mud.common.messages.responses.NewGuestResponse;
import org.fourchan.d.mud.common.messages.responses.PlayerInfoResponse;
import org.fourchan.d.mud.common.messages.responses.PromoteGuestToNonGuestResponse;
import org.fourchan.d.mud.common.messages.responses.RegisterResponse;
import org.fourchan.d.mud.common.messages.responses.SpawnPointResponse;
import org.fourchan.d.mud.common.messages.responses.TextPayload;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

public class ClientMessageDeserializer implements JsonDeserializer<ClientMessage> {
	@Override
	public ClientMessage deserialize(
			JsonElement jsonElement,
			Type type,
			JsonDeserializationContext context)
			throws JsonParseException {
		JsonObject jsonObject = jsonElement.getAsJsonObject();
		JsonElement jsonType = jsonObject.get("type");
		String stringType = jsonType.getAsString();
		
		ClientMessage clientMessage = null;     
		
		if (stringType.equals(ResponseConstants.REGISTER_RESPONSE_MESSAGE_TYPE)) {
			clientMessage = context.deserialize(jsonElement, RegisterResponse.class);
		} else if (stringType.equals(ResponseConstants.LOGIN_CHALLENGE_RESPONSE_MESSAGE_TYPE)) {
			clientMessage = context.deserialize(jsonElement, LoginChallengeResponse.class);
		} else if (stringType.equals(ResponseConstants.LOGIN_RESPONSE_MESSAGE_TYPE)) {
			clientMessage = context.deserialize(jsonElement, LoginResponse.class);
		} else if (stringType.equals(ResponseConstants.LOGOUT_RESPONSE_MESSAGE_TYPE)) {
			clientMessage = context.deserialize(jsonElement, LogoutResponse.class);
		} else if (stringType.equals(ResponseConstants.PLAYER_INFO_RESPONSE_MESSAGE_TYPE)) {
			clientMessage = context.deserialize(jsonElement, PlayerInfoResponse.class);
		} else if (stringType.equals(ResponseConstants.CREATE_CHARACTER_RESPONSE_MESSAGE_TYPE)) {
			clientMessage = context.deserialize(jsonElement, CreateCharacterResponse.class);
		} else if (stringType.equals(ResponseConstants.SELECT_CHARACTER_RESPONSE_MESSAGE_TYPE)) {
			clientMessage = context.deserialize(jsonElement, CharacterSelectedResponse.class);
		} else if (stringType.equals(ResponseConstants.MAP_PAYLOAD_MESSAGE_TYPE)) {
			clientMessage = context.deserialize(jsonElement, MapPayload.class);
		} else if (stringType.equals(ResponseConstants.MOVE_RESPONSE_MESSAGE_TYPE)) {
			clientMessage = context.deserialize(jsonElement, MoveResponse.class);
		} else if (stringType.equals(ResponseConstants.SPAWN_POINT_RESPONSE_MESSAGE_TYPE)) {
			clientMessage = context.deserialize(jsonElement, SpawnPointResponse.class);
		} else if (stringType.equals(ResponseConstants.ENTITY_PAYLOAD_MESSAGE_TYPE)) {
			clientMessage = context.deserialize(jsonElement, EntityPayload.class);
		} else if (stringType.equals(ResponseConstants.ACTION_PAYLOAD_MESSAGE_TYPE)) {
			clientMessage = context.deserialize(jsonElement, ActionPayload.class);
		} else if (stringType.equals(ResponseConstants.TEXT_PAYLOAD_MESSAGE_TYPE)) {
			clientMessage = context.deserialize(jsonElement, TextPayload.class);
		} else if (stringType.equals(ResponseConstants.INVENTORY_RESPONSE_MESSAGE_TYPE)) {
			clientMessage = context.deserialize(jsonElement, InventoryResponse.class);
		} else if (stringType.equals(ResponseConstants.NEW_GUEST_RESPONSE_MESSAGE_TYPE)) {
			clientMessage = context.deserialize(jsonElement, NewGuestResponse.class);
		} else if (stringType.equals(ResponseConstants.PROMOTE_GUEST_RESPONSE_MESSAGE_TYPE)) {
			clientMessage = context.deserialize(jsonElement, PromoteGuestToNonGuestResponse.class);
		} else {
			throw new JsonParseException("Unknown client message type: " + stringType);
		}
		
		return clientMessage;
	}
}
