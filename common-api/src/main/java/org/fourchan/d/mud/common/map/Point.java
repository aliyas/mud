package org.fourchan.d.mud.common.map;

public final class Point {
	private int x;
	private int y;
	private int z;

	public Point(int x, int y, int z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public int getZ() {
		return z;
	}

	@Override
	public boolean equals(Object o) {
		if (!(o instanceof Point)) {
			return false;
		}
		Point p = (Point) o;
		if (this == p) {
			return true;
		}
		return this.x == p.getX()
				&& this.y == p.getY()
				&& this.z == p.getZ();
	}

	@Override
	public int hashCode() {
		int result = 17;
		result = 31 * result ^ x;
		result = 31 * result ^ y;
		result = 31 * result ^ z;
		return result;
	}

	@Override
	public String toString() {
		return String.format("(%d,%d,%d)", x, y, z);
	}
}
