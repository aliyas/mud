package org.fourchan.d.mud.common.messages.responses;

import java.util.List;

import org.fourchan.d.mud.common.StockadeClient;
import org.fourchan.d.mud.common.messages.ClientMessage;
import org.fourchan.d.mud.common.messages.ResponseConstants;
import org.fourchan.d.mud.common.messages.common.Item;

public class InventoryResponse extends ClientMessage {
	private List<Item> items;

	public InventoryResponse(List<Item> items) {
		super(ResponseConstants.INVENTORY_RESPONSE_MESSAGE_TYPE);
		if (items != null && !items.isEmpty()) {
			this.items = items;
		}
	}

	public List<Item> getItems() {
		return items;
	}

	public void visit(StockadeClient client) {
		client.handleInventoryResponse(this);
	}
}
