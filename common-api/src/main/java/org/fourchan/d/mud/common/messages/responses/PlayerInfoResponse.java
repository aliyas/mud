package org.fourchan.d.mud.common.messages.responses;

import org.fourchan.d.mud.common.StockadeClient;
import org.fourchan.d.mud.common.messages.ClientMessage;
import org.fourchan.d.mud.common.messages.ResponseConstants;
import org.fourchan.d.mud.common.messages.common.Player;

public class PlayerInfoResponse extends ClientMessage {
	private String error;
	private boolean successful;
	private Player player;
	
	public PlayerInfoResponse(String error) {
		super(ResponseConstants.PLAYER_INFO_RESPONSE_MESSAGE_TYPE);
		this.successful = false;
		this.error = error;
	}
	
	public PlayerInfoResponse(Player player) {
		super(ResponseConstants.PLAYER_INFO_RESPONSE_MESSAGE_TYPE);
		this.successful = true;
		this.player = player;
	}

	public String getError() {
		return error;
	}

	public boolean isSuccessful() {
		return successful;
	}

	public Player getPlayer() {
		return player;
	}

	@Override
	public void visit(StockadeClient client) {
		client.handlePlayerInfoResponse(this);
	}
}
