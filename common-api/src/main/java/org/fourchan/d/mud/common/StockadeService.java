package org.fourchan.d.mud.common;

import org.fourchan.d.mud.common.messages.requests.CharacterSelectedRequest;
import org.fourchan.d.mud.common.messages.requests.CreateCharacterRequest;
import org.fourchan.d.mud.common.messages.requests.ExamineEntityRequest;
import org.fourchan.d.mud.common.messages.requests.InventoryRequest;
import org.fourchan.d.mud.common.messages.requests.MoveRequest;
import org.fourchan.d.mud.common.messages.requests.PlayerInfoRequest;
import org.fourchan.d.mud.common.messages.requests.privileged.MapUpdateRequest;
import org.java_websocket.WebSocket;

public interface StockadeService {
	void handleRegisterRequest(WebSocket socket, String username, String publicKey);
	void handleLoginChallengeRequest(WebSocket socket);
	void handleLoginRequest(WebSocket socket, String username, String answer);
	void handleLogoutRequest(WebSocket socket);
	void handlePlayerInfoRequest(WebSocket socket, PlayerInfoRequest request);
	void handleCreateCharacterRequest(WebSocket socket, CreateCharacterRequest request);
	void handleSelectCharacterRequest(WebSocket socket, CharacterSelectedRequest request);
	void handleMoveRequest(WebSocket socket, MoveRequest request);
	void handleSpawnPointRequest(WebSocket socket);
	void handleMapUpdateRequest(WebSocket socket, MapUpdateRequest request);
	void handleExamineEntityRequest(WebSocket socket, ExamineEntityRequest request);
	void handleInventoryRequest(WebSocket socket, InventoryRequest request);
	void handleNewGuestRequest(WebSocket socket);
	void handlePromoteGuestToNonGuestRequest(WebSocket socket, String username, String publicKey);
}
