package org.fourchan.d.mud.common.messages.responses;

import org.fourchan.d.mud.common.StockadeClient;
import org.fourchan.d.mud.common.messages.ClientMessage;
import org.fourchan.d.mud.common.messages.ResponseConstants;
import org.fourchan.d.mud.common.messages.common.Player;

public class NewGuestResponse extends ClientMessage {
	private String error;
	private boolean successful;
	private Player player;

	public NewGuestResponse(Player player) {
		super(ResponseConstants.NEW_GUEST_RESPONSE_MESSAGE_TYPE);
		this.successful = true;
		this.player = player;
	}

	public NewGuestResponse(String error) {
		super(ResponseConstants.NEW_GUEST_RESPONSE_MESSAGE_TYPE);
		this.error = error;
		this.successful = false;
	}

	public String getError() {
		return error;
	}

	public boolean isSuccessful() {
		return successful;
	}

	public Player getPlayer() {
		return player;
	}

	@Override
	public void visit(StockadeClient client) {
		client.handleNewGuestResponse(this);
	}
}
