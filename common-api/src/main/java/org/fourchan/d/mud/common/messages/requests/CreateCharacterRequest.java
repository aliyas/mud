package org.fourchan.d.mud.common.messages.requests;

import org.fourchan.d.mud.common.StockadeService;
import org.fourchan.d.mud.common.messages.RequestConstants;
import org.fourchan.d.mud.common.messages.ServerMessage;
import org.java_websocket.WebSocket;

public class CreateCharacterRequest extends ServerMessage {
	private String firstName;
	private String lastName;
	private String sexChoice;
	private String spawnChoice;
	public CreateCharacterRequest() {
		super(RequestConstants.CREATE_CHARACTER_REQUEST_MESSAGE_TYPE);
	}
	
	public CreateCharacterRequest(
			String firstName,
			String lastName,
			String sexChoice,
			String spawnChoice) {
		super(RequestConstants.CREATE_CHARACTER_REQUEST_MESSAGE_TYPE);
		this.firstName = firstName;
		this.lastName = lastName;
		this.sexChoice = sexChoice;
		this.spawnChoice = spawnChoice;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public String getSexChoice() {
		return sexChoice;
	}

	public String getSpawnChoice() {
		return spawnChoice;
	}

	@Override
	public void visit(StockadeService service, WebSocket socket) {
		service.handleCreateCharacterRequest(socket, this);
	}
}
