package org.fourchan.d.mud.common.messages.responses;

import java.util.List;

import org.fourchan.d.mud.common.StockadeClient;
import org.fourchan.d.mud.common.messages.ClientMessage;
import org.fourchan.d.mud.common.messages.ResponseConstants;
import org.fourchan.d.mud.common.messages.common.Entity;

public class EntityPayload extends ClientMessage {
	private List<Entity> entities;

	public EntityPayload(List<Entity> entities) {
		super(ResponseConstants.ENTITY_PAYLOAD_MESSAGE_TYPE);
		if (entities != null && !entities.isEmpty()) {
			this.entities = entities;
		}
	}

	public List<Entity> getEntities() {
		return entities;
	}

	@Override
	public void visit(StockadeClient client) {
		client.handleEntityPayload(this);
	}
}
