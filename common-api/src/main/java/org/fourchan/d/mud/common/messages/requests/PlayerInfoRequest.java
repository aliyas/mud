package org.fourchan.d.mud.common.messages.requests;

import org.fourchan.d.mud.common.StockadeService;
import org.fourchan.d.mud.common.messages.RequestConstants;
import org.fourchan.d.mud.common.messages.ServerMessage;
import org.java_websocket.WebSocket;

public class PlayerInfoRequest extends ServerMessage {
	private String username;

	public PlayerInfoRequest(String error) {
		super(RequestConstants.PLAYER_INFO_REQUEST_MESSAGE_TYPE);
	}

	public String getUsername() {
		return username;
	}

	@Override
	public void visit(StockadeService service, WebSocket socket) {
		service.handlePlayerInfoRequest(socket, this);
	}
}
