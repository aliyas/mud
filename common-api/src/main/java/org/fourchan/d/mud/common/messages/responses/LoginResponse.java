package org.fourchan.d.mud.common.messages.responses;

import org.fourchan.d.mud.common.StockadeClient;
import org.fourchan.d.mud.common.messages.ClientMessage;
import org.fourchan.d.mud.common.messages.ResponseConstants;
import org.fourchan.d.mud.common.messages.common.Player;

public class LoginResponse extends ClientMessage {
	private String error;
	private boolean successful;
	private Player player;

	public LoginResponse(String error) {
		super(ResponseConstants.LOGIN_RESPONSE_MESSAGE_TYPE);
		this.successful = false;
		this.error = error;
	}
	
	public LoginResponse(Player player) {
		super(ResponseConstants.LOGIN_RESPONSE_MESSAGE_TYPE);
		this.successful = true;
		this.player = player;
	}

	public String getError() {
		return error;
	}

	public boolean isSuccessful() {
		return successful;
	}

	public Player getPlayer() {
		return player;
	}
	
	@Override
	public void visit(StockadeClient client) {
		client.handleLoginResponse(this);
	}
}
