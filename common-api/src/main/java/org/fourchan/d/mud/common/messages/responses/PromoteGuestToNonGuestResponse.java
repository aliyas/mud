package org.fourchan.d.mud.common.messages.responses;

import org.fourchan.d.mud.common.StockadeClient;
import org.fourchan.d.mud.common.messages.ClientMessage;
import org.fourchan.d.mud.common.messages.ResponseConstants;
import org.fourchan.d.mud.common.messages.common.Player;

public class PromoteGuestToNonGuestResponse extends ClientMessage {
	private String error;
	private boolean successful;
	private Player player;

	public PromoteGuestToNonGuestResponse(Player player) {
		super(ResponseConstants.PROMOTE_GUEST_RESPONSE_MESSAGE_TYPE);
		this.successful = true;
		this.player = player;
	}

	public PromoteGuestToNonGuestResponse(String error) {
		super(ResponseConstants.PROMOTE_GUEST_RESPONSE_MESSAGE_TYPE);
		this.error = error;
		this.successful = false;
	}

	public String getError() {
		return error;
	}

	public boolean isSuccessful() {
		return successful;
	}

	public Player getPlayer() {
		return player;
	}

	@Override
	public void visit(StockadeClient client) {
		client.handlePromoteGuestToNonGuestResponse(this);
	}
}