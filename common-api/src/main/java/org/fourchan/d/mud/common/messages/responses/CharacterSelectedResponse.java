package org.fourchan.d.mud.common.messages.responses;

import org.fourchan.d.mud.common.StockadeClient;
import org.fourchan.d.mud.common.messages.ClientMessage;
import org.fourchan.d.mud.common.messages.ResponseConstants;
import org.fourchan.d.mud.common.messages.common.Character;

public class CharacterSelectedResponse extends ClientMessage {
	private String error;
	private boolean successful;
	private Character character;
	public CharacterSelectedResponse(String error) {
		super(ResponseConstants.SELECT_CHARACTER_RESPONSE_MESSAGE_TYPE);
		this.error = error;
		this.successful = false;
	}

	public CharacterSelectedResponse(Character character) {
		super(ResponseConstants.SELECT_CHARACTER_RESPONSE_MESSAGE_TYPE);
		this.successful = true;
		this.character = character;
	}

	public String getError() {
		return error;
	}

	public boolean isSuccessful() {
		return successful;
	}

	public Character getCharacter() {
		return character;
	}

	@Override
	public void visit(StockadeClient client) {
		client.handleCharacterSelectedResponse(this);
	}
}
