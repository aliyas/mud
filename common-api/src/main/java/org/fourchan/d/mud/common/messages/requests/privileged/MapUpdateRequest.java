package org.fourchan.d.mud.common.messages.requests.privileged;

import org.fourchan.d.mud.common.StockadeService;
import org.fourchan.d.mud.common.map.MapPointId;
import org.fourchan.d.mud.common.map.Point;
import org.fourchan.d.mud.common.messages.RequestConstants;
import org.fourchan.d.mud.common.messages.ServerMessage;
import org.java_websocket.WebSocket;

public class MapUpdateRequest extends ServerMessage {
	private Point point;
	private MapPointId pointId;

	public MapUpdateRequest(Point point, MapPointId pointId) {
		super(RequestConstants.MAP_UPDATE_REQUEST_MESSAGE_TYPE);
		this.point = point;
		this.pointId = pointId;
	}

	public Point getPoint() {
		return point;
	}

	public MapPointId getMapPointId() {
		return pointId;
	}

	@Override
	public void visit(StockadeService service, WebSocket socket) {
		service.handleMapUpdateRequest(socket, this);
	}
}
