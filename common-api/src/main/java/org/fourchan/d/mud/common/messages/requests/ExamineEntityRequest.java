package org.fourchan.d.mud.common.messages.requests;

import org.fourchan.d.mud.common.StockadeService;
import org.fourchan.d.mud.common.messages.ActionMessage;
import org.fourchan.d.mud.common.messages.RequestConstants;
import org.fourchan.d.mud.common.messages.ServerMessage;
import org.fourchan.d.mud.common.messages.common.Entity;
import org.java_websocket.WebSocket;

public class ExamineEntityRequest extends ServerMessage implements ActionMessage {
	private Entity.Type targetEntityType;
	private int targetEntityId;

	public ExamineEntityRequest(
			Entity.Type targetEntityType,
			int targetEntityId) {
		super(RequestConstants.EXAMINE_ENTITY_REQUEST_MESSAGE_TYPE);
		this.targetEntityType = targetEntityType;
		this.targetEntityId = targetEntityId;
	}

	public Entity.Type getTargetEntityType() {
		return targetEntityType;
	}

	public int getTargetEntityId() {
		return targetEntityId;
	}

	@Override
	public void visit(StockadeService service, WebSocket socket) {
		service.handleExamineEntityRequest(socket, this);
	}
}
