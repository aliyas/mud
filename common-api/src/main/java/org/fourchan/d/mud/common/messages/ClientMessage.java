package org.fourchan.d.mud.common.messages;

import org.fourchan.d.mud.common.StockadeClient;

/**
 * A message to be received by the client, and sent by the server.
 */
public class ClientMessage {
	/** Used in {@link ClientMessageDeserializer} */
	@SuppressWarnings("unused")
	private final String type;
	
	public ClientMessage(String type) {
		this.type = type;
	}

	/** Override to apply to services */
	public void visit(StockadeClient client) {
		throw new RuntimeException("Override ClientMessage.visit to successfully route this message.");
	}
}
