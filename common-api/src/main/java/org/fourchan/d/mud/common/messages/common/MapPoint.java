package org.fourchan.d.mud.common.messages.common;

import org.fourchan.d.mud.common.map.MapPointId;
import org.fourchan.d.mud.common.map.Point;

public class MapPoint {
	public MapPointId pointId;
	public Point point;
}
