package org.fourchan.d.mud.common.messages.requests;

import org.fourchan.d.mud.common.StockadeService;
import org.fourchan.d.mud.common.messages.RequestConstants;
import org.fourchan.d.mud.common.messages.ServerMessage;
import org.java_websocket.WebSocket;

public class LoginRequest extends ServerMessage {
	private String username;
	private String answer;
	
	public LoginRequest(String username, String answer) {
		super(RequestConstants.LOGIN_REQUEST_MESSAGE_TYPE);
		this.username = username;
		this.answer = answer;
	}

	@Override
	public void visit(StockadeService service, WebSocket socket) {
		service.handleLoginRequest(socket, username, answer);
	}
}
