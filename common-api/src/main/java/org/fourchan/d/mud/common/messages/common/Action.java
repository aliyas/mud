package org.fourchan.d.mud.common.messages.common;

public class Action {
	public enum Type {
		EXAMINE
	}

	public Type actionType;
	public Entity.Type targetEntityType;
	public int targetEntityId;
	public String title;
	public String description;
}
