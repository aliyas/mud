package org.fourchan.d.mud.common.messages.responses;

import org.fourchan.d.mud.common.StockadeClient;
import org.fourchan.d.mud.common.messages.ClientMessage;
import org.fourchan.d.mud.common.messages.ResponseConstants;

public class CreateCharacterResponse extends ClientMessage {
	private String error;
	private boolean successful;
	public CreateCharacterResponse(String error) {
		super(ResponseConstants.CREATE_CHARACTER_RESPONSE_MESSAGE_TYPE);
		this.error = error;
		this.successful = false;
	}

	public CreateCharacterResponse() {
		super(ResponseConstants.CREATE_CHARACTER_RESPONSE_MESSAGE_TYPE);
		this.successful = true;
	}

	public String getError() {
		return error;
	}

	public boolean isSuccessful() {
		return successful;
	}

	@Override
	public void visit(StockadeClient client) {
		client.handleCreateCharacterResponse(this);
	}
}
