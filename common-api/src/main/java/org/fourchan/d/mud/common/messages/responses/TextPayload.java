package org.fourchan.d.mud.common.messages.responses;

import org.fourchan.d.mud.common.StockadeClient;
import org.fourchan.d.mud.common.messages.ClientMessage;
import org.fourchan.d.mud.common.messages.ResponseConstants;

public class TextPayload extends ClientMessage {
	public enum Kind {
		EXAMINE_TEXT
	}

	private String text;
	private Kind kind;

	public TextPayload(String text, Kind kind) {
		super(ResponseConstants.TEXT_PAYLOAD_MESSAGE_TYPE);
		this.text = text;
		this.kind = kind;
	}

	public String getText() {
		return text;
	}

	public Kind getKind() {
		return kind;
	}

	public void visit(StockadeClient client) {
		client.handleTextPayload(this);
	}
}
