package org.fourchan.d.mud.common.messages.requests;

import org.fourchan.d.mud.common.StockadeService;
import org.fourchan.d.mud.common.messages.RequestConstants;
import org.fourchan.d.mud.common.messages.ServerMessage;
import org.java_websocket.WebSocket;

public class LoginChallengeRequest extends ServerMessage {
	public LoginChallengeRequest() {
		super(RequestConstants.LOGIN_CHALLENGE_REQUEST_MESSAGE_TYPE);
	}

	@Override
	public void visit(StockadeService service, WebSocket socket) {
		service.handleLoginChallengeRequest(socket);
	}
}
