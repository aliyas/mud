package org.fourchan.d.mud.common.messages.requests;

import org.fourchan.d.mud.common.StockadeService;
import org.fourchan.d.mud.common.messages.RequestConstants;
import org.fourchan.d.mud.common.messages.ServerMessage;
import org.java_websocket.WebSocket;

public class SpawnPointRequest extends ServerMessage {
	public SpawnPointRequest() {
		super(RequestConstants.SPAWN_POINT_REQUEST_MESSAGE_TYPE);
	}

	@Override
	public void visit(StockadeService service, WebSocket socket) {
		service.handleSpawnPointRequest(socket);
	}
}
