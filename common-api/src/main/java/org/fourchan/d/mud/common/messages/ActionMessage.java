package org.fourchan.d.mud.common.messages;

/**
 * Denotes that a message is an Action Message.
 */
public interface ActionMessage {}
