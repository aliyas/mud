package org.fourchan.d.mud.common.messages;

public final class ResponseConstants {
	private ResponseConstants() {}

	public static final String REGISTER_RESPONSE_MESSAGE_TYPE = "registerResponse";
	public static final String LOGIN_CHALLENGE_RESPONSE_MESSAGE_TYPE = "loginChallengeResponse";
	
	public static final String LOGIN_RESPONSE_MESSAGE_TYPE = "loginResponse";
	public static final String LOGOUT_RESPONSE_MESSAGE_TYPE = "logoutResponse";

	public static final String PLAYER_INFO_RESPONSE_MESSAGE_TYPE = "playerInfoResponse";
	public static final String CREATE_CHARACTER_RESPONSE_MESSAGE_TYPE = "createCharacterResponse";
	public static final String SELECT_CHARACTER_RESPONSE_MESSAGE_TYPE = "selectCharacterResponse";
	public static final String MAP_PAYLOAD_MESSAGE_TYPE = "mapPayload";
	public static final String MOVE_RESPONSE_MESSAGE_TYPE = "moveResponse";
	public static final String SPAWN_POINT_RESPONSE_MESSAGE_TYPE = "spawnPointResponse";
	public static final String ENTITY_PAYLOAD_MESSAGE_TYPE = "entityPayload";
	public static final String ACTION_PAYLOAD_MESSAGE_TYPE = "actionPayload";
	public static final String TEXT_PAYLOAD_MESSAGE_TYPE = "textPayload";
	public static final String INVENTORY_RESPONSE_MESSAGE_TYPE = "inventoryResponse";
	public static final String NEW_GUEST_RESPONSE_MESSAGE_TYPE = "newGuestResponse";
	public static final String PROMOTE_GUEST_RESPONSE_MESSAGE_TYPE = "promoteGuestResponse";
}
