package org.fourchan.d.mud.common.messages;

public final class RequestConstants {
	private RequestConstants() {}

	public static final String REGISTER_REQUEST_MESSAGE_TYPE = "registerRequest";
	public static final String LOGIN_CHALLENGE_REQUEST_MESSAGE_TYPE = "loginChallengeRequest";
	
	public static final String LOGIN_REQUEST_MESSAGE_TYPE = "loginRequest";
	public static final String LOGOUT_REQUEST_MESSAGE_TYPE = "logoutRequest";

	public static final String PLAYER_INFO_REQUEST_MESSAGE_TYPE = "playerInfoRequest";
	public static final String CREATE_CHARACTER_REQUEST_MESSAGE_TYPE = "createCharacterRequest";
	public static final String SELECT_CHARACTER_REQUEST_MESSAGE_TYPE = "selectCharacterRequest";
	public static final String MOVE_REQUEST_MESSAGE_TYPE = "moveRequest";
	public static final String SPAWN_POINT_REQUEST_MESSAGE_TYPE = "spawnRequest";
	public static final String MAP_UPDATE_REQUEST_MESSAGE_TYPE = "mapUpdateRequest";
	public static final String EXAMINE_ENTITY_REQUEST_MESSAGE_TYPE = "examineEntityRequest";
	public static final String INVENTORY_REQUEST_MESSAGE_TYPE = "inventoryRequest";
	public static final String NEW_GUEST_REQUEST_MESSAGE_TYPE = "newGuestRequest";
	public static final String PROMOTE_GUEST_REQUEST_MESSAGE_TYPE = "promoteGuestRequest";
}
