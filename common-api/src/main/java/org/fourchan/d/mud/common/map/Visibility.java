package org.fourchan.d.mud.common.map;

public final class Visibility {
	private Visibility() {}

	public static final int DEFAULT_XY_DISTANCE = 7;
	public static final int DEFAULT_Z_DISTANCE = 0;
}
