package org.fourchan.d.mud.common.messages.responses;

import org.fourchan.d.mud.common.StockadeClient;
import org.fourchan.d.mud.common.messages.ClientMessage;
import org.fourchan.d.mud.common.messages.ResponseConstants;

public class LogoutResponse extends ClientMessage {
	private String error;
	private boolean successful;
	public LogoutResponse() {
		super(ResponseConstants.LOGOUT_RESPONSE_MESSAGE_TYPE);
		this.successful = true;
	}
	
	public LogoutResponse(String error) {
		super(ResponseConstants.LOGOUT_RESPONSE_MESSAGE_TYPE);
		this.successful = false;
		this.error = error;
	}

	public String getError() {
		return error;
	}

	public boolean isSuccessful() {
		return successful;
	}

	@Override
	public void visit(StockadeClient client) {
		client.handleLogoutResponse(this);
	}
}
