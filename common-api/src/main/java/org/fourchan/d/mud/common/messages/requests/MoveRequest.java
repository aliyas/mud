package org.fourchan.d.mud.common.messages.requests;

import org.fourchan.d.mud.common.StockadeService;
import org.fourchan.d.mud.common.map.Direction;
import org.fourchan.d.mud.common.messages.RequestConstants;
import org.fourchan.d.mud.common.messages.ServerMessage;
import org.java_websocket.WebSocket;

public class MoveRequest extends ServerMessage {
	private Direction direction;
	
	public MoveRequest(Direction direction) {
		super(RequestConstants.MOVE_REQUEST_MESSAGE_TYPE);
		this.direction = direction;
	}
	
	public Direction getDirection() {
		return direction;
	}

	@Override
	public void visit(StockadeService service, WebSocket socket) {
		service.handleMoveRequest(socket, this);
	}
}
