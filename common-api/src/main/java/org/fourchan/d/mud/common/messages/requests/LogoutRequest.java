package org.fourchan.d.mud.common.messages.requests;

import org.fourchan.d.mud.common.StockadeService;
import org.fourchan.d.mud.common.messages.RequestConstants;
import org.fourchan.d.mud.common.messages.ServerMessage;
import org.java_websocket.WebSocket;

public class LogoutRequest extends ServerMessage {
	public LogoutRequest() {
		super(RequestConstants.LOGOUT_REQUEST_MESSAGE_TYPE);
	}

	@Override
	public void visit(StockadeService service, WebSocket socket) {
		service.handleLogoutRequest(socket);
	}
}
