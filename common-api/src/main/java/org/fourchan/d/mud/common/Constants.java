package org.fourchan.d.mud.common;

import com.google.common.collect.ImmutableList;

public final class Constants {
	private Constants() {}

	public static final String USERNAME_REGEX = "[a-zA-Z]*"; // TODO: also put in client for registering
	public static final int MAX_LENGTH_USER_NAME = 64; // TODO: use in server and client for registering and logging in
	
	public static final int MAX_LENGTH_CHARACTER_FIRST_NAME = 64;
	public static final int MAX_LENGTH_CHARACTER_LAST_NAME = 64;
	public static final String CHARACTER_NAME_REGEX = "[a-zA-Z]*";
	public static final String FEMALE_HUMAN_STARTING_SEX = "Female Human";
	public static final String MALE_HUMAN_STARTING_SEX = "Male Human";
	public static final ImmutableList<String> ALL_STARTING_SEXES =
			ImmutableList.of(FEMALE_HUMAN_STARTING_SEX, MALE_HUMAN_STARTING_SEX);
}
