package org.fourchan.d.mud.common.messages.responses;

import org.fourchan.d.mud.common.StockadeClient;
import org.fourchan.d.mud.common.messages.ClientMessage;
import org.fourchan.d.mud.common.messages.ResponseConstants;

public class RegisterResponse extends ClientMessage {
	private String error;
	private boolean successful;

	public RegisterResponse() {
		super(ResponseConstants.REGISTER_RESPONSE_MESSAGE_TYPE);
		this.successful = true;
	}
	
	public RegisterResponse(String error) {
		super(ResponseConstants.REGISTER_RESPONSE_MESSAGE_TYPE);
		this.error = error;
		this.successful = false;
	}

	public String getError() {
		return error;
	}

	public boolean isSuccessful() {
		return successful;
	}
	
	@Override
	public void visit(StockadeClient client) {
		client.handleRegisterResponse(this);
	}
}
