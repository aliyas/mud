package org.fourchan.d.mud.common.messages.common.sex;

/**
 * Length or equivalent depth.
 *
 * <p>Why not use a number, such as inches? Because its unfair to content
 * producers to try to determine what sizes go together if everything is some
 * form of int/double whose units need to be kept in mind. It's much clearer to
 * simply reference the non-numeric forms below and do simple comparisons. The
 * numerical versions allow the game to allow finer-grained stepwise
 * transitions. This also allows bounding of values, as what is supposed to
 * happen if a player grinds a value to INT_MAX?
 *
 * <p>Nip all the bullshit of numbers in the bud, there's 200 enums here to
 * represent a wide range of game states. Content producers can use up to
 * twenty states if they so care:
 *
 * <ul>
 * <li>EXTREMELY_STUBBY
 * <li>VERY_STUBBY
 * <li>STUBBY
 * <li>EXTREMELY_SHORT
 * <li>VERY_SHORT
 * <li>SHORT
 * <li>SLIGHTLY_SHORT
 * <li>VERY_SLIGHTLY_SHORT
 * <li>AVERAGE
 * <li>VERY_SLIGHTLY_ABOVE_AVERAGE
 * <li>SLIGHTLY_ABOVE_AVERAGE
 * <li>ABOVE_AVERAGE
 * <li>VERY_SLIGHTLY_LONG
 * <li>SLIGHTLY_LONG
 * <li>LONG
 * <li>VERY_LONG
 * <li>EXTREMELY_LONG
 * <li>HUGELY_LENGTHY
 * <li>VERY_HUGELY_LENGTHY
 * <li>EXTREMELY_HUGELY_LENGTHY
 * </ul>
 *
 * <p>If they don't want such fine-grained detail in their content, they can
 * use the following five general categories, provided as helper functions:
 *
 * <ul>
 * <li>{@link #isStubby() Stubby}
 * <li>{@link #isShort() Short}
 * <li>{@link #isAverage() Average (includes above-average)}
 * <li>{@link #isLong() Long}
 * <li>{@link #isHugelyLengthy() Hugely Lengthy}
 * </ul>
 *
 * <p>This will keep progress for characters consistent as content creators
 * won't gate content by "10" in one instance for a "you have a huge [thing]
 * scene" and "20" in another to gate the "huge [thing] scene".
 */
public enum LengthDepth {
	// Must keep in order of increasing value for longer/shorter.
	EXTREMELY_STUBBY(0),
	EXTREMELY_STUBBY_1(100),
	EXTREMELY_STUBBY_2(200),
	EXTREMELY_STUBBY_3(300),
	EXTREMELY_STUBBY_4(400),
	EXTREMELY_STUBBY_5(500),
	EXTREMELY_STUBBY_6(600),
	EXTREMELY_STUBBY_7(700),
	EXTREMELY_STUBBY_8(800),
	EXTREMELY_STUBBY_9(900),
	VERY_STUBBY(1000),
	VERY_STUBBY_1(1100),
	VERY_STUBBY_2(1200),
	VERY_STUBBY_3(1300),
	VERY_STUBBY_4(1400),
	VERY_STUBBY_5(1500),
	VERY_STUBBY_6(1600),
	VERY_STUBBY_7(1700),
	VERY_STUBBY_8(1800),
	VERY_STUBBY_9(1900),
	STUBBY(2000),
	STUBBY_1(2100),
	STUBBY_2(2200),
	STUBBY_3(2300),
	STUBBY_4(2400),
	STUBBY_5(2500),
	STUBBY_6(2600),
	STUBBY_7(2700),
	STUBBY_8(2800),
	STUBBY_9(2900),
	EXTREMELY_SHORT(3000),
	EXTREMELY_SHORT_1(3100),
	EXTREMELY_SHORT_2(3200),
	EXTREMELY_SHORT_3(3300),
	EXTREMELY_SHORT_4(3400),
	EXTREMELY_SHORT_5(3500),
	EXTREMELY_SHORT_6(3600),
	EXTREMELY_SHORT_7(3700),
	EXTREMELY_SHORT_8(3800),
	EXTREMELY_SHORT_9(3900),
	VERY_SHORT(4000),
	VERY_SHORT_1(4100),
	VERY_SHORT_2(4200),
	VERY_SHORT_3(4300),
	VERY_SHORT_4(4400),
	VERY_SHORT_5(4500),
	VERY_SHORT_6(4600),
	VERY_SHORT_7(4700),
	VERY_SHORT_8(4800),
	VERY_SHORT_9(4900),
	SHORT(5000),
	SHORT_1(5100),
	SHORT_2(5200),
	SHORT_3(5300),
	SHORT_4(5400),
	SHORT_5(5500),
	SHORT_6(5600),
	SHORT_7(5700),
	SHORT_8(5800),
	SHORT_9(5900),
	SLIGHTLY_SHORT(6000),
	SLIGHTLY_SHORT_1(6100),
	SLIGHTLY_SHORT_2(6200),
	SLIGHTLY_SHORT_3(6300),
	SLIGHTLY_SHORT_4(6400),
	SLIGHTLY_SHORT_5(6500),
	SLIGHTLY_SHORT_6(6600),
	SLIGHTLY_SHORT_7(6700),
	SLIGHTLY_SHORT_8(6800),
	SLIGHTLY_SHORT_9(6900),
	VERY_SLIGHTLY_SHORT(7000),
	VERY_SLIGHTLY_SHORT_1(7100),
	VERY_SLIGHTLY_SHORT_2(7200),
	VERY_SLIGHTLY_SHORT_3(7300),
	VERY_SLIGHTLY_SHORT_4(7400),
	VERY_SLIGHTLY_SHORT_5(7500),
	VERY_SLIGHTLY_SHORT_6(7600),
	VERY_SLIGHTLY_SHORT_7(7700),
	VERY_SLIGHTLY_SHORT_8(7800),
	VERY_SLIGHTLY_SHORT_9(7900),
	AVERAGE(8000),
	AVERAGE_1(8100),
	AVERAGE_2(8200),
	AVERAGE_3(8300),
	AVERAGE_4(8400),
	AVERAGE_5(8500),
	AVERAGE_6(8600),
	AVERAGE_7(8700),
	AVERAGE_8(8800),
	AVERAGE_9(8900),
	VERY_SLIGHTLY_ABOVE_AVERAGE(9000),
	VERY_SLIGHTLY_ABOVE_AVERAGE_1(9100),
	VERY_SLIGHTLY_ABOVE_AVERAGE_2(9200),
	VERY_SLIGHTLY_ABOVE_AVERAGE_3(9300),
	VERY_SLIGHTLY_ABOVE_AVERAGE_4(9400),
	VERY_SLIGHTLY_ABOVE_AVERAGE_5(9500),
	VERY_SLIGHTLY_ABOVE_AVERAGE_6(9600),
	VERY_SLIGHTLY_ABOVE_AVERAGE_7(9700),
	VERY_SLIGHTLY_ABOVE_AVERAGE_8(9800),
	VERY_SLIGHTLY_ABOVE_AVERAGE_9(9900),
	SLIGHTLY_ABOVE_AVERAGE(10000),
	SLIGHTLY_ABOVE_AVERAGE_1(10100),
	SLIGHTLY_ABOVE_AVERAGE_2(10200),
	SLIGHTLY_ABOVE_AVERAGE_3(10300),
	SLIGHTLY_ABOVE_AVERAGE_4(10400),
	SLIGHTLY_ABOVE_AVERAGE_5(10500),
	SLIGHTLY_ABOVE_AVERAGE_6(10600),
	SLIGHTLY_ABOVE_AVERAGE_7(10700),
	SLIGHTLY_ABOVE_AVERAGE_8(10800),
	SLIGHTLY_ABOVE_AVERAGE_9(10900),
	ABOVE_AVERAGE(11000),
	ABOVE_AVERAGE_1(11100),
	ABOVE_AVERAGE_2(11200),
	ABOVE_AVERAGE_3(11300),
	ABOVE_AVERAGE_4(11400),
	ABOVE_AVERAGE_5(11500),
	ABOVE_AVERAGE_6(11600),
	ABOVE_AVERAGE_7(11700),
	ABOVE_AVERAGE_8(11800),
	ABOVE_AVERAGE_9(11900),
	VERY_SLIGHTLY_LONG(12000),
	VERY_SLIGHTLY_LONG_1(12100),
	VERY_SLIGHTLY_LONG_2(12200),
	VERY_SLIGHTLY_LONG_3(12300),
	VERY_SLIGHTLY_LONG_4(12400),
	VERY_SLIGHTLY_LONG_5(12500),
	VERY_SLIGHTLY_LONG_6(12600),
	VERY_SLIGHTLY_LONG_7(12700),
	VERY_SLIGHTLY_LONG_8(12800),
	VERY_SLIGHTLY_LONG_9(12900),
	SLIGHTLY_LONG(13000),
	SLIGHTLY_LONG_1(13100),
	SLIGHTLY_LONG_2(13200),
	SLIGHTLY_LONG_3(13300),
	SLIGHTLY_LONG_4(13400),
	SLIGHTLY_LONG_5(13500),
	SLIGHTLY_LONG_6(13600),
	SLIGHTLY_LONG_7(13700),
	SLIGHTLY_LONG_8(13800),
	SLIGHTLY_LONG_9(13900),
	LONG(14000),
	LONG_1(14100),
	LONG_2(14200),
	LONG_3(14300),
	LONG_4(14400),
	LONG_5(14500),
	LONG_6(14600),
	LONG_7(14700),
	LONG_8(14800),
	LONG_9(14900),
	VERY_LONG(15000),
	VERY_LONG_1(15100),
	VERY_LONG_2(15200),
	VERY_LONG_3(15300),
	VERY_LONG_4(15400),
	VERY_LONG_5(15500),
	VERY_LONG_6(15600),
	VERY_LONG_7(15700),
	VERY_LONG_8(15800),
	VERY_LONG_9(15900),
	EXTREMELY_LONG(16000),
	EXTREMELY_LONG_1(16100),
	EXTREMELY_LONG_2(16200),
	EXTREMELY_LONG_3(16300),
	EXTREMELY_LONG_4(16400),
	EXTREMELY_LONG_5(16500),
	EXTREMELY_LONG_6(16600),
	EXTREMELY_LONG_7(16700),
	EXTREMELY_LONG_8(16800),
	EXTREMELY_LONG_9(16900),
	HUGELY_LENGTHY(17000),
	HUGELY_LENGTHY_1(17100),
	HUGELY_LENGTHY_2(17200),
	HUGELY_LENGTHY_3(17300),
	HUGELY_LENGTHY_4(17400),
	HUGELY_LENGTHY_5(17500),
	HUGELY_LENGTHY_6(17600),
	HUGELY_LENGTHY_7(17700),
	HUGELY_LENGTHY_8(17800),
	HUGELY_LENGTHY_9(17900),
	VERY_HUGELY_LENGTHY(18000),
	VERY_HUGELY_LENGTHY_1(18100),
	VERY_HUGELY_LENGTHY_2(18200),
	VERY_HUGELY_LENGTHY_3(18300),
	VERY_HUGELY_LENGTHY_4(18400),
	VERY_HUGELY_LENGTHY_5(18500),
	VERY_HUGELY_LENGTHY_6(18600),
	VERY_HUGELY_LENGTHY_7(18700),
	VERY_HUGELY_LENGTHY_8(18800),
	VERY_HUGELY_LENGTHY_9(18900),
	EXTREMELY_HUGELY_LENGTHY(19000),
	EXTREMELY_HUGELY_LENGTHY_1(19100),
	EXTREMELY_HUGELY_LENGTHY_2(19200),
	EXTREMELY_HUGELY_LENGTHY_3(19300),
	EXTREMELY_HUGELY_LENGTHY_4(19400),
	EXTREMELY_HUGELY_LENGTHY_5(19500),
	EXTREMELY_HUGELY_LENGTHY_6(19600),
	EXTREMELY_HUGELY_LENGTHY_7(19700),
	EXTREMELY_HUGELY_LENGTHY_8(19800),
	EXTREMELY_HUGELY_LENGTHY_9(19900);

	private final int size;

	private LengthDepth(int size) {
		this.size = size;
	}

	public int getSize() {
		return size;
	}

	/**
	 * Stubby, shorter than {@link #isShort() short}.
	 */
	public boolean isStubby() {
		return size < EXTREMELY_SHORT.getSize();
	}

	/**
	 * Short, shorter than {@link #isAverage() average} but longer than
	 * {@link #isStubby() stubby}.
	 */
	public boolean isShort() {
		return size >= EXTREMELY_SHORT.getSize()
				&& size < AVERAGE.getSize();
	}

	/**
	 * Average, shorter than {@link #isLong() long} but longer than
	 * {@link #isShort() short}.
	 */
	public boolean isAverage() {
		return size >= AVERAGE.getSize()
				&& size < VERY_SLIGHTLY_LONG.getSize();
	}

	/**
	 * Long, shorter than {@link #isHugelyLengthy() hugely lengthy} but longer
	 * than {@link #isAverage() average}.
	 */
	public boolean isLong() {
		return size >= VERY_SLIGHTLY_LONG.getSize()
				&& size < HUGELY_LENGTHY.getSize();
	}

	/**
	 * Hugely lengthy, longer than {@link #isLong() long}.
	 */
	public boolean isHugelyLengthy() {
		return size >= HUGELY_LENGTHY.getSize();
	}

	private static final LengthDepth[] VALUES = values();

	public LengthDepth longer() {
		if (this.equals(EXTREMELY_HUGELY_LENGTHY_9)) {
			return EXTREMELY_HUGELY_LENGTHY_9;
		}
        return VALUES[(this.ordinal() + 1) % VALUES.length];
	}

	public LengthDepth shorter() {
		if (this.equals(EXTREMELY_STUBBY)) {
			return EXTREMELY_STUBBY;
		}
        return VALUES[(this.ordinal() - 1) % VALUES.length];
	}

	public static LengthDepth parseFrom(int size) {
		for (LengthDepth ld : VALUES) {
			if (ld.getSize() == size) {
				return ld;
			}
		}
		return null;
	}
}
