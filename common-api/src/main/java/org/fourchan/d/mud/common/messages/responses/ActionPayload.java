package org.fourchan.d.mud.common.messages.responses;

import java.util.List;

import org.fourchan.d.mud.common.StockadeClient;
import org.fourchan.d.mud.common.messages.ClientMessage;
import org.fourchan.d.mud.common.messages.ResponseConstants;
import org.fourchan.d.mud.common.messages.common.Action;

public class ActionPayload extends ClientMessage {
	private List<Action> actions;

	public ActionPayload(List<Action> actions) {
		super(ResponseConstants.ACTION_PAYLOAD_MESSAGE_TYPE);
		if (actions != null && !actions.isEmpty()) {
			this.actions = actions;
		}
	}

	public List<Action> getActions() {
		return actions;
	}

	@Override
	public void visit(StockadeClient client) {
		client.handleActionPayload(this);
	}
}
