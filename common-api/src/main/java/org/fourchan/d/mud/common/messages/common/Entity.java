package org.fourchan.d.mud.common.messages.common;

import org.fourchan.d.mud.common.map.Point;

public class Entity {
	public enum Type {
		HUMAN_PLAYER_CHARACTER
	}

	public Type type;
	public int id;
	public String name;
	public String description;
	public Point location;
}
