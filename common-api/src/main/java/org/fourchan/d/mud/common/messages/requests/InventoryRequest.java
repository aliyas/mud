package org.fourchan.d.mud.common.messages.requests;

import org.fourchan.d.mud.common.StockadeService;
import org.fourchan.d.mud.common.messages.RequestConstants;
import org.fourchan.d.mud.common.messages.ServerMessage;
import org.java_websocket.WebSocket;

public class InventoryRequest extends ServerMessage {
	public InventoryRequest() {
		super(RequestConstants.INVENTORY_REQUEST_MESSAGE_TYPE);
	}

	public void visit(StockadeService service, WebSocket socket) {
		service.handleInventoryRequest(socket, this);
	}
}
