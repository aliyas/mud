package org.fourchan.d.mud.common.messages;

import java.lang.reflect.Type;

import org.fourchan.d.mud.common.messages.requests.CharacterSelectedRequest;
import org.fourchan.d.mud.common.messages.requests.CreateCharacterRequest;
import org.fourchan.d.mud.common.messages.requests.ExamineEntityRequest;
import org.fourchan.d.mud.common.messages.requests.InventoryRequest;
import org.fourchan.d.mud.common.messages.requests.LoginChallengeRequest;
import org.fourchan.d.mud.common.messages.requests.LoginRequest;
import org.fourchan.d.mud.common.messages.requests.LogoutRequest;
import org.fourchan.d.mud.common.messages.requests.MoveRequest;
import org.fourchan.d.mud.common.messages.requests.NewGuestRequest;
import org.fourchan.d.mud.common.messages.requests.PlayerInfoRequest;
import org.fourchan.d.mud.common.messages.requests.PromoteGuestToNonGuestRequest;
import org.fourchan.d.mud.common.messages.requests.RegisterRequest;
import org.fourchan.d.mud.common.messages.requests.SpawnPointRequest;
import org.fourchan.d.mud.common.messages.requests.privileged.MapUpdateRequest;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

public class ServerMessageDeserializer implements JsonDeserializer<ServerMessage> {
	@Override
	public ServerMessage deserialize(
			JsonElement jsonElement,
			Type type,
			JsonDeserializationContext context)
			throws JsonParseException {
		JsonObject jsonObject = jsonElement.getAsJsonObject();
		JsonElement jsonType = jsonObject.get("type");
		String stringType = jsonType.getAsString();
		
		ServerMessage serverMessage = null;
		
		if (stringType.equals(RequestConstants.REGISTER_REQUEST_MESSAGE_TYPE)) {
			serverMessage = context.deserialize(jsonElement, RegisterRequest.class);
		} else if (stringType.equals(RequestConstants.LOGIN_CHALLENGE_REQUEST_MESSAGE_TYPE)) {
			serverMessage = context.deserialize(jsonElement, LoginChallengeRequest.class);
		} else if (stringType.equals(RequestConstants.LOGIN_REQUEST_MESSAGE_TYPE)) {
			serverMessage = context.deserialize(jsonElement, LoginRequest.class);
		} else if (stringType.equals(RequestConstants.LOGOUT_REQUEST_MESSAGE_TYPE)) {
			serverMessage = context.deserialize(jsonElement, LogoutRequest.class);
		} else if (stringType.equals(RequestConstants.PLAYER_INFO_REQUEST_MESSAGE_TYPE)) {
			serverMessage = context.deserialize(jsonElement, PlayerInfoRequest.class);
		} else if (stringType.equals(RequestConstants.CREATE_CHARACTER_REQUEST_MESSAGE_TYPE)) {
			serverMessage = context.deserialize(jsonElement, CreateCharacterRequest.class);
		} else if (stringType.equals(RequestConstants.SELECT_CHARACTER_REQUEST_MESSAGE_TYPE)) {
			serverMessage = context.deserialize(jsonElement, CharacterSelectedRequest.class);
		} else if (stringType.equals(RequestConstants.MOVE_REQUEST_MESSAGE_TYPE)) {
			serverMessage = context.deserialize(jsonElement, MoveRequest.class);
		} else if (stringType.equals(RequestConstants.SPAWN_POINT_REQUEST_MESSAGE_TYPE)) {
			serverMessage = context.deserialize(jsonElement, SpawnPointRequest.class);
		} else if (stringType.equals(RequestConstants.MAP_UPDATE_REQUEST_MESSAGE_TYPE)) {
			serverMessage = context.deserialize(jsonElement, MapUpdateRequest.class);
		} else if (stringType.equals(RequestConstants.EXAMINE_ENTITY_REQUEST_MESSAGE_TYPE)) {
			serverMessage = context.deserialize(jsonElement, ExamineEntityRequest.class);
		} else if (stringType.equals(RequestConstants.INVENTORY_REQUEST_MESSAGE_TYPE)) {
			serverMessage = context.deserialize(jsonElement, InventoryRequest.class);
		} else if (stringType.equals(RequestConstants.NEW_GUEST_REQUEST_MESSAGE_TYPE)) {
			serverMessage = context.deserialize(jsonElement, NewGuestRequest.class);
		} else if (stringType.equals(RequestConstants.PROMOTE_GUEST_REQUEST_MESSAGE_TYPE)) {
			serverMessage = context.deserialize(jsonElement, PromoteGuestToNonGuestRequest.class);
		} else {
			throw new JsonParseException("Unknown server message type: " + stringType);
		}
		
		return serverMessage;
	}
}
