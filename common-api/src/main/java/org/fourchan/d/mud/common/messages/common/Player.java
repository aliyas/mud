package org.fourchan.d.mud.common.messages.common;

import java.util.List;

public class Player {
	public List<Character> characters;
	public String username;
	public PlayerPermissions permissions;
	public PlayerKind playerKind;

	public static class PlayerPermissions {
		public boolean canEditMap;
	}

	public enum PlayerKind {
		UNKNOWN,
		NORMAL,
		GUEST
	}
}
