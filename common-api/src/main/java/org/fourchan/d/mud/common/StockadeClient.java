package org.fourchan.d.mud.common;

import org.fourchan.d.mud.common.messages.responses.ActionPayload;
import org.fourchan.d.mud.common.messages.responses.CharacterSelectedResponse;
import org.fourchan.d.mud.common.messages.responses.CreateCharacterResponse;
import org.fourchan.d.mud.common.messages.responses.EntityPayload;
import org.fourchan.d.mud.common.messages.responses.InventoryResponse;
import org.fourchan.d.mud.common.messages.responses.LoginChallengeResponse;
import org.fourchan.d.mud.common.messages.responses.LoginResponse;
import org.fourchan.d.mud.common.messages.responses.LogoutResponse;
import org.fourchan.d.mud.common.messages.responses.MapPayload;
import org.fourchan.d.mud.common.messages.responses.MoveResponse;
import org.fourchan.d.mud.common.messages.responses.NewGuestResponse;
import org.fourchan.d.mud.common.messages.responses.PlayerInfoResponse;
import org.fourchan.d.mud.common.messages.responses.PromoteGuestToNonGuestResponse;
import org.fourchan.d.mud.common.messages.responses.RegisterResponse;
import org.fourchan.d.mud.common.messages.responses.SpawnPointResponse;
import org.fourchan.d.mud.common.messages.responses.TextPayload;

public interface StockadeClient {
	void handleRegisterResponse(RegisterResponse response);
	void handleLoginChallengeResponse(LoginChallengeResponse response);
	void handleLoginResponse(LoginResponse response);
	void handleLogoutResponse(LogoutResponse response);
	void handlePlayerInfoResponse(PlayerInfoResponse response);
	void handleCreateCharacterResponse(CreateCharacterResponse response);
	void handleCharacterSelectedResponse(CharacterSelectedResponse response);
	void handleMapPayload(MapPayload payload);
	void handleMoveResponse(MoveResponse response);
	void handleSpawnPointResponse(SpawnPointResponse response);
	void handleEntityPayload(EntityPayload response);
	void handleActionPayload(ActionPayload response);
	void handleTextPayload(TextPayload response);
	void handleInventoryResponse(InventoryResponse response);
	void handleNewGuestResponse(NewGuestResponse response);
	void handlePromoteGuestToNonGuestResponse(PromoteGuestToNonGuestResponse response);
}
