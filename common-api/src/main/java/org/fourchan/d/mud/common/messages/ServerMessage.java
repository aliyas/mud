package org.fourchan.d.mud.common.messages;

import org.fourchan.d.mud.common.StockadeService;
import org.java_websocket.WebSocket;

/**
 * A message to be received by the server, and sent by the client.
 */
public class ServerMessage {
	/** Used in {@link ServerMessageDeserializer} */
	@SuppressWarnings("unused")
	private final String type;
	
	public ServerMessage(String type) {
		this.type = type;
	}

	/** Override to apply to services */
	public void visit(StockadeService service, WebSocket socket) {
		throw new RuntimeException("Override ServerMessage.visit to successfully route this message.");
	}
}
